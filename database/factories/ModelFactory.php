<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// User
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'code' => str_random(24),
        'name' => $faker->name,
        'location' => $faker->address,
        'avatar' => "https://s3.amazonaws.com/dev-files.songline.fm/avatars/".$faker->uuid.".jpg",
        'bio' => $faker->text(),
        'email' => $faker->email
    ];
});

// Playlist
$factory->define(App\Playlist::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->uuid,
        'user_code' => function () {
            return App\User::inRandomOrder()->first()->code;
        },
        'title' => $faker->sentence(3),
        'status' => rand(0, 1),
        'privacy' => rand(0, 1)
    ];
});

// Song
$factory->define(App\Song::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->uuid,
        'user_code' => function () {
            return App\User::inRandomOrder()->first()->code;
        },
        'title' => $faker->sentence(3),
        'producer' => $faker->name,
        'cover' => "https://s3.amazonaws.com/dev-files.songline.fm/covers/". $faker->uuid() . ".jpg",
        'file' => "https://s3.amazonaws.com/dev-files.songline.fm/mp3/". $faker->uuid() . ".mp3"
    ];
});

// SongStat
$factory->define(App\SongStat::class, function (Faker\Generator $faker) {
    return [
        'song_code' => function () {
            return App\Song::inRandomOrder()->first()->code;
        },
        'played' => $faker->numberBetween(0, 100),
        'added_to_playlist' => $faker->numberBetween(0, 100),
        'shared' => $faker->numberBetween(0, 100),
        'voted' => $faker->numberBetween(0, 100),
        'liked' => $faker->numberBetween(0, 100)
    ];
});

// Player
$factory->define(App\Player::class, function (Faker\Generator $faker) {
    return [
        'user_code' => function () {
            return App\User::inRandomOrder()->first()->user_code;
        },
        "trackPaused" => 0,
        "timePaused" => $faker->randomFloat(6, 0, 9999),
        "isShuffle" => rand(0, 1),
        "repeatState" => rand(0, 1),
        "tracksOrder" => "'1d425114-df38-339c-ab3a-c5587c9d28c4', '36300c3a-30f7-357f-bca8-a836ed0a6b1e'",
    ];
});

// Genre
$factory->define(App\Genre::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->uuid,
        'title' => $faker->sentence(3)
    ];
});

// Likes
$factory->define(App\Like::class, function (Faker\Generator $faker) {
    return [
        'user_code' => function () {
            return App\User::inRandomOrder()->first()->code;
        },
        'song_code' => function () {
            return App\Song::inRandomOrder()->first()->code;
        },
    ];
});

// ArtistStat
$factory->define(App\ArtistStat::class, function (Faker\Generator $faker) {
    return [
        'user_code' => function () {
            return App\User::inRandomOrder()->first()->code;
        }
    ];
});

// Roles
$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->uuid,
        'title' => $faker->word
    ];
});

// Competition
$factory->define(App\Competition::class, function (Faker\Generator $faker) {
    return [
        'code' => $faker->uuid,
        'title' => $faker->sentence(2),
        'date_start' => date('Y-m-d H:i:s', time()),
        'date_end' => date('Y-m-d H:i:s', time() + 7*24*60*60),
        'winner_song_code' => function () {
            return App\Song::inRandomOrder()->first()->code;
        },
        'winner_artist_code' => App\User::first()->code,
        'type' => 'Weekly',
        'status' => rand(0, 2)
    ];
});
