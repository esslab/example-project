<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GenreTableSeeder::class);
        $this->call(LangTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        // Create competitions
        $this->call(CompetitionSeeder::class);
        // Create achievements
        $this->call(AchievementSeeder::class);
        // Create comments
        $this->call(CommentSeeder::class);
        // Create gallery
        $this->call(GallerySeeder::class);
    }
}
