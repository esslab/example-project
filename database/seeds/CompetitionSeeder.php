<?php

use Illuminate\Database\Seeder;

class CompetitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $code = Faker\Factory::create()->uuid;
        $song = DB::table('songs')->first();
        $start = time();
        $end = (time() + (60 * 60 * 24 * 31));
        // Create competition
        DB::table('competitions')->insert([
            'code' => $code,
            'title' => 'Test competition',
            'date_start' => date("Y-m-d H:i:s", $start),
            'date_end' => date("Y-m-d H:i:s", $end),
            'type' => 'Bimonthly',
            'status' => 1,
            'winner_song_code' => $song->code,
            'winner_artist_code' => UsersTableSeeder::ARTIST,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        // Create competitions songs list
        DB::table('competitions_list')->insert([
            'competition_code' => $code,
            'song_code' => $song->code,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        // Create vote
        DB::table('votes')->insert([
            'competition_code' => $code,
            'song_code' => $song->code,
            'user_code' => UsersTableSeeder::USER,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
