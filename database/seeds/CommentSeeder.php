<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = Faker\Factory::create()->uuid;
        $song = DB::table('songs')->first();
        // Create first test comment
        DB::table('comments')->insert([
            'code' => $code,
            'user_code' => UsersTableSeeder::USER,
            'message' => 'Great song',
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        // Bind comment and song
        DB::table('comments_songs')->insert([
            'song_code' => $song->code,
            'comment_code' => $code,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
