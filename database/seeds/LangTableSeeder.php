<?php

use Illuminate\Database\Seeder;

class LangTableSeeder extends Seeder
{
    protected $countriesLangs = [
        "ar" => "Arabic",
        "be" => "Belarusian",
        "zh" => "Chinese",
        "da" => "Danish",
        "en" => "English",
        "et" => "Estonian",
        "fi" => "Finnish",
        "fr" => "French",
        "de" => "German",
        "hi" => "Hindi",
        "ja" => "Japanese",
        "ko" => "Korean",
        "lv" => "Latvian",
        "lt" => "Lithuanian",
        "no" => "Norwegian",
        "pl" => "Polish",
        "pt" => "Portuguese",
        "ru" => "Russian",
        "es" => "Spanish",
        "sv" => "Swedish",
        "uk" => "Ukrainian",
        "ur" => "Urdu",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->deleteNeedlessLanguages();
        $this->addingNewLanguagesIfNotExists();
        echo("Done!\n");
    }

    private function newLanguageExists($shortName) {
        return isset($this->countriesLangs["{$shortName}"]);
    }

    private function deleteNeedlessLanguages() {
        echo "1. Removing needless languages from database..\n";
        $allLanguageRecords = \App\Language::all();
        foreach ($allLanguageRecords as $langRec) {
            if (!$this->newLanguageExists($langRec->short_name)) {
                echo " Remove language {$langRec->short_name}\n";
                $langRec->songs()->detach();
                $langRec->users()->detach();
                $langRec->delete();
            }
        }
        return true;
    }

    private function addingNewLanguagesIfNotExists() {
        echo "2. Adding new languages if not exists.. \n";
        foreach ($this->countriesLangs as $code => $fullName) {
            if (count(\App\Language::where('short_name', $code)->get()) == 0) {
                echo " Add language {$code}\n";
                \App\Language::create([
                    "code" => Faker\Factory::create()->uuid,
                    "short_name" => $code,
                    "full_name" => $fullName
                ]);
            }
        }
        return true;
    }
}
