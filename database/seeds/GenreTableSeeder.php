<?php
use App\Genre;
use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    private static $genres = [
        'Alternative', 'Ambient', 'Blues', 'Breakbeat', 'Country', 'Dancehall', 'Deep House', 'Disco', 'Downtempo',
        'Drum’n’base', 'Dubstep', 'EDM', 'Experimental', 'Funk', 'Folk', 'Grime', 'Hard Rock', 'Heavy Metal', 'Hip-Hop',
        'House', 'IDM', 'Instrumental', 'Jazz', 'J-Pop', 'Jungle', 'K-Pop', 'Liquid D’n’B', 'Lo-Fi', 'Metal',
        'New Beat', 'New Wave', 'Pop', 'Punk', 'Psychedelic', 'R&B', 'Progressive', 'Reggae', 'Reggaeton', 'Rock',
        'Ska', 'Soul', 'Techno', 'Trance', 'Trap', 'Trip Hop', 'UK Garage'
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $availableGenres = Genre::all();
        $countNewGenres = count(self::getGenres());

        if ($availableGenres->count() === $countNewGenres && $availableGenres->contains('title', 'Breakbeat')) {
            return;
        }

        foreach (self::$genres as $genre) {
            factory(Genre::class)->create([
                'title'  => $genre,
                'status' => 1
            ]);
        }
    }

    public static function getGenres()
    {
        return self::$genres;
    }
}
