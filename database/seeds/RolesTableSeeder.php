<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    // const ADMIN = "5876505ebf8e63325065cd61";
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Role::class)->create(["title" => 'Admin']);
        factory(App\Role::class)->create(["title" => 'User']);
    }
}
