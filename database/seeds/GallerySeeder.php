<?php

use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gCode = Faker\Factory::create()->uuid;
        $iCode = Faker\Factory::create()->uuid;
        //
        DB::table('galleries')->insert([
            'code' => $gCode,
            'title' => 'Default gallery',
            'user_code' => UsersTableSeeder::USER,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        //
        DB::table('galleries_items')->insert([
            'code' => $iCode,
            'gallery_code' => $gCode,
            'user_code' => UsersTableSeeder::USER,
            'title' => Faker\Factory::create()->sentence(4),
            'source_file' => Faker\Factory::create()->imageUrl(),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
