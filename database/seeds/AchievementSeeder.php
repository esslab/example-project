<?php

use Illuminate\Database\Seeder;

class AchievementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $code = Faker\Factory::create()->uuid;
        // Create test achievement
        DB::table('achievements')->insert([
            'code' => $code,
            'title' => 'First Playlist',
            'description' => 'Create your first playlist and gain this achievement.',
            'image_file' => Faker\Factory::create()->imageUrl(),
            'status' => 1,
            'condition' => json_encode(['key' => 'value']),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        // Create user achievement connection
        DB::table('users_achievements')->insert([
            'user_code' => UsersTableSeeder::USER,
            'achievement_code' => $code,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
