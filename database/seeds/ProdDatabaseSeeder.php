<?php
use App\Auth0ManagementAPI;
use App\Classes\Facades\Logger;
use App\GalleryItem;
use App\Genre;
use App\Like;
use App\Song;
use App\Repository\StripeRepository;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;

class ProdDatabaseSeeder extends Seeder
{
    private $faker;
    private $sick6SongIdMappedSonglineSongId = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timeStart = microtime(true);

        $this->faker = \Faker\Factory::create();
        $this->call(RolesTableSeeder::class);

        $this->migrateUsers();
        $this->migrateUsersData();
        $this->migrateCompetitionsData();
        $this->exportUsersToAuth0AndUpdateIds();

        $timeEnd = microtime(true);
        $timeDiff = $timeEnd - $timeStart;
        $this->info($timeDiff, "Total time");
    }

    private function migrateUsers()
    {
        $users = \DB::connection('mysql_sick6')->table('wp_users')
            ->select(['ID', 'user_login', 'user_pass', 'user_email', 'user_registered'])
            // founded one user with empty 'email' - we don't need such users
            ->where('user_email', '<>', '')
            // Users below have 2 account and one of them have no any information or music and images
            // so i think it is the best way to ignore this users quickly
            ->whereNotIn('user_email', [
                'century25th@gmail.com',
                'Bullmedina176@gmail.com',
                'submusic30727@gmail.com',
                'james.carr1996@Yahoo.com'
            ])
            ->get();

        $usersSick6 = $users->each(function ($user) {
            $meta = \DB::connection('mysql_sick6')->table('wp_usermeta')
                ->select('meta_key', 'meta_value')
                ->where('user_id', $user->ID)
                ->whereIn('meta_key', [
                    'first_name', 'last_name', 'genres', 'location', 'city', 'user_avtar', 'description'
                ])
                ->get();

            $mappedUserMeta = $meta->mapWithKeys(function ($item) {
                return [$item->meta_key => $item->meta_value];
            });
            $user->meta = $mappedUserMeta;

            return $user;
        });

        foreach ($usersSick6 as $userSick6) {
            $this->createSonglineUser($userSick6);
        }

        $this->info('Users');
    }

    private function migrateUsersData()
    {
        $songlineUsers = User::all();
        $usersSongsAndCovers = collect(json_decode($this->getSongsAndCovers()));

        $songlineUsers->each(function ($user, $key) use ($usersSongsAndCovers) {
            // migrate songs
            $sick6UserSongs = $this->getSick6UserSongs($user, $usersSongsAndCovers);
            $this->createSonglineUserSongs($user, $sick6UserSongs);

            //todo[delete later maybe]
            // migrate images
            // $sick6UserImages = $this->getSick6UserImages($user);
            // $this->createSonglineUserImages($user, $sick6UserImages);
        });
        $this->info('Users songs and images');
    }

    private function createSonglineUser($userSick6)
    {
        $userRole = \App\Role::where('title', 'User')->first()->code;
        $adminRole = \App\Role::where('title', 'Admin')->first()->code;
        $usersSongsAndCovers = collect(json_decode($this->getSongsAndCovers()));
        $sick6UserFromFile = $usersSongsAndCovers->where('email', $userSick6->user_email)->values();

        $user = \App\User::create([
            "auth0_id"   => str_random(24),
            "role_code"  => $userSick6->user_email === 'zalmanson.idan@gmail.com' ? $adminRole : $userRole,
            "name"       => $this->getUserName($userSick6),
            "location"   => $this->getLocation($userSick6),
            "avatar"     => !empty($sick6UserFromFile[0]->avatar) ?
                $this->uploadSourceFileAndReturnNewPath($sick6UserFromFile[0]->avatar, 'avatars') : "",
            "bio"        => isset($userSick6->meta['description']) ? trim($userSick6->meta['description']) : "",
            "email"      => trim($userSick6->user_email),
            "created_at" => trim($userSick6->user_registered),
            "provider"   => "auth0",
            //todo[uncomment] After PR for adding this property will be done uncomment this
            // "is_artist"  => 1
        ]);

        if (! empty($userSick6->meta['genres'])) {
            $this->associateUserWithGenres($user, $userSick6->meta['genres']);
        }
        $this->createPlayer($user);
        // create User stat
        $user->stat()->create([
            "created_at" => trim($userSick6->user_registered),
            "updated_at" => trim($userSick6->user_registered)
        ]);

        $customer = $this->createStripeRecords($user);
        $user->stripe_id = $customer->id;
        $user->update();

        $this->info("User [{$user->code}] was created.");
    }

    private function getUserName($userSick6)
    {
        $name = $userSick6->user_login;

        if (! empty($userSick6->meta['first_name'])) {
            $name = $userSick6->meta['first_name'];
        }
        if (!empty($userSick6->meta['last_name'])) {
            $name .= ' '. $userSick6->meta['last_name'];
        }

        return trim($name);
    }

    private function getLocation($userSick6)
    {
        $location = '';

        if (!empty($userSick6->meta['location'])) {
            $location .= $userSick6->meta['location'];
        }
        if (! empty($userSick6->meta['city'])) {
            $location = $userSick6->meta['city'] .' '. $location;
        }

        return trim($location);
    }

    private function associateUserWithGenres($user, $genres)
    {
        if (1 === preg_match('/\d/', $genres)) {
            return;
        }
        // Alter, Hip Hop, ...
        $genresTitles = explode(',', trim($genres));
        array_walk($genresTitles, 'trim');
        $genresIds = Genre::whereIn('title', $genresTitles)->pluck('code')->toArray();
        $user->genres()->attach($genresIds);
    }

    private function info($string, $default = 'Migrated')
    {
        if (isset($this->command)) {
            $this->command->getOutput()->writeln("<info>$default:</info> $string");
        }
    }

    private function warn($string)
    {
        if (isset($this->command)) {
            $this->command->getOutput()->writeln("<error>$string</error>");
        }
        Logger::logMessage($string, false, true, 'error');
    }

    private function getSick6UserSongs($user, $usersSongsAndCovers)
    {
        $songs = [];
        $sick6UserSongs = $usersSongsAndCovers->where('email', $user->email)->values();

        if (empty($sick6UserSongs[0]->songs)) {
            return $songs;
        }

        foreach ($sick6UserSongs[0]->songs as $sick6UserSong) {

            $song = \DB::connection('mysql_sick6')->table('wp_posts')
                ->select(
                    'wp_posts.ID',
                    'wp_posts.post_date',
                    'wp_posts.post_modified',
                    'wp_posts.post_title',
                    'wp_posts.guid',
                    'wp_posts.post_parent'
                )
                ->join('wp_users', 'wp_posts.post_author', '=', 'wp_users.ID')
                ->where('wp_posts.ID', $sick6UserSong->post_id)
                ->first();

            if (is_null($song)) {
                $song = new \App\Song();
                $song->post_date = Carbon::create(2016, 06, 12);
                $song->post_modified = Carbon::create(2016, 06, 12);
            }

            $song->guid = $sick6UserSong->audio;
            $song->cover = $sick6UserSong->cover;
            $song->post_title = !empty($sick6UserSong->audio_title) ?
                                    $sick6UserSong->audio_title : $this->getSongTitleFromPath($sick6UserSong->audio);

            $distinctUsersIds = \DB::connection('mysql_sick6')->table('wp_users')->select('id')
                ->distinct()->orderBy('id')->get();
            // ratings
            $ratings = \DB::connection('mysql_sick6')->table('wp_songs_ratings')
                ->select(
                    \DB::raw(
                        "distinct(concat_ws('-', wp_songs_ratings.post_id, wp_songs_ratings.rating)) as unique_param, wp_songs_ratings.rating, wp_users.user_email, wp_songs_ratings.created"
                    )
                )
                ->join("wp_users", "wp_songs_ratings.user_id", "=", "wp_users.ID")
                ->where("wp_songs_ratings.post_id", $sick6UserSong->post_id)
                ->whereIn("wp_songs_ratings.user_id", $distinctUsersIds->pluck('id')->all())
                ->get();

            $song->ratings = $ratings;

            $songs[] = $song;
        }

        return $songs;
    }

    private function createSonglineUserSongs($user, $songs)
    {
        if (empty($songs)) {
            $this->info("User [{$user->code}] [{$user->email}] has no Songs files to migrate!");
            return;
        }
        foreach ($songs as $song) {
            if (! $filePath = $this->uploadSourceFileAndReturnNewPath($song->guid, 'audio')) {
                continue;
            }
            $songlineSong = \App\Song::create([
                "user_code"  => $user->code,
                "title"      => $song->post_title,
                "producer"   => "",
                "file"       => $filePath,
                "cover"      => empty($song->cover) ?
                                    '' : $this->uploadSourceFileAndReturnNewPath($song->cover, 'covers'),
                "created_at" => trim($song->post_date),
                "updated_at" => trim($song->post_modified),
                "duration"   => 0.0
            ]);
            // create Song's stat
            $songlineSong->stat()->create([]);

            if (!empty($song->ratings)) {
                $song->ratings->each(function ($item, $key) use ($songlineSong, $user) {
                    $ratingUser = User::where("email", $item->user_email)->first();
                    if ($item->rating >= 3
                        && !is_null($ratingUser)
                        && is_null(Like::byUserToSong($ratingUser, $songlineSong)->first())
                    ) {
                        // create a Like
                        $songlineSong->likes()->create([
                            'user_code' => $ratingUser->code,
                            'created_at' => $item->created,
                            'updated_at' => $item->created
                        ]);
                        // add 'liked' stat for a Song
                        $songlineSong->stat()->increment('liked');
                        // increment User 'liked' stat
                        $user->stat()->increment('liked');
                    }
                });
            }

            $this->sick6SongIdMappedSonglineSongId[(string)$song->guid] = $songlineSong->code;
        }

        $this->info('Songs for User: '. $user->code);
    }

    private function createSonglineUserSongFromSick6AndDelete($userEmail, $song)
    {
        $user = $this->getArtistByEmail($userEmail);
        $songlineSong = \App\Song::create([
            "user_code"  => $user->code,
            "title"      => $this->getSongTitleFromPath($song->guid),
            "producer"   => "",
            "file"       => $song->guid,
            "cover"      => "",
            // let's say all competition's songs from Dropbox were created at 12/06/2016
            "created_at" => Carbon::create(2016, 06, 12),
            "updated_at" => Carbon::create(2016, 06, 12),
            "deleted_at" => Carbon::now(),
            "duration"   => 0.0
        ]);

        // create Song's stat
        $songlineSong->stat()->create([]);

        if (!empty($song->ratings)) {
            $song->ratings->each(function ($item, $key) use ($songlineSong, $user) {
                $ratingUser = User::where("email", $item->user_email)->first();
                if ($item->rating >= 3
                    && !is_null($ratingUser)
                    && is_null(Like::byUserToSong($ratingUser, $songlineSong)->first())
                ) {
                    // create a Like
                    $songlineSong->likes()->create([
                        'user_code' => $ratingUser->code,
                        'created_at' => $item->created,
                        'updated_at' => $item->created
                    ]);
                    // add 'liked' stat for a Song
                    $songlineSong->stat()->increment('liked');
                    // increment User 'liked' stat
                    $user->stat()->increment('liked');
                }
            });
        }

        $this->sick6SongIdMappedSonglineSongId[$song->guid] = $songlineSong->code;
        $this->info("Song [{$song->guid}] for User [{$userEmail}] created and deleted.");

        return $songlineSong->code;
    }

    private function uploadSourceFileAndReturnNewPath($path, $folder)
    {
        $s3FilePath = urldecode($path);
        $s3 = \Storage::disk('s3');
        if (!$s3->exists($s3FilePath)) {
            abort(500, "File to copy [$path] does not exists.");
        }
        $s3FileUrl = $s3->url($s3FilePath);
        $fileInfo = $this->getFileInfo($s3FileUrl);
        $newFileName = $fileInfo['filename'];
        // we need to upload file to AWS S3 to proper folder by $type
        $filePathNew = "$folder/source/$newFileName";

        try {
            $s3->copy($s3FilePath, $filePathNew);
            $s3->setVisibility($filePathNew, 'public');

            $this->info("File [$path] copied to [$folder] folder");
        } catch (Exception $e) {
            $message = "An Error has occurred while copying file ($path) to S3.";
            $message .=  PHP_EOL . $e->getMessage();
            $this->warn($message);
            if ($folder === "avatars" || $folder === "covers") {
                return '';
            } else {
                return false;
            }
        }
        // return new relative path for the file
        return $filePathNew;
    }

    private function getSick6UserImages($user)
    {
        $images = \DB::connection('mysql_sick6')->table('wp_posts')
            ->select('post_date', 'post_modified', 'post_title', 'guid')
            ->join('wp_users', 'wp_posts.post_author', '=', 'wp_users.ID')
            ->where('wp_users.user_email', $user->email)
            ->where('post_mime_type', 'like', 'image%')
            ->where('post_status', '<>', 'Trash')
            ->get();

        return $images;
    }

    private function createSonglineUserImages($user, $images)
    {
        $gallery = $user->gallery()->first();

        if (is_null($gallery)) {
            $gallery = $user->gallery()->create([
                'code'       => $this->faker->uuid,
                'title'      => $user->name . ' gallery',
                'status'     => 0,
                'privacy'    => 0,
                'created_at' => $user->created_at,
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }

        foreach ($images as $image) {
            if (! $sourceFilePath = $this->uploadSourceFileAndReturnNewPath($image->guid, 'images')) {
                // if we can't read the file from url-path, we didn't create a GalleryItem resource
                continue;
            }

            $item = new GalleryItem();
            $item->code = $this->faker->uuid;
            $item->gallery_code = $gallery->code;
            $item->user_code = $user->code;
            $item->title = $image->post_title;
            $item->type = 'picture';
            $item->privacy = 0;
            $item->status = 0;
            $item->source_file = $sourceFilePath;
            $item->created_at = $image->post_date;
            $item->updated_at = $image->post_modified;
            $item->save();
        }

        $this->info('GalleryItems for User: '. $user->code);
    }

    private function migrateCompetitionsData()
    {
        $competitions = json_decode($this->getMigrationCompetition());

        foreach ($competitions as $key => $competition) {
            $songlineCompetition = $this->createSonglineCompetition($competition);
            $final6BimonthlyList = $this->getSongsByUrls($competition->final_6_list);
            $songlineCompetition->songs()->attach($final6BimonthlyList);

            foreach ($competition->weekly_list as $weeklyCompetition) {
                $songlineWeeklyCompetition = $this->createSonglineCompetition($weeklyCompetition);
                $final6WeeklyList = $this->getSongsByUrls($weeklyCompetition->final_6_list);
                $songlineWeeklyCompetition->songs()->attach($final6WeeklyList);
            }
        }

        $this->info('Competitions');
    }

    private function exportUsersToAuth0AndUpdateIds()
    {
        $users = User::with('role')->get();

        if ($users->isEmpty()) {
            $this->warn("There is no users to import.");
            return null;
        }
        $auth0ManagementAPI = new Auth0ManagementAPI();

        foreach ($users as $user) {
            $userData = [
                "connection"     => config('laravel-auth0.connection'),
                "password"       => bcrypt("secret"),
                "email"          => $user->email,
                "email_verified" => false,
                "verify_email"   => false,
                "name"           => $user->name,
                "user_metadata"  => [
                    "name"     => $user->name,
                    "location" => $user->location,
                ],
                "app_metadata"   => [
                    "migrated_from_sick6" => true,
                    "roles" => [
                        "user"
                    ]
                ]
            ];
            if ($user->role->title === 'Admin') {
                $userData["app_metadata"]["roles"][] = "admin";
            }
            // create User on Auth0 service
            $userAuth0Data = $auth0ManagementAPI->createUser($userData);
            // get Auth0 user ID for storing in our DB
            $user->auth0_id = $userAuth0Data['user_id'] ?? $userAuth0Data['sub'];
            $user->save();
        }

        $this->info("Users to Auth0.", "Imported");
    }

    /**
     * Return array of new file name and urlencoded file path values
     * @param $path
     * @return array
     */
    private function getFileInfo($path)
    {
        $file = new SplFileInfo($path);
        $fileInfo = [];

        $fileExtension = $file->getExtension() ?: mb_substr($path, strrpos($path, '.') + 1);
        $fileInfo['filename'] = $this->faker->uuid . (!empty($fileExtension) ? ".$fileExtension" : '');
        $fileInfo['title'] = $this->getSongTitleFromPath($file->getFilename());

        return $fileInfo;
    }

    private function getCompetitionWinnerSong($songUrl, $userEmail)
    {
        if (array_key_exists($songUrl, $this->sick6SongIdMappedSonglineSongId)) {
            return $this->sick6SongIdMappedSonglineSongId[$songUrl];
        }

        $songCode = '';
        if (Str::contains($songUrl, 'www.thesick6.com')) {
            $sick6SongByGuid = $this->getSick6SongByGuid($songUrl);
            $songCode = is_null($sick6SongByGuid) ?
                            '' : $this->createSonglineUserSongFromSick6AndDelete($userEmail, $sick6SongByGuid);
        }

        return $songCode;
    }

    private function getArtistByEmail($artistEmail)
    {
        $user = User::where('email', $artistEmail)->first();
        if (is_null($user)) {
            $this->warn("Can't find user by email ({$artistEmail}) for Competition.");
            return '';
        }
        return $user;

    }

    private function getMigrationCompetition()
    {
        $filePath = '/data-migration/competitions.json';
        $competitions = \Storage::get($filePath);
        return $competitions;
    }

    /**
     * @param $competition
     * @return \App\Competition
     */
    private function createSonglineCompetition($competition)
    {
        $dateEnd = Carbon::createFromFormat('d.m.Y', $competition->date_end);
        $dateStart = Carbon::createFromFormat('d.m.Y', $competition->date_start);
        $user = $this->getArtistByEmail($competition->winner_artist_code);

        $songlineCompetition = new \App\Competition();
        $songlineCompetition->code = $this->faker->uuid;
        $songlineCompetition->title = $this->createCompetitionTitleFromDate($dateStart, $dateEnd);;
        $songlineCompetition->date_start = $dateStart;
        $songlineCompetition->date_end = $dateEnd;
        $songlineCompetition->winner_artist_code = $user->code;
        $songlineCompetition->winner_song_code = $this->getCompetitionWinnerSong(
            $competition->winner_song_code,
            $competition->winner_artist_code
        );
        $songlineCompetition->type = $competition->type;
        $songlineCompetition->status = $competition->status;
        $songlineCompetition->created_at = Carbon::createFromFormat('d.m.Y', $competition->created);
        $songlineCompetition->updated_at = Carbon::createFromFormat('d.m.Y', $competition->created);
        $songlineCompetition->save();

        return $songlineCompetition;
    }

    private function getSongsByUrls($final_6_list)
    {
        $songs = [];
        $songsSeparated = $this->separateSongsToDropboxAndSick6($final_6_list);

        if (!empty($songsSeparated['dropbox'])) {
            $songsUrls = collect($songsSeparated['dropbox'])->pluck('song')->toArray();
            // check this songs already in the Migrated list
            foreach ($songsUrls as $songsUrl) {
                if (array_key_exists($songsUrl, $this->sick6SongIdMappedSonglineSongId)) {
                    $songs[] = $this->sick6SongIdMappedSonglineSongId[$songsUrl];
                }
            }
        }

        if (!empty($songsSeparated['sick6'])) {
            $artistsSongsGuids = collect($songsSeparated['sick6'])->pluck('song', 'artist')->toArray();
            foreach ($artistsSongsGuids as $artistEmail => $sick6SongGuid) {
                // check this songs already in the Migrated list
                if (array_key_exists($sick6SongGuid, $this->sick6SongIdMappedSonglineSongId)) {
                    $songs[] = $this->sick6SongIdMappedSonglineSongId[$sick6SongGuid];
                } else {
                    $sick6Song = $this->getSick6SongByGuid($sick6SongGuid);
                    $songs[] = is_null($sick6Song) ?
                        '' : $this->createSonglineUserSongFromSick6AndDelete($artistEmail, $sick6Song);
                }
            }
        }

        return $songs;
    }

    private function separateSongsToDropboxAndSick6($final_6_list)
    {
        $onlySongsUrls = collect($final_6_list);
        $theSick6Songs = $onlySongsUrls->filter(function ($value, $key) use ($onlySongsUrls) {
            $check = Str::contains($value->song, 'www.thesick6.com');
            if ($check) {
                $onlySongsUrls->forget($key);
            }
            return $check;
        });

        return ['sick6' => $theSick6Songs->all(), 'dropbox' => $onlySongsUrls->all()];
    }

    private function getSick6SongByGuid($songUrl)
    {
        $song = \DB::connection('mysql_sick6')->table('wp_posts')
            ->select(
                'wp_posts.ID',
                'wp_posts.post_date',
                'wp_posts.post_modified',
                'wp_posts.post_title',
                'wp_posts.guid',
                'wp_posts.post_parent'
            )
            ->join('wp_users', 'wp_posts.post_author', '=', 'wp_users.ID')
            ->where('wp_posts.guid', $songUrl)
            ->first();

        if (!is_null($song)) {
            $distinctUsersIds = \DB::connection('mysql_sick6')->table('wp_users')->select('id')
                ->distinct()->orderBy('id')->get();
            // ratings
            $ratings = \DB::connection('mysql_sick6')->table('wp_songs_ratings')
                ->select(
                    \DB::raw(
                        "distinct(concat_ws('-', wp_songs_ratings.post_id, wp_songs_ratings.rating)) as unique_param, wp_songs_ratings.rating, wp_users.user_email, wp_songs_ratings.created"
                    )
                )
                ->join("wp_users", "wp_songs_ratings.user_id", "=", "wp_users.ID")
                ->where("wp_songs_ratings.post_id", $song->ID)
                ->whereIn("wp_songs_ratings.user_id", $distinctUsersIds->pluck('id')->all())
                ->get();
            $song->ratings = $ratings;
        }

        return $song;
    }

    private function createPlayer($user)
    {
        $newPlayerData = [
            'timePaused'  => 0,
            'trackPaused' => 0,
            'tracksOrder' => '',
            'isShuffle'   => 0,
            'repeatState'    => 0
        ];
        $player = $user->player()->create($newPlayerData);
        $songs = Song::with(['user', 'stat', 'genres', 'languages'])->limit(10)->get();
        $songs = $songs->pluck('code')->all();
        $player->songs()->sync($songs);
    }

    /**
     * @param $user
     * @return \Stripe\Customer
     */
    private function createStripeRecords($user): \Stripe\Customer
    {
        $stripeRepository = new StripeRepository();
        $stripeRepository->createPlan($user, 0);
        $customer = $stripeRepository->createCustomer($user);

        return $customer;
    }

    private function getSongsAndCovers()
    {
        $filePath = '/data-migration/user-data.json';
        $data = \Storage::get($filePath);
        return $data;
    }

    private function getSongTitleFromPath($audio)
    {
        $prefix = substr($audio, strrpos($audio, '.'));
        $title = basename($audio, $prefix);
        $title = str_replace(['_', '-'], ' ', $title);
        $title = rawurldecode($title);
        return $title;
    }

    /**
     * Creates a title for competition
     * @example January 1 - 8 or 29 May - 11 June
     * @param string $start Start date of the competition
     * @param string $end End date of the competition
     * @return string
     */
    private function createCompetitionTitleFromDate($start, $end)
    {
        $start = Carbon::parse($start);
        $end = Carbon::parse($end);

        $startDay = $start->day;
        $endDay = $end->day;

        $startMonth = $start->month;
        $endMonth = $end->month;

        $startMonthName = $start->format('F');
        $endMonthName = $end->format('F');

        if ($startMonth !== $endMonth) {
            return sprintf('%s %d - %s %d', $startMonthName, $startDay, $endMonthName, $endDay);
        } else {
            return sprintf('%s %d - %d', $startMonthName, $startDay, $endDay);
        }
    }
}
