<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    const USER = "580cde88f9b77d276511cfd1";
    const ARTIST = "580fa44d6f60741b4d5726f9";
    const ADMIN = "5876505ebf8e63325065cd61";
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = \App\Role::where('title', 'User')->first()->code;
        $adminRole = \App\Role::where('title', 'Admin')->first()->code;

        $user = factory(App\User::class)->create([
            "code" => self::USER,
            "role_code" => $userRole
        ]);
        $user_artist = factory(App\User::class)->create([
            "code" => self::ARTIST,
            "role_code" => $userRole
        ]);
        factory(App\User::class)->create([
            "code" => self::ADMIN,
            "role_code" => $adminRole
        ]);

        $user->languages()->attach(\App\Language::inRandomOrder()->first()->code);
        $user_artist->languages()->attach(\App\Language::inRandomOrder()->first()->code);

        $user_artist->followedByUsers()->attach($user->code);

        factory(App\ArtistStat::class)->create(['user_code' => self::ARTIST]);

        $user_artist->genres()->attach(\App\Genre::inRandomOrder()->take(4)->get()->pluck('code')->toArray());

        foreach (static::getMp3Names() as $k => $code) {
            $song = factory(App\Song::class)->create([
                'code'        => $code,
                'user_code' => $user_artist->code,
                'cover'       => "https://s3.amazonaws.com/dev-files.songline.fm/covers/$code.jpg",
                'file'        => "https://s3.amazonaws.com/dev-files.songline.fm/mp3/$code.mp3"
            ]);
            $song->stat()->save(
                factory(App\SongStat::class)->make()
            );
            $genreCode = \App\Genre::inRandomOrder()->first()->code;
            $song->genres()->attach($genreCode);

            $langCode = \App\Language::inRandomOrder()->first()->code;
            $song->languages()->attach($langCode);

            if ($k % 2 === 0) {
                factory(\App\Like::class)->create([
                    'song_code' => $code,
                    'user_code' => $user->code
                ]);
            }
        }
        $playlist = factory(App\Playlist::class)->make();
        $user->playlists()->save($playlist);
        $playlist->songs()->saveMany(\App\Song::inRandomOrder()->take(4)->get());

        $playlist = factory(App\Playlist::class)->make();
        $user_artist->playlists()->save($playlist);
        $playlist->songs()->saveMany(\App\Song::inRandomOrder()->take(4)->get());

        $queue1 = array_rand(array_flip(static::getMp3Names()), 5);
        $queue2 = array_rand(array_flip(static::getMp3Names()), 3);
        $playerUser = factory(App\Player::class)->create([
            'user_code' => $user->code,
            'trackPaused' => 0,
            'tracksOrder' => array_reduce($queue1, function ($carry, $item) use ($queue1) {
                $lastItem = end($queue1);
                $carry .= "'".$item."'";
                $carry .= $lastItem !== $item ? ',' : '';
                return $carry;
            })
        ]);
        $playerArtist = factory(App\Player::class)->create([
            'user_code' => $user_artist->code,
            'trackPaused' => 0,
            'tracksOrder' => array_reduce($queue2, function ($carry, $item, $init = '') use ($queue2) {
                $lastItem = end($queue2);
                $carry .= "'".$item."'";
                $carry .= $lastItem !== $item ? ',' : '';
                return $carry;
            })
        ]);
        $playerUser->songs()->attach($queue1);
        $playerArtist->songs()->attach($queue2);
    }

    /**
     * Simple returns constant UUID's for mp3 and image files for S3 cloud
     * @return array
     */
    private static function getMp3Names()
    {
        return [
            "1d425114-df38-339c-ab3a-c5587c9d28c4",
            "1f43e00f-0ed0-34c3-bb7b-db1e0b2dbaff",
            "224c71d2-b15d-3d3e-8c08-0ee9a81761d2",
            "250ccf14-aa02-3a3c-bdcc-76ce06059aa7",
            "36300c3a-30f7-357f-bca8-a836ed0a6b1e",
            "39071a42-49fe-31c1-9824-e9535cfe5652",
            "671bf1c2-1a06-3625-a8b0-28a9b85cf4c0",
            "779336c1-135e-34b8-9fdc-6646c3484aaf",
            "bc18aab7-5b3e-316b-add0-17288b106616",
            "d5cef022-0d01-3737-8152-96216e16d15d"
        ];
    }
}
