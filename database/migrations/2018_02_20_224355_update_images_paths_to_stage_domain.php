<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class UpdateImagesPathsToStageDomain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (App::environment('production')) {
            return;
        }
        if (Schema::hasTable('songs')) {
            dump('Start updating songs table');
            $affected = DB::update(
                'update songs set cover = replace(cover, ?, ?), file = replace(file, ?, ?)',
                ['https://s3-eu-west-1.amazonaws.com/songline-staging-files/', config('app.app_files_url'),
                'https://s3-eu-west-1.amazonaws.com/songline-staging-files/', config('app.app_files_url')]
            );
            dump('Updated records number: ' . $affected);
        }
        if (Schema::hasTable('galleries_items')) {
            dump('Start updating galleries_items table');
            DB::update(
                'update galleries_items set source_file = replace(source_file, ?, ?)',
                ['https://s3-eu-west-1.amazonaws.com/songline-staging-files/', '']
            );
            $affected = DB::update(
                "update galleries_items set source_file = CONCAT(?, trim(leading ? from source_file))",
                [config('app.app_files_url'), '/']
            );
            dump('Updated records number: ' . $affected);
        }
        if (Schema::hasTable('users')) {
            dump('Start updating users table');
            $affected = DB::update(
                'update users set avatar = replace(avatar, ?, ?)',
                ['https://s3-eu-west-1.amazonaws.com/songline-staging-files/', config('app.app_files_url')]
            );
            dump('Updated records number: ' . $affected);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (App::environment('production')) {
            return;
        }
        if (Schema::hasTable('songs')) {
            dump('Start reverting back songs table');
            $affected = DB::update(
                'update songs set cover = replace(cover, ?, ?), file = replace(file, ?, ?)',
                [config('app.app_files_url'), 'https://s3-eu-west-1.amazonaws.com/songline-staging-files/',
                    config('app.app_files_url'), 'https://s3-eu-west-1.amazonaws.com/songline-staging-files/']
            );
            dump('Reverted records number: ' . $affected);
        }

        if (Schema::hasTable('galleries_items')) {
            dump('Start reverting back galleries_items table');
            DB::update(
                'update galleries_items set source_file = replace(source_file, ?, ?)',
                [config('app.app_files_url'), '']
            );
            dump('Reverted records number: ' . $affected);
        }
        if (Schema::hasTable('users')) {
            dump('Start reverting back users table');
            $affected = DB::update(
                'update users set avatar = replace(avatar, ?, ?)',
                [config('app.app_files_url'), 'https://s3-eu-west-1.amazonaws.com/songline-staging-files/']
            );
            dump('Reverted records number: ' . $affected);
        }
    }
}
