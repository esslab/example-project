<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlayListsTable extends Migration
{
    private $table = 'playlists';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('code');
                $table->char('user_code', 24);
                $table->string('title');
                $table->tinyInteger('status')->default(0);
                $table->tinyInteger('privacy')->default(0);
                $table->timestamps();
                // Create indexes
                $table->primary('code');
                $table->index('user_code');
                // Create foreign keys
                $table->foreign('user_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
