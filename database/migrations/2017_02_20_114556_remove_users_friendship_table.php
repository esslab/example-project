<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUsersFriendshipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users_friendship');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('users_friendship')) {
            Schema::create('users_friendship', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->char('sender_code', 24);
                $table->char('recipient_code', 24);
                $table->tinyInteger('status')->default(0);
                $table->string('message');
                $table->timestamps();
                // Create indexes
                $table->index('sender_code');
                $table->index('recipient_code');
                $table->unique(['sender_code', 'recipient_code']);
                // Create foreign keys
                $table->foreign('sender_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('recipient_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }
}
