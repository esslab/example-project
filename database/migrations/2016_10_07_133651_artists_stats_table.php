<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtistsStatsTable extends Migration
{
    private $table = 'artists_stats';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->char('artist_code', 24);
                $table->integer('rating')->default(0);
                $table->integer('winner')->default(0);
                $table->integer('followed')->default(0);
                $table->timestamps();
                // Create indexes
                $table->primary('artist_code');
                // Create foreign keys
                $table->foreign('artist_code')
                    ->references('user_code')
                    ->on('artists')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
