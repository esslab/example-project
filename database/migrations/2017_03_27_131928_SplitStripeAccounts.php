<?php

use App\Repository\StripeRepository;
use Illuminate\Database\Migrations\Migration;

class SplitStripeAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $stripeRepository = new StripeRepository();
        $stripeRepository->deleteAllCustomers();
        $stripeRepository->deleteAllPlans();
        $users = \App\User::all();
        foreach ($users as $user) {
            $stripeRepository->createPlan($user, 0);
            $customer         = $stripeRepository->createCustomer($user);
            DB::table('users')->where('code', $user->code)->update(['stripe_id' => $customer->id]);
        }
        \App\Subscription::truncate();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
