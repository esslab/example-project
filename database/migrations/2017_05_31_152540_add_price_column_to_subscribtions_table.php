<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceColumnToSubscribtionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function(Blueprint $table) {
            $table->decimal('price', 8, 2)->nullable();
        });
        $subscriptions = App\Subscription::all();
        dump('Starting to update subscription price');
        foreach ($subscriptions as $subscription) {
            $artist = App\User::find($subscription->stripe_plan);
            $subscription->price = $artist->plan_cost;
            $subscription->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function(Blueprint $table) {
            $table->dropColumn('price');
        });
    }
}
