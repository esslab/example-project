<?php

use App\Repository\Modules\Profiles\ApiArtists;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetFieldIsArtist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ApiArtists::has('songs')->update(['is_artist' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ApiArtists::has('songs')->update(['is_artist' => false]);
    }
}
