<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserPayoutDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_payout_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('user_code', 24);
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('birthday')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('postcode', 12)->nullable();
            $table->string('personal_id')->nullable();
            $table->string('file')->nullable();
            $table->string('account_number')->nullable();
            $table->string('swift')->nullable();
            $table->string('bank_name')->nullable();
            $table->primary('user_code');
            $table->timestamps();

            $table->foreign('user_code')
                ->references('code')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_payout_details');
    }
}
