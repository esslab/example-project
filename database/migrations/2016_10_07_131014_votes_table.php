<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VotesTable extends Migration
{
    private $table = 'votes';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table){
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('competition_code');
                $table->uuid('song_code');
                $table->char('user_code', 24);
                $table->timestamps();
                // Create indexes
                $table->index('competition_code');
                $table->index('song_code');
                $table->index('user_code');
                $table->unique(['competition_code', 'song_code', 'user_code']);
                // Create foreign keys
                $table->foreign('competition_code')
                    ->references('code')
                    ->on('competitions')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('song_code')
                    ->references('code')
                    ->on('songs')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('user_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
