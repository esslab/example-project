<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSongsStatisticColumnsToArtistStatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('artists_stats', function (Blueprint $table) {
            $table->integer('liked')->default(0);
            $table->integer('shared')->default(0);
            $table->integer('added_to_playlist')->default(0);
            $table->integer('played')->default(0);
            $table->integer('voted')->default(0);
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('artists_stats', function (Blueprint $table) {
            $table->dropColumn(['liked', 'shared', 'added_to_playlist', 'played', 'voted']);
        });
        Schema::enableForeignKeyConstraints();
    }
}
