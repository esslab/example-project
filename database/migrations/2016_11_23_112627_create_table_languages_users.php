<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLanguagesUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->char('user_code', 24);
            $table->uuid('lang_code');
            $table->timestamps();
            // Create indexes
            $table->index('lang_code');
            $table->index('user_code');
            $table->unique(['lang_code', 'user_code']);
            // Create foreign keys
            $table->foreign('lang_code')
                ->references('code')
                ->on('languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_code')
                ->references('code')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages_users');
    }
}
