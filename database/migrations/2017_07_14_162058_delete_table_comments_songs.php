<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTableCommentsSongs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('comments_songs');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('comments_songs')) {
            Schema::create('comments_songs', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('song_code');
                $table->uuid('comment_code');
                $table->timestamps();

                // Create indexes
                $table->unique(['song_code', 'comment_code']);
                // Create foreign keys
                $table->foreign('song_code')
                    ->references('code')
                    ->on('songs')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('comment_code')
                    ->references('code')
                    ->on('comments')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }

    }
}
