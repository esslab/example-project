<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAchievementsTable extends Migration
{
    private $table = 'users_achievements';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->char('user_code', 24);
                $table->uuid('achievement_code');
                $table->timestamps();
                // Create indexes
                $table->index('user_code');
                $table->index('achievement_code');
                $table->unique(['user_code', 'achievement_code']);
                // Create foreign keys
                $table->foreign('user_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('achievement_code')
                    ->references('code')
                    ->on('achievements')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
