<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('likes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            // fields
            $table->uuid('code');
            $table->uuid('song_code');
            $table->string('user_code', 24);
            $table->timestamps();
            // indexes
            $table->primary('code');
            $table->foreign('song_code')
                ->references('code')
                ->on('songs')
                ->onDelete('cascade');
            $table->foreign('user_code')
                ->references('code')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->unique(['song_code', 'user_code']);
            $table->index(['song_code', 'user_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
