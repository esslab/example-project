<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddedByAdminFieldToCompetitionsListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('competitions_list', 'added_by_admin')) {
            return;
        }
        Schema::table('competitions_list', function (Blueprint $table) {
            $table->tinyInteger('added_by_admin', false, true)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn('competitions_list', 'added_by_admin')) {
            return;
        }
        Schema::table('competitions_list', function (Blueprint $table) {
            $table->dropColumn('added_by_admin');
        });
    }
}
