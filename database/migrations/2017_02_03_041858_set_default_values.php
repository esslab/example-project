<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('songs_stats', function (Blueprint $table) {
            $table->integer('played')->unsigned()->default(0)->change();
            $table->integer('added_to_playlist')->unsigned()->default(0)->change();
            $table->integer('liked')->unsigned()->default(0)->change();
            $table->integer('shared')->unsigned()->default(0)->change();
            $table->integer('voted')->unsigned()->default(0)->change();
        });

        Schema::table('artists_stats', function (Blueprint $table) {
            $table->integer('played')->unsigned()->change();
            $table->integer('added_to_playlist')->unsigned()->change();
            $table->integer('liked')->unsigned()->change();
            $table->integer('shared')->unsigned()->change();
            $table->integer('voted')->unsigned()->change();
            $table->integer('rating')->unsigned()->change();
            $table->integer('winner')->unsigned()->change();
            $table->integer('followed')->unsigned()->change();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('songs_stats', function (Blueprint $table) {
            $table->integer('played')->default(NULL)->change();
            $table->integer('added_to_playlist')->default(NULL)->change();
            $table->integer('liked')->default(NULL)->change();
            $table->integer('shared')->default(NULL)->change();
            $table->integer('voted')->default(NULL)->change();
        });

        Schema::table('artists_stats', function (Blueprint $table) {
            $table->integer('played')->change();
            $table->integer('added_to_playlist')->change();
            $table->integer('liked')->change();
            $table->integer('shared')->change();
            $table->integer('voted')->change();
            $table->integer('rating')->change();
            $table->integer('winner')->change();
            $table->integer('followed')->change();
        });
    }
}
