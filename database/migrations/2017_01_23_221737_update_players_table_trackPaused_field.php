<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePlayersTableTrackPausedField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('players', function (Blueprint $table) {
            $table->dropColumn('trackPaused');
        });
        Schema::table('players', function (Blueprint $table) {
            $table->smallInteger('trackPaused')->unsigned()->default(0);
            $table->text('tracksOrder');
        });

        if (Schema::hasColumn('players', 'tracksOrder')) {
            // fill 'tracksOrder' field by data
            \App\Player::all()->each(function ($player) {
                $tracks = DB::table('player_song')
                    ->selectRaw('group_concat(quote(song_code)) as tracks_order')
                    ->where('player_code', $player->user_code)
                    ->groupBy('player_code')->first();
                $player->tracksOrder = $tracks->tracks_order;
                $player->timePaused = 0.0;
                $player->save();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players', function (Blueprint $table) {
            $table->dropColumn(['trackPaused', 'tracksOrder']);
        });
        Schema::table('players', function (Blueprint $table) {
            $table->uuid('trackPaused');
        });
    }
}
