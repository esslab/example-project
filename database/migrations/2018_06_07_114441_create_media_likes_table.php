<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_likes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('item_code')
                ->references('code')
                ->on('galleries_items')
                ->onDelete('cascade');
            $table->string('user_code', 24)
                ->references('code')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();

            $table->unique(['item_code', 'user_code']);
            $table->index(['item_code', 'user_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_likes');
    }
}
