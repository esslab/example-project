<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SongsTable extends Migration
{

    private $table = 'songs';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table){
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('code');
                $table->char('artist_code', 24);
                $table->string('cover');
                $table->string('file');
                $table->string('title');
                $table->string('producer');
                $table->timestamps();
                // Create indexes
                $table->primary('code');
                // Create foreign keys
                $table->foreign('artist_code')
                    ->references('user_code')
                    ->on('artists')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
