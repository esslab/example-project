<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class UpdateImagesPathsToProdDomain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!App::environment('production')) {
            dump('This command expects to be run on Production environment.');
            return;
        }
        if (Schema::hasTable('songs')) {
            dump('Start updating songs table');
            $affected = DB::update(
                'update songs set cover = replace(cover, ?, ?), file = replace(file, ?, ?)',
                ['https://songline-production-files.s3.amazonaws.com/', config('app.app_files_url'),
                'https://songline-production-files.s3.amazonaws.com/', config('app.app_files_url')]
            );
            dump('Updated records number: ' . $affected);
        }
        if (Schema::hasTable('galleries_items')) {
            dump('Start updating galleries_items table');
            DB::update(
                'update galleries_items set source_file = replace(source_file, ?, ?)',
                ['https://songline-production-files.s3.amazonaws.com/', '']
            );
            $affected = DB::update(
                "update galleries_items set source_file = CONCAT(?, trim(leading ? from source_file))",
                [config('app.app_files_url'), '/']
            );
            dump('Updated records number: ' . $affected);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!App::environment('production')) {
            dump('This command expects to be run on Production environment.');
            return;
        }
        if (Schema::hasTable('songs')) {
            dump('Start updating songs table');
            $affected = DB::update(
                'update songs set cover = replace(cover, ?, ?), file = replace(file, ?, ?)',
                [config('app.app_files_url'), 'https://songline-production-files.s3.amazonaws.com/',
                    config('app.app_files_url'), 'https://songline-production-files.s3.amazonaws.com/']
            );
            dump('Updated records number: ' . $affected);
        }

        if (Schema::hasTable('galleries_items')) {
            dump('Start updating galleries_items table');
            DB::update(
                'update galleries_items set source_file = replace(source_file, ?, ?)',
                [config('app.app_files_url'), '']
            );
            dump('Updated records number: ' . $affected);
        }
    }
}
