<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerSongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_song', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            // Create columns
            $table->uuid('player_code');
            $table->uuid('song_code');
            $table->timestamps();
            // Create indexes
            $table->index('player_code');
            $table->index('song_code');
            $table->unique(['player_code', 'song_code']);
            // Create foreign keys
            $table->foreign('player_code')
                ->references('user_code')
                ->on('players')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('song_code')
                ->references('code')
                ->on('songs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_song');
    }
}
