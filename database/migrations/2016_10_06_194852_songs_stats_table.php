<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SongsStatsTable extends Migration
{
    private $table = 'songs_stats';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('song_code');
                $table->integer('played');
                $table->integer('added_to_playlist');
                $table->integer('shared');
                $table->integer('voted');
                $table->timestamps();
                // Create indexes
                $table->primary('song_code');
                // Create foreign keys
                $table->foreign('song_code')
                    ->references('code')
                    ->on('songs')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
