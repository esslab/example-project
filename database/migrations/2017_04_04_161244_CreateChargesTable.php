<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charges', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('user_code', 24);
            $table->string('plan', 24);
            $table->unsignedMediumInteger('amount');
            $table->string('currency');
            $table->timestampTz('created')->nullable();
            $table->timestamps();
            // Create indexes
            $table->index('user_code');
            // Create foreign keys
            $table->foreign('user_code')
                ->references('code')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charges');
    }
}
