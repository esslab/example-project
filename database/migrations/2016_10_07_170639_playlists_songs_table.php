<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlaylistsSongsTable extends Migration
{
    private $table = 'playlists_songs';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('playlist_code');
                $table->uuid('song_code');
                $table->tinyInteger('privacy')->default(0);
                $table->timestamps();
                // Create indexes
                $table->index('playlist_code');
                $table->index('song_code');
                $table->unique(['playlist_code', 'song_code']);
                // Create foreign keys
                $table->foreign('playlist_code')
                    ->references('code')
                    ->on('playlists')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('song_code')
                    ->references('code')
                    ->on('songs')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
