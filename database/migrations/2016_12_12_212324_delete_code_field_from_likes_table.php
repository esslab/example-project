<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCodeFieldFromLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('likes', function (Blueprint $table) {
            $table->dropPrimary('likes_code_primary');
            $table->dropColumn('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('likes', function (Blueprint $table) {
            $table->uuid('code');
        });
        $likes = \App\Like::all();
        foreach ($likes as $like) {
            DB::update(
                'update likes set code = ? where user_code = ? and song_code = ?',
                [Faker\Factory::create()->uuid, $like->user_code, $like->song_code]
            );
        }
        Schema::table('likes', function (Blueprint $table) {
            $table->primary('code');
        });
    }
}
