<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTableComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('comments');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('comments')) {
            Schema::create('comments', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('code');
                $table->bigInteger('user_code', false, true);
                $table->text('message');
                $table->timestamps();

                // Create indexes
                $table->primary('code');
                $table->foreign('user_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }
}
