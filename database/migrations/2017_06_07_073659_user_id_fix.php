<?php

use App\Repository\StripeRepository;
use App\Repository\UserRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserIdFix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = \App\User::all();
        if (App::environment('development', 'local')) {
            dump('Starting truncate plans');
            $stripeRepository = new StripeRepository();
            $stripeRepository->deleteAllPlans();
            dump('Completed');
            foreach ($users as $user) {
                try {
                    \Stripe\Plan::create(array(
                        "name" => $user->name,
                        "id" => $user->code,
                        "interval" => "month",
                        "currency" => "usd",
                        "statement_descriptor" => 'SongLine subscription',
                        "amount" => 0,
                    ));
                } catch (\Stripe\Error\Base $e) {
                    dump('Plan creating error ' . $user->code);
                }
                dump('Plan was updated');
            }
            \App\Subscription::truncate();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
