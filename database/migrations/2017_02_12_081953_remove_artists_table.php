<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveArtistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('artists');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('artists')) {
            return;
        }
        Schema::create('artists', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            // Create columns
            $table->char('user_code', 24);
            $table->mediumText('artist_bio')->nullable();
            $table->timestamps();
            // Create indexes
            $table->primary('user_code');
            // Create foreign keys
            $table->foreign('user_code')
                ->references('code')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        // restore available Artist data
        $artists = \DB::table('songs')
            ->select('songs.user_code', 'users.created_at', 'users.bio')->distinct()
            ->join('users', 'users.code', '=', 'songs.user_code')->get();

        foreach ($artists as $artist) {
            \DB::table('artists')->insert([
                'user_code' => $artist->user_code,
                'artist_bio' => $artist->bio,
                'created_at' => $artist->created_at,
                'updated_at' => \Carbon\Carbon::now()
            ]);
        }
    }
}
