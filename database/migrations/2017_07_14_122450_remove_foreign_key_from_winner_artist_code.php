<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveForeignKeyFromWinnerArtistCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('competitions', function (Blueprint $table) {
            $table->dropForeign(['winner_artist_code']);
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('competitions', function (Blueprint $table) {
            $table->foreign('winner_artist_code')->references('code')->on('users')->onUpdate('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }
}
