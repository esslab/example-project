<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveStatusFieldFromGenresTable extends Migration
{
    private $tableName = 'genres';
    private $column = 'status';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn($this->tableName, $this->column)) {
            return;
        }
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->dropColumn($this->column);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn($this->tableName, $this->column)) {
            return;
        }
        Schema::table($this->tableName, function (Blueprint $table) {
            $table->tinyInteger($this->column, false, true)->default(0);
        });
    }
}
