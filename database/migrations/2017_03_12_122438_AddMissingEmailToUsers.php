<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissingEmailToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $auth0 = new \App\Auth0ManagementAPI();
        $jwtUsers = $auth0->getAll([]);
        foreach ($jwtUsers as $jwtUser) {
            if (!empty($jwtUser['email'])) {
                $user = User::find($jwtUser['identities'][0]['user_id']);
                if(!empty($user->code)) {
                    $user->email = $jwtUser['email'];
                    $user->save();
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
