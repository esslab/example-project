<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSharedArtistsStatistic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $songs = \App\Song::with('stat')->get();

        $artistStats = \App\ArtistStat::all();
        foreach ($artistStats as $artistStat) {
            $shared = 0;
            foreach ($songs as $song) {
                if ($artistStat->user_code == $song->user_code) {
                    $shared += $song->stat()->first()->shared;
                }
            }
            $artistStat->shared = $shared;
            $artistStat->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
