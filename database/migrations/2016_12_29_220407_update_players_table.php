<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('players', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->uuid('trackPaused');
            $table->float('timePaused', 10, 6);
            $table->tinyInteger('isShuffle');
            $table->tinyInteger('isRepeat');
            $table->index('user_code');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('players', function (Blueprint $table) {
            $table->dropIndex(['user_code']);
            $table->dropColumn(['trackPaused', 'timePaused', 'isShuffle', 'isRepeat']);
            $table->text('status');
        });
        Schema::enableForeignKeyConstraints();
    }
}
