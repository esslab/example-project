<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissingPlayers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = App\User::all();
        foreach ($users as $user) {
            if (!$user->player()->first()) {
                $newPlayerData = [
                    'timePaused' => 0,
                    'trackPaused' => 0,
                    'tracksOrder' => '',
                    'isShuffle' => 0,
                    'isRepeat' => 0
                ];

                $user->player()->create($newPlayerData);
                dump('Player for user ' . $user->code . ' was created');
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
