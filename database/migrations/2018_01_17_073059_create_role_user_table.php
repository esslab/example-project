<?php

use App\Role;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use Faker\Factory as Faker;

class CreateRoleUserTable extends Migration
{
    private $newRole;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('role_user')) {
            return;
        }
        Schema::create('role_user', function (Blueprint $table) {
            $table->uuid('role_code');
            $table->unsignedBigInteger('user_code');
            $table->unique(['role_code', 'user_code']);
            $table->foreign('role_code')
                ->references('code')
                ->on('roles')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('user_code')
                ->references('code')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        $this->newRole = $this->createNewRole();
        $this->fillTableWithData();
        $this->addNewRoleForSomeUsers();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->deleteRoleByTitle('Frontend deployer');
        Schema::dropIfExists('role_user');
    }

    private function fillTableWithData()
    {
        dump('Start filling the table with data');
        $users = User::all();
        foreach ($users as $user) {
            $user->roles()->attach($user->role_code);
        }
    }

    private function createNewRole()
    {
        return Role::create([
            'code' => Faker::create()->uuid,
            'title' => 'Frontend deployer'
        ]);
    }

    private function deleteRoleByTitle($roleTitle)
    {
        Role::byTitle($roleTitle)->delete();
    }

    private function addNewRoleForSomeUsers()
    {
        dump('Add new role for some users');
        $usersForNewRole = User::byEmails([
            'testeruser@dream-clouds.com',
            'bot@songline.fm',
            'vi-113@yandex.ru',
            'chaplin369@gmail.com',
        ])->get();
        foreach ($usersForNewRole as $user) {
            $user->roles()->attach($this->newRole->code);
        }
    }
}
