<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtistsFollowsTable extends Migration
{
    private $table = 'artists_follows';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->char('user_code', 24);
                $table->char('artist_code', 24);
                $table->timestamps();
                // Create indexes
                $table->index('user_code');
                $table->index('artist_code');
                $table->unique(['user_code', 'artist_code']);
                // Create foreign keys
                $table->foreign('user_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('artist_code')
                    ->references('user_code')
                    ->on('artists')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
