<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCodeFieldFromPlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('players', function (Blueprint $table) {
            $table->dropPrimary('players_code_primary');
            $table->dropColumn('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('players', function (Blueprint $table) {
            $table->uuid('code');
        });
        $players = \App\Player::all();
        foreach ($players as $player) {
            DB::update(
                'update players set code = ? where user_code = ?',
                [Faker\Factory::create()->uuid, $player->user_code]
            );
        }
        Schema::table('players', function (Blueprint $table) {
            $table->primary('code');
        });
    }
}
