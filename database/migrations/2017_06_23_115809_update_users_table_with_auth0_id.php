<?php
use App\Repository\StripeRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableWithAuth0Id extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->deleteUsersAndStripeAccounts();

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('auth0_id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->string('auth0_id', 50)->after('code');
            // indices
            $table->index('auth0_id');
            $table->unique('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex(['auth0_id']);
            $table->dropUnique(['email']);
        });
    }

    private function deleteUsersAndStripeAccounts()
    {
        $users = App\User::all();
        $users->each(function ($user) {
            $user->delete();
        });
        $stripeRepository = new StripeRepository();
        $stripeRepository->deleteAllStripeData();
    }
}
