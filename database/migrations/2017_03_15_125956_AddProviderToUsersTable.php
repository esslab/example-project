<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class AddProviderToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('provider');
        });

        $auth0 = new \App\Auth0ManagementAPI();
        $jwtUsers = true;

        /**
         *  Going through all of the users and updating provider
         */
        for ($i = 0; !empty($jwtUsers); $i++) {
            $jwtUsers = $auth0->getAll([
                'page' => $i,
            ]);
            foreach ($jwtUsers as $jwtUser) {
                $user = User::find($jwtUser['identities'][0]['user_id']);
                if(!empty($user->code)) {
                    $user->provider = $jwtUser['identities'][0]['provider'];
                    $user->save();
                }
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('provider');
        });
    }
}
