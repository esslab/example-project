<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        dump("Starting...");
        \App\Repository\StripeRepository::cancelAllSubscriptions();
        \App\Subscription::truncate();
        dump("All subscriptions was cancelled");
        dump("Updating plan_cost...");
        $users = \App\User::all();
        $users->each(function ($user) {
            $user->plan_cost = 0;
            $user->card_brand = null;
            $user->card_last_four = null;
            $user->save();
        });
        dump("Done");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
