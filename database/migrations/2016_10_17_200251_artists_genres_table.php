<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtistsGenresTable extends Migration
{
    private $table = 'artists_genres';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->char('artist_code', 24);
                $table->uuid('genre_code');
                $table->timestamps();
                // Create indexes
                $table->index('artist_code');
                $table->index('genre_code');
                $table->unique(['artist_code', 'genre_code']);
                // Create foreign keys
                $table->foreign('artist_code')
                    ->references('user_code')
                    ->on('artists')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('genre_code')
                    ->references('code')
                    ->on('genres')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
