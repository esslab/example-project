<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersNameSongsTitleFullTextIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement('ALTER TABLE users ADD FULLTEXT full_name_search (name)');
        DB::statement('ALTER TABLE songs ADD FULLTEXT full_title_search (title)');
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('users', function (Blueprint $table) {
            $table->dropIndex('full_name_search');
        });
        Schema::table('songs', function (Blueprint $table) {
            $table->dropIndex('full_title_search');
        });
        Schema::enableForeignKeyConstraints();

    }
}
