<?php

use App\Repository\StripeRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteStripePlansAndSubscriptionsAgain extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = \App\User::all();
        foreach ($users as $user) {
            $user->plan_cost = $user->plan_cost * 100;
            $user->save();
        }
        $subs = \App\Subscription::all();
        foreach ($subs as $sub) {
            $sub->price = $sub->price * 100;
            $sub->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
