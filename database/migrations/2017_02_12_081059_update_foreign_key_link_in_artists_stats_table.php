<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateForeignKeyLinkInArtistsStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('artists_stats', function (Blueprint $table) {
            $table->dropForeign(['artist_code']);
            $table->renameColumn('artist_code', 'user_code');
            $table->foreign('user_code')->references('code')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('artists_stats', function (Blueprint $table) {
            $table->dropForeign(['user_code']);
            $table->renameColumn('user_code', 'artist_code');
            $table->foreign('artist_code')->references('user_code')->on('artists')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }
}
