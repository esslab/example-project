<?php
use App\Genre;
use Illuminate\Database\Migrations\Migration;

class UpdateGenresTableData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $availableGenres = Genre::all();
        $newGenres = collect(\GenreTableSeeder::getGenres());

        if ($availableGenres->count() === $newGenres->count() && $availableGenres->contains('title', 'Breakbeat')) {
            // if new genres already in database
            return;
        }

        $newGenresToCreate = $newGenres->splice($availableGenres->count());

        if ($availableGenres->isNotEmpty()) {
            foreach ($availableGenres as $k => $availableGenre) {
                $availableGenre->title = $newGenres[$k];
                $availableGenre->save();
            }
        }

        $this->createRemainingGenres($newGenresToCreate);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // will do nothing
    }

    private function createRemainingGenres($newGenresToCreate)
    {
        foreach ($newGenresToCreate as $genre) {
            factory(Genre::class)->create(['title' => $genre, 'status' => 1]);
        }
    }
}
