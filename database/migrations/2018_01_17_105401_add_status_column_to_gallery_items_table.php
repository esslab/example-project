<?php

use App\GalleryItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusColumnToGalleryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function up()
    {
        Schema::table('galleries_items', function (Blueprint $table) {
            $table->unsignedTinyInteger('transcodingCompleted')->nullable();
        });
        GalleryItem::withoutGlobalScopes()->where('type', 'video')->update(['transcodingCompleted' => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     * @throws Throwable
     */
    public function down()
    {
        Schema::table('galleries_items', function (Blueprint $table) {
            $table->dropColumn('transcodingCompleted');
        });
    }
}
