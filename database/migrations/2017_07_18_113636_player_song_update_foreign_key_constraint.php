<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlayerSongUpdateForeignKeyConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('player_song', function (Blueprint $table) {
            $table->dropForeign(['player_code']);
        });
        Schema::table('player_song', function (Blueprint $table) {
            $table->foreign('player_code')
                ->references('user_code')
                ->on('players')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('player_song', function (Blueprint $table) {
            $table->dropForeign(['player_code']);
        });
        Schema::table('player_song', function (Blueprint $table) {
            $table->foreign('player_code')
                ->references('user_code')
                ->on('players')
                ->onUpdate('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }
}
