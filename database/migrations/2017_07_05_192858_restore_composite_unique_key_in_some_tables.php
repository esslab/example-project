<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RestoreCompositeUniqueKeyInSomeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('artists_genres', function (Blueprint $table) {
            $table->dropUnique(['artist_code', 'genre_code']);
        });
        Schema::table('artists_genres', function (Blueprint $table) {
            $table->unique(['user_code', 'genre_code']);
        });

        Schema::table('languages_users', function (Blueprint $table) {
            $table->dropUnique(['lang_code', 'user_code']);
        });
        Schema::table('languages_users', function (Blueprint $table) {
            $table->unique(['lang_code', 'user_code']);
        });

        Schema::table('likes', function (Blueprint $table) {
            $table->dropUnique(['song_code', 'user_code']);
        });
        Schema::table('likes', function (Blueprint $table) {
            $table->unique(['song_code', 'user_code']);
        });

        Schema::table('votes', function (Blueprint $table) {
            $table->dropUnique(['competition_code', 'song_code', 'user_code']);
        });
        Schema::table('votes', function (Blueprint $table) {
            $table->unique(['competition_code', 'song_code', 'user_code']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // reverse migration maybe impossible because of tables data

        Schema::table('artists_genres', function (Blueprint $table) {
            $table->dropUnique(['user_code', 'genre_code']);
        });
        Schema::table('artists_genres', function (Blueprint $table) {
            $table->unique(['user_code', 'genre_code'], 'artists_genres_artist_code_genre_code_unique');
        });
    }
}
