<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUsersAchievementsTable extends Migration
{
    private $table = 'users_achievements';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists($this->table);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function (Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->bigInteger('user_code', false, true);
                $table->uuid('achievement_code');
                $table->timestamps();
                // Create indexes
                $table->unique(['user_code', 'achievement_code']);
                // Create foreign keys
                $table->foreign('user_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('achievement_code')
                    ->references('code')
                    ->on('achievements')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

}
