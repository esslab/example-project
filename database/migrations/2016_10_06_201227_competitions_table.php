<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CompetitionsTable extends Migration
{
    private $table = 'competitions';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('code');
                $table->string('title');
                $table->timestamp('date_start')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->dateTime('date_end')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->uuid('winner_song_code');
                $table->char('winner_artist_code',24);
                $table->string('type', 25)->default('Weekly');
                $table->tinyInteger('status')->default(0);
                $table->timestamps();
                // Create indexes
                $table->primary('code');
                $table->index('winner_song_code');
                $table->index('winner_artist_code');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
