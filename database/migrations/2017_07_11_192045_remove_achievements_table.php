<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAchievementsTable extends Migration
{
    private $table = 'achievements';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists($this->table);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('code');
                $table->string('title');
                $table->mediumText('description');
                $table->string('image_file');
                $table->tinyInteger('status')->default(0);
                $table->string('user_type', 10)->default('all');
                $table->string('duration_type', 20)->default('infinite');
                $table->timestamp('date_start')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->dateTime('date_end')->default(DB::raw('CURRENT_TIMESTAMP'));
                $table->text('condition');
                $table->timestamps();
                // Create indexes
                $table->primary('code');
            });
        }
    }
}
