<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GalleriesItemsTable extends Migration
{
    private $table = 'galleries_items';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('code');
                $table->uuid('gallery_code');
                $table->char('user_code', 24);
                $table->tinyInteger('status')->default(0);
                $table->string('type', 20)->default('picture');
                $table->string('source_file');
                $table->tinyInteger('privacy')->default(0);
                $table->timestamps();
                // Create indexes
                $table->primary('code');
                $table->index('gallery_code');
                $table->index('user_code');
                // Create foreign keys
                $table->foreign('gallery_code')
                    ->references('code')
                    ->on('galleries')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('user_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
