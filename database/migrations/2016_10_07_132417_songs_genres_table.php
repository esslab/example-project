<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SongsGenresTable extends Migration
{
    private $table = 'songs_genres';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('genre_code');
                $table->uuid('song_code');
                $table->timestamps();
                // Create indexes
                $table->index('genre_code');
                $table->index('song_code');
                $table->unique(['genre_code', 'song_code']);
                // Create foreign keys
                $table->foreign('genre_code')
                    ->references('code')
                    ->on('genres')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('song_code')
                    ->references('code')
                    ->on('songs')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
