<?php
use App\Repository\StripeRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTableCodeFieldToAiBigint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->deleteUsersAndStripeAccounts();

        Schema::disableForeignKeyConstraints();
        dump('Begin update foreign keys...');
        $allConstraints = $this->getAllConstraints();

        // Update 'player_song' table
        Schema::table('player_song', function (Blueprint $table) {
            //    delete constraint
            $table->dropForeign(['player_code']);
            //    delete field 'code'
            $table->dropColumn('player_code');
        });
        Schema::table('player_song', function (Blueprint $table) {
            //    create field with new type
            $table->bigInteger('player_code', false, true);
        });
        dump('Table [player_song] updated.');

        foreach ($allConstraints as $constraint) {
            Schema::table($constraint->TABLE_NAME, function (Blueprint $table) use ($constraint) {
                //    delete constraint
                $table->dropForeign($constraint->CONSTRAINT_NAME);
                //    delete field 'user_code'
                $table->dropColumn('user_code');
            });
            Schema::table($constraint->TABLE_NAME, function (Blueprint $table) use ($constraint) {
                //    create field with new type
                $table->bigInteger('user_code', false, true);
            });
            dump('Table [' . $constraint->TABLE_NAME . '] updated.');
        }

        // we need manually update 'competitions', 'subscriptions', 'charges', 'artists_follows' tables

        // Update 'competitions' table
        Schema::table('competitions', function (Blueprint $table) {
            //    delete constraint
            $table->dropIndex(['winner_artist_code']);
            //    delete field 'code'
            $table->dropColumn('winner_artist_code');
        });
        Schema::table('competitions', function (Blueprint $table) {
            //    create field with new type
            $table->bigInteger('winner_artist_code', false, true);
        });
        dump('Table [competitions] updated.');

        // Update 'subscriptions' table
        Schema::table('subscriptions', function (Blueprint $table) {
            //    delete constraint
            $table->dropForeign(['user_id']);
            $table->dropForeign(['stripe_plan']);
            //    delete field 'code'
            $table->dropColumn(['user_id', 'stripe_plan']);
        });
        Schema::table('subscriptions', function (Blueprint $table) {
            //    create field with new type
            $table->bigInteger('user_id', false, true);
            $table->bigInteger('stripe_plan', false, true);
        });
        dump('Table [subscriptions] updated.');

        // Update 'charges' table
        Schema::table('charges', function (Blueprint $table) {
            $table->dropColumn('plan');
        });
        Schema::table('charges', function (Blueprint $table) {
            //    create field with new type
            $table->bigInteger('plan', false, true);
        });
        dump('Table [charges] updated.');

        // Update 'artists_follows' table
        Schema::table('artists_follows', function (Blueprint $table) {
            $table->dropForeign(['artist_code']);
            $table->dropUnique('artists_follows_user_code_artist_code_unique');
            $table->dropIndex(['artist_code']);
            $table->dropColumn('artist_code');
        });
        Schema::table('artists_follows', function (Blueprint $table) {
            //    create field with new type
            $table->bigInteger('artist_code', false, true);
        });
        dump('Table [artists_follows] updated.');

        // Update 'users' table
        Schema::table('users', function (Blueprint $table) {
            //    delete constraint
            $table->dropPrimary(['code']);
            //    delete field 'code'
            $table->dropColumn('code');
        });
        Schema::table('users', function (Blueprint $table) {
            //    create field with new type
            $table->bigIncrements('code')->after('auth0_id');
        });
        dump('Table [users], constraint for [code] updated.');

        // Enable all foreign_keys constraints
        foreach ($allConstraints as $constraint) {
            Schema::table($constraint->TABLE_NAME, function (Blueprint $table) {
                //    create a foreign key constraint
                $table->foreign('user_code')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
            });
            dump('Table [' . $constraint->TABLE_NAME . '], constraint for [user_code] created.');
        }
        Schema::table('competitions', function (Blueprint $table) {
            $table->foreign('winner_artist_code')->references('code')->on('users')->onUpdate('cascade');
        });
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->foreign('user_id')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('stripe_plan')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('charges', function (Blueprint $table) {
            $table->foreign('plan')->references('code')->on('users')->onUpdate('cascade');
        });
        Schema::table('player_song', function (Blueprint $table) {
            $table->foreign('player_code')->references('user_code')->on('players')->onUpdate('cascade');
        });
        Schema::table('artists_follows', function (Blueprint $table) {
            $table->foreign('artist_code')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['user_code', 'artist_code']);
        });
        dump('Foreign key constraints created for tables competitions, subscriptions, charges...');

        // start autoincrement from some value
        $sql = 'alter table users auto_increment = 10000000';
        \DB::statement($sql);
        dump('auto_increment is set for the users table.');
        Schema::enableForeignKeyConstraints();
        dump('Done.');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        dump('Begin update foreign keys...');
        $allConstraints = $this->getAllConstraints();

        // Restore 'player_song' table
        Schema::table('player_song', function (Blueprint $table) {
            $table->dropForeign(['player_code']);
            $table->dropColumn('player_code');
        });
        Schema::table('player_song', function (Blueprint $table) {
            $table->string('player_code', 24);
        });
        dump('Table [player_song] restored.');

        foreach ($allConstraints as $constraint) {
            Schema::table($constraint->TABLE_NAME, function (Blueprint $table) use ($constraint) {
                $table->dropForeign($constraint->CONSTRAINT_NAME);
                $table->dropColumn('user_code');
            });
            Schema::table($constraint->TABLE_NAME, function (Blueprint $table) use ($constraint) {
                $table->string('user_code', 24);
            });
            dump('Table [' . $constraint->TABLE_NAME . '] restored.');
        }

        // we need manually update 'competitions', 'subscriptions', 'charges', 'artists_follows' tables

        // Restore 'competitions' table
        Schema::table('competitions', function (Blueprint $table) {
            $table->dropForeign(['winner_artist_code']);
            $table->dropColumn('winner_artist_code');
        });
        Schema::table('competitions', function (Blueprint $table) {
            $table->string('winner_artist_code', 24);
        });
        dump('Table [competitions] restored.');

        // Restore 'subscriptions' table
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['stripe_plan']);
            $table->dropColumn(['user_id', 'stripe_plan']);
        });
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('user_id', 24);
            $table->string('stripe_plan', 24);
        });
        dump('Table [subscriptions] restored.');

        // Restore 'charges' table
        Schema::table('charges', function (Blueprint $table) {
            $table->dropForeign(['plan']);
            $table->dropColumn('plan');
        });
        Schema::table('charges', function (Blueprint $table) {
            $table->string('plan', 24);
        });
        dump('Table [charges] restored.');

        // Restore 'artists_follows' table
        Schema::table('artists_follows', function (Blueprint $table) {
            $table->dropForeign(['artist_code']);
            $table->dropUnique('artists_follows_user_code_artist_code_unique');
            $table->dropColumn(['artist_code']);
        });
        Schema::table('artists_follows', function (Blueprint $table) {
            $table->string('artist_code', 24);
        });
        dump('Table [artists_follows] restored.');

        // Restore 'users' table
        $sql = "ALTER TABLE users
                DROP PRIMARY KEY,
                CHANGE code code VARCHAR(24) COLLATE utf8_unicode_ci NOT NULL,
                ADD PRIMARY KEY (code);";
        \DB::statement($sql);
        dump('Table [users], constraint for [code] restored.');

        // Enable all foreign_keys constraints
        foreach ($allConstraints as $constraint) {
            Schema::table($constraint->TABLE_NAME, function (Blueprint $table) {
                $table->foreign('user_code')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
            });
            dump('Table [' . $constraint->TABLE_NAME . '], constraint for [user_code] created.');
        }

        Schema::table('competitions', function (Blueprint $table) {
            $table->foreign('winner_artist_code')->references('code')->on('users')->onUpdate('cascade');
        });
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->foreign('user_id')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('stripe_plan')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('charges', function (Blueprint $table) {
            $table->foreign('plan')->references('code')->on('users')->onUpdate('cascade');
        });
        Schema::table('player_song', function (Blueprint $table) {
            $table->foreign('player_code')->references('user_code')->on('players')->onUpdate('cascade');
        });
        Schema::table('artists_follows', function (Blueprint $table) {
            $table->foreign('artist_code')->references('code')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->unique(['user_code', 'artist_code']);
        });
        dump('Foreign key constraints created for tables competitions, subscriptions, charges...');

        Schema::enableForeignKeyConstraints();
        dump('Done.');
    }

    private function getAllConstraints()
    {
        $sql = "select TABLE_NAME, CONSTRAINT_NAME
                    from INFORMATION_SCHEMA.TABLE_CONSTRAINTS
                    where CONSTRAINT_TYPE = 'FOREIGN KEY'
                        and CONSTRAINT_SCHEMA = '". env("DB_DATABASE") ."'
                        and CONSTRAINT_NAME like '%user_code%';";

        return $res = \DB::select($sql);
    }

    private function deleteUsersAndStripeAccounts()
    {
        dump("Starting delete all Users ...");
        $users = App\User::all();
        $users->each(function ($user) {
            $user->delete();
        });
        dump("Users were deleted from DB.");

        $stripeRepository = new StripeRepository();
        dump("Start deleting Users from Stripe ...");
        $stripeRepository->deleteAllStripeData();
        dump("Users were deleted from Stripe.");
    }
}
