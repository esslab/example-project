<?php

use Illuminate\Database\Migrations\Migration;

class UpdateCompetitionsWinnerArtistCodeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! App::environment('production')) return;

        $competitions = \App\Competition::all();
        if ($competitions->isEmpty()) {
            echo "Competitions table is empty.";
            return;
        }

        $filePath = '/data-migration/competitions.json';
        if (! \Storage::exists($filePath)) {
            echo "$filePath didn't exists.";
            return;
        }
        $competitionsListFromFile = json_decode(\Storage::get($filePath));

        foreach ($competitionsListFromFile as $competition) {
            $this->updateWinnerArtistCode($competition);
            foreach ($competition->weekly_list as $weeklyCompetition) {
                $this->updateWinnerArtistCode($weeklyCompetition);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    private function getArtistByEmail($artistEmail)
    {
        $user = \App\User::where('email', $artistEmail)->first();
        if (is_null($user)) {
            return '';
        }
        return $user->code;
    }

    private function updateWinnerArtistCode($competitionFromFile)
    {
        $userCode = $this->getArtistByEmail($competitionFromFile->winner_artist_code);
        $songlineCompetition = \App\Competition::whereDate(
            'date_start',
            \Carbon\Carbon::createFromFormat('d.m.Y', $competitionFromFile->date_start)->toDateString()
        )->where('type', $competitionFromFile->type)->first();
        $songlineCompetition->winner_artist_code = $userCode;
        $songlineCompetition->save();
        return true;
    }
}
