<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanCostColumnToInteger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->unsignedInteger('plan_cost')->change();
        });
        Schema::table('subscriptions', function(Blueprint $table) {
            $table->unsignedInteger('price')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->decimal('plan_cost',8,2)->change();
        });
        Schema::table('subscriptions', function(Blueprint $table) {
            $table->decimal('price',8,2)->change();
        });
    }
}
