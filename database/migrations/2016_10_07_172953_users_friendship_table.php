<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersFriendshipTable extends Migration
{
    private $table = 'users_friendship';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->char('sender_code', 24);
                $table->char('recipient_code', 24);
                $table->tinyInteger('status')->default(0);
                $table->string('message');
                $table->timestamps();
                // Create indexes
                $table->index('sender_code');
                $table->index('recipient_code');
                $table->unique(['sender_code', 'recipient_code']);
                // Create foreign keys
                $table->foreign('sender_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('recipient_code')
                    ->references('code')
                    ->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
