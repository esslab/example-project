<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SwitchPrimaryKeyOnUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::table('subscriptions', function (Blueprint $table) {
                $table->foreign('stripe_plan')->references('code')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('user_id')->references('code')->on('users')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        
            $users = App\User::all();
            foreach ($users as $user) {
                $code = $user->code;
                $user->code = $user->auth0_id;
                $user->auth0_id = $code;
                $user->save();
                dump('User\'s key ' . $user->code . ' was swapped');
            }
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            $users = \App\User::all();
            foreach ($users as $user) {
                $code = $user->code;
                $user->code = $user->auth0_id;
                $user->auth0_id = $code;
                $user->save();
                dump('User\'s key ' . $user->code . ' was swapped');
            }
            Schema::table('subscriptions', function (Blueprint $table) {
                $table->dropForeign(['stripe_plan']);
                $table->dropForeign(['user_id']);
            });
        });
    }
}
