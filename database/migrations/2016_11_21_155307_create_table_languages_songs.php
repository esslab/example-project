<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLanguagesSongs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('languages_songs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            
            $table->uuid('song_code');
            $table->uuid('lang_code');
            $table->timestamps();
            // Create indexes
            $table->index('lang_code');
            $table->index('song_code');
            $table->unique(['lang_code', 'song_code']);
            // Create foreign keys
            $table->foreign('lang_code')
                ->references('code')
                ->on('languages')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('song_code')
                ->references('code')
                ->on('songs')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('languages_songs');
    }
}
