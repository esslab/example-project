<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatisticColumnsToCompetitionList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competitions_list', function (Blueprint $table) {
            $table->unsignedInteger('liked')->default(0);
            $table->unsignedInteger('added_to_playlist')->default(0);
            $table->unsignedInteger('played')->default(0);
            $table->unsignedInteger('voted')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competitions_list', function (Blueprint $table) {
            $table->dropColumn(['liked', 'added_to_playlist', 'played', 'voted']);
        });
    }
}
