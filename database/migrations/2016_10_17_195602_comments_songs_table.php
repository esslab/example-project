<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentsSongsTable extends Migration
{
    private $table = 'comments_songs';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->table)) {
            Schema::create($this->table, function(Blueprint $table) {
                $table->engine = 'InnoDB';
                // Create columns
                $table->uuid('song_code');
                $table->uuid('comment_code');
                $table->timestamps();
                // Create indexes
                $table->index('song_code');
                $table->index('comment_code');
                $table->unique(['song_code', 'comment_code']);
                // Create foreign keys
                $table->foreign('song_code')
                    ->references('code')
                    ->on('songs')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
                $table->foreign('comment_code')
                    ->references('code')
                    ->on('comments')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
