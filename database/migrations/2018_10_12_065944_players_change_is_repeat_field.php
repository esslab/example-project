<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlayersChangeIsRepeatField extends Migration
{
    private $table = 'players';
    private $field = 'isRepeat';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn($this->table, $this->field)) {
            return;
        }
        Schema::table($this->table, function (Blueprint $table) {
            $table->renameColumn($this->field, 'repeatState');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasColumn($this->table, 'repeatState')) {
            return;
        }
        Schema::table($this->table, function (Blueprint $table) {
            $table->renameColumn('repeatState', $this->field);
        });
    }
}
