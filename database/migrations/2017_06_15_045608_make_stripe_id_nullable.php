<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeStripeIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function(Blueprint $table) {
            $table->string('stripe_id')->nullable()->change();
        });
        dump("Start deleting subscriptions...");
        \App\Repository\StripeRepository::cancelAllSubscriptions();
        \App\Subscription::truncate();
        dump("All subscriptions was cancelled");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function(Blueprint $table) {
            $table->string('stripe_id')->change();
        });
    }
}
