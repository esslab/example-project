<?php

use App\Repository\UserRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class AddIdColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->char('auth0_id', 24);
        });
        DB::update("ALTER TABLE users AUTO_INCREMENT = 10000000;");

        $users = User::all();
        $i = 10000000;
        foreach ($users as $user) {
            $user->auth0_id = $user->code;
            $user->code = $i++;
            $user->save();
        }
        dump('End migrating users');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $users = User::all();
            foreach ($users as $user) {
                $user->code = $user->auth0_id;
            }
            $table->dropColumn('auth0_id');
        });
    }
}
