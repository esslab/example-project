<?php

Route::group([
    'prefix' => 'web',
], function () {
    /******************* Profile routes *************************/
    Route::group([
        'prefix' => 'profile/{user}',
    ], function () {
        Route::group([], function () {
            Route::get('/', "StaticPagesController@getArtist");
            Route::get('/about', "StaticPagesController@getArtist");
            Route::get('/music/{song}', "StaticPagesController@getSong");
            Route::get('/playlists/{playlist}', "StaticPagesController@getPlaylist");
            Route::get('/gallery/{gallery}', "StaticPagesController@getGalleryItem");
        });
    });
});
