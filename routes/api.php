<?php
Route::get('/auth', 'UserController@getCode')->middleware('auth0.jwt');

/******************* Temporary rest  *************************/
Route::get('test', 'GenresController@all');

/******************* Genres routes *************************/
Route::get('genres', 'GenresController@all');

/******************* Languages routes **********************/
Route::get('languages', 'LanguagesController@all');

/******************* Player routes *************************/
Route::group([
    'prefix' => 'player',
    'middleware' => 'auth0.jwt'
], function () {
    Route::get('/', "PlayerController@getStatus");
    Route::put('/', "PlayerController@updateStatus");
});

/******************* Configs routes *************************/
Route::get('/configs', "ConfigController@getS3Config")->middleware('auth0.jwt');

/******************* Premium content routes *************************/
Route::group([
    'prefix' => 'subscriptions',
    'middleware' => 'auth0.jwt'
], function () {
    Route::delete('/{subscription}', "SubscriptionsController@unsubscribe");
    Route::put('/{subscription}/notification', "SubscriptionsController@markNotificationAsRead");
    Route::post('', "SubscriptionsController@subscribe");
});
Route::group([
    'prefix' => 'users/{user}',
], function () {
    Route::group([
        'middleware' => 'soft.auth0.jwt'
    ], function () {
        Route::get('subscriptions', "SubscriptionsController@getAllSubscriptions");
        Route::get('subscribers', "SubscriptionsController@getSubscribers");
        Route::get('plans', "SubscriptionsController@getPlan");
    });

    Route::put('plans', "SubscriptionsController@editPlan")->middleware('auth0.jwt');
});

Route::post('stripe/webhook', 'WebhookController@handleWebhook');
Route::get('stripe/webhook/test', 'WebhookController@testWebhook');

/******************* Wallet ************************/
Route::get('wallet', "SubscriptionsController@getWallet")->middleware('auth0.jwt');

/******************* Users and Artists ************************/
Route::get('/access-token', "UserController@getAccessToken")->middleware('auth0.jwt');
Route::get('/favorites-artists-content', 'SongsController@getFavoriteArtistsSongs')->middleware('auth0.jwt');

/*********************** Settings *****************************/
Route::group([
    //TODO: get rid of /user/{user}
    'prefix' => '/user/{user}',
    'middleware' => 'auth0.jwt'
], function () {
    Route::get('/payment-settings', "SubscriptionsController@getCardCredentials");
    Route::put('/payment-settings', "SubscriptionsController@updateCardDetails");
    Route::delete('/payment-settings', "SubscriptionsController@deleteCard");

    Route::get('/profile-settings', "UserController@getProfileSettings");
    Route::put('/profile-settings', "UserController@setProfileSettings");

    Route::put('/payout-settings', "UserController@savePayoutDetails");
    Route::get('/payout-settings', "UserController@getPayoutDetails");
});

Route::get('artists', "UserController@getArtists")->middleware('soft.auth0.jwt');
Route::group([
    'prefix' => 'artists',
    'middleware' => 'auth0.jwt'
], function () {
    Route::post('/{user}/followers', "UserController@followArtist");
    Route::delete('/{user}/followers', "UserController@unfollowArtist");
});
/******************* Playlists routes *************************/
Route::group([
    'prefix' => 'playlists',
    'middleware' => 'auth0.jwt'
], function () {
    Route::patch('/{playlist}', "PlaylistController@update");
    Route::delete('/{playlist}', "PlaylistController@destroy");
    Route::post('/', "PlaylistController@store");
    Route::get('/', "PlaylistController@index");
    Route::post('/{playlist}/songs/', "PlaylistController@addSong");
    Route::delete('/{playlist}/songs/{song}', "PlaylistController@deleteSong");
});

/******************* Songs routes *************************/
Route::get('/songs', 'SongsController@getSongs')->middleware('soft.auth0.jwt');

Route::group([
    'prefix' => 'songs',
    'middleware' => 'auth0.jwt'
], function () {
    Route::post('/', "SongsController@saveSong");
    Route::put('/{song}', 'SongsController@updateSong');
    Route::delete('/{song}', "SongsController@deleteSong");
    Route::post('/{song}/likes', 'SongsController@likeAdd');
    Route::delete('/{song}/likes', 'SongsController@likeRemove');
    Route::post('/{song}/votes', 'SongsController@vote');
    Route::delete('/{song}/votes', 'SongsController@unvote');
});

Route::post('songs/{song}/shares', 'SongsController@shareAdd');
Route::post('songs/{song}/plays', 'SongsController@playAdd');

/******************* Profile routes *************************/
Route::group([
    'prefix' => 'profile/{user}',
], function () {
    Route::group([
        'middleware' => 'soft.auth0.jwt'
    ], function () {
        Route::get('/', "ProfileController@getProfile");
        Route::get('/playlists', "ProfileController@getPlaylists");
        Route::get('/followers', "ProfileController@getFollowers");
        Route::get('/favorites', "ProfileController@getFavoriteArtists");
        Route::get('/media', "GalleryController@getMediaItems");
        Route::get('/media/{uuid}', "GalleryController@getMediaItemById");
    });

    Route::group([
        'middleware' => 'auth0.jwt'
    ], function () {
        Route::delete('/', "ProfileController@deleteProfile");
        Route::post('/media', "GalleryController@saveMediaItem");
        Route::put('/media/{item}', "GalleryController@updateMediaItem");
        Route::delete('/media/{uuid}', "GalleryController@deleteMediaItem");
    });
});

/******************** Media likes ***********************/
Route::post('/media/{uuid}/likes', 'GalleryController@likeAdd')->middleware('auth0.jwt');
Route::delete('/media/{uuid}/likes', 'GalleryController@likeRemove')->middleware('auth0.jwt');

/******************* AWS routes *************************/
Route::put('/media/{media_item}', "GalleryController@lambdaUpdateMediaItem");
Route::put('/songs/{song}/status', 'SongsController@updateSongStatus');

/******************* Competitions Routes *******************/
Route::group([
    'prefix' => 'competitions'
], function () {
    Route::group([
        'middleware' => 'soft.auth0.jwt'
    ], function () {
        Route::get('', 'CompetitionController@getCollection');
        Route::get('/weekly', 'CompetitionController@getCurrentWeekly');
        Route::get('/bimonthly', 'CompetitionController@getCurrentBimonthly');
        Route::get('/yearly', 'CompetitionController@getCurrentYearly');
        Route::get('/{uuid}', 'CompetitionController@getSingle');
    });
});

/******************* Search route *************************/
Route::get('/search', "SearchController@searchSongsOrArtists")->middleware('soft.auth0.jwt');

/******************* Admin route *************************/
Route::group([
    'middleware' => 'auth0.jwt',
    'prefix' => 'admin'
], function () {
    Route::get('/payouts', "AdminController@payouts");
    Route::get('/competitions', "AdminController@getCompetitions");
    Route::get('/competitions/{type}', "AdminController@getCompetitionsByType");
    Route::get('/competitions/{competition}', "AdminController@getCompetition");
    Route::get('/songs/{song}/competitions', "AdminController@getSongCompetitions");
    Route::post('competitions/{competition}/songs', "AdminController@addSongToCompetition");
    Route::delete('competitions/{competition}/songs/{song}', "AdminController@removeSongFromCompetition");
    Route::patch('competitions/{competition}/songs/{song}', "AdminController@updateSongStatsInCompetition");
    Route::put('competitions/{competition}', "AdminController@updateCompetition");
});
/******************* Notifications routes *************************/
Route::group([
    'middleware' => 'auth0.jwt',
    'prefix'     => 'notifications'
], function () {
    Route::get('/', "UserController@getNotifications");
    Route::put('/{notification}/read', "UserController@markNotificationAsRead");
    Route::put('/subscription-price-changed', "UserController@markNotificationsSubscriptionPriceChangedAsRead");
});
/******************* Services routes *************************/
Route::any('/healthcheck', "ServiceController@healthcheck");
