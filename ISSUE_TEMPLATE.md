**1. Short description:**
<!--Please describe a problem detailed as possible-->

**2. Request options:**
<!--route/url, response status code, response message-->

- **route:** 
<!--where the request was sent (e.g `/api/competitions`)-->
- **response status code:** 
<!--you can find it here: Chrome tab or Postman (e.g `404 Not found`)-->
- **response message:** 
<!--you can find it here: Chrome tab or Postman (e.g `Token is empty`)-->
- **Authorization:** 
<!--_User/Admin/Not required_  does the request need some sort of authenication (e.g user is authorized or not, admin user or regular)-->

**3. Sentry service link for error:**
<!--If the error was reported to the Sentry service, please, provide it here-->

**4. Expected result:**
<!--What you expect to get from response? (e.g - _the collection of the competitions_ )-->

**5. Actual result:**
<!--What you really get from response (e.g - _competition was not found_)-->

---
_PS: please, always format your issue description, it will help better understand it._ 

_PSS: Use code-blocks, lists, bold, comments, links and other elements of formatting. Checkout **shortcats** to increase the speed of formatting._ 
