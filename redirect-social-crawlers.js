//Node.js 6.10

'use strict';

exports.handler = (event, context, callback) => {
    var https = require('https');
    const headers = event.Records[0].cf.request.headers;
    var uri = event.Records[0].cf.request.uri;
    const querystring = event.Records[0].cf.request.querystring;
    if(querystring) {
        uri = uri + '?' + querystring;
    }
    console.log(uri);

    var agent = headers["user-agent"][0].value;
    if (agent.includes('Twitterbot') || agent.includes('facebookexternalhit') || agent.includes('Slackbot') || agent.includes('TelegramBot')) {
        console.log('Crawler Detected');
        console.log(agent);
        https.get('https://songline.fm/web' + uri, function(res) {
            console.log('STATUS: ' + res.statusCode);
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                //JSON.stringify(res.headers)
                var response = {
                    body: chunk,
                    bodyEncoding: 'text',
                    headers: {
                        'content-type': [{
                            key: 'Content-Type',
                            value: 'text/html'
                        }]
                    },
                    status: res.statusCode,
                    statusDescription: 'Crawler\'s content'
                };
                callback(null, response);
            });
        }).on('error', function(e) {
            console.error(e);
        }).end();
    }
    else {
        console.log('User-Agent Real Request');
        console.log(agent);
        var response = event.Records[0].cf.request;
        callback(null, response);
    }
};
