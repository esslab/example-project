<?php
namespace App\Validators;

use App\Competition;
use App\CompetitionsList;
use App\Services\CompetitionService;
use App\Song;

class CompetitionValidator
{
    /**
     * Return false if competition of type Top50 and has status 2 (Closed)
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function notClosedTop50($attribute, $value, $parameters, $validator)
    {
        $competitionStatus = $parameters[0];
        $competitionType = $parameters[1];
        return !($competitionType === 'Top50' && $competitionStatus == '2');
    }

    /**
     * Return false if number of songs >= 6 for competitions of type Top6 (not Top50)
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function checkSongsCount($attribute, $value, $parameters, $validator)
    {
        $competitionCode = $parameters[0];
        $competitionType = $parameters[1];
        if ($competitionType === 'Top50') {
            return true;
        }
        $songsCount = Competition::find($competitionCode)->songs()->count();
        return $songsCount < 6;
    }

    /**
     * Return false if the Song or Artist (only Top6) of the song already participate in competition
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function isSongOrArtistInCompetition($attribute, $value, $parameters, $validator)
    {
        $competitionCode = $parameters[0];
        $competitionType = $parameters[1];

        if ($competitionType === 'Top50') {
            // For Top50 we only check if the Song already in competition
            return !CompetitionsList::where(['competition_code' => $parameters[0], $attribute => $value])->exists();
        } else {
            $song = Song::find($value);
            $competition = Competition::find($competitionCode);
            $competitionService = new CompetitionService;
            return !$competitionService->isSongOrArtistInCompetition($song, $competition);
        }
    }

    /**
     * Return true if competition type = Weekly and Status = 0 (planned)
     * @param $attribute
     * @param $value
     * @param $parameters
     * @param $validator
     * @return bool
     */
    public function plannedWeeklyCompetition($attribute, $value, $parameters, $validator)
    {
        $competition = Competition::find($value);
        return ($competition->type === 'Weekly' && $competition->status === 0);
    }
}
