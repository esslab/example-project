<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $primaryKey = 'user_code';

    public $incrementing = false;

    protected $guarded = ['user_code'];

    protected $hidden = ['created_at', 'updated_at', 'tracksOrder', 'user_code'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_code', 'code');
    }

    public function songs()
    {
        return $this->belongsToMany(Song::class, 'player_song', 'player_code', 'song_code')->withTimestamps();
    }
}
