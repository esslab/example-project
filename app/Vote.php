<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $primaryKey = null;

    public $incrementing = false;

    protected $table = self::TABLE_NAME;

    const TABLE_NAME = 'votes';

    const FIELD_COMPETITION_CODE = 'competition_code';
    const FIELD_SONG_CODE = 'song_code';
    const FIELD_USER_CODE = 'user_code';
    const FIELD_CREATED_AT = 'created_at';

    public function scopeByUserToSong($query, User $user, Song $song)
    {
        return $query->where('user_code', $user->code)->where('song_code', $song->code);
    }

    public function scopeInActiveCompetition($query)
    {
        $competitions = Competition::activeSick6()->get()->pluck('code');
        return $query->whereIn('competition_code', $competitions->all());
    }
}
