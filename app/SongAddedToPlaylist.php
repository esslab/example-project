<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongAddedToPlaylist extends Model
{
    protected $table = 'song_added_to_playlists';
    protected $guarded = '*';
    public $incrementing = false;
}
