<?php

namespace App\Events;

use App\User;
use Illuminate\Queue\SerializesModels;

class ArtistWinCompetition
{
    use SerializesModels;
    /**
     * @var User
     */
    public $artist;

    /**
     * Create a new event instance.
     *
     * @param $artistCode
     * @internal param User $artist
     */
    public function __construct($artistCode)
    {
        $this->artist = User::findOrFail($artistCode);
    }
}
