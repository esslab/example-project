<?php
namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Artist;
use App\Classes\Traits\Events\ArtistStats;
use UnexpectedValueException;

class ArtistSubscribe
{
    use SerializesModels, ArtistStats;

    /**
     * @var Artist
     */
    protected $artist;

    /**
     * ArtistSubscribe constructor.
     * Creates a new event instance.
     *
     * @param Artist $artist
     * @throws UnexpectedValueException
     */
    public function __construct($artist)
    {
        if (!$this->checkIfArtistHasRelatedStats($artist)) {
            throw new UnexpectedValueException('Artist must be an instance of User.');
        }

        $this->artist = $artist;
    }

    /**
     * @return Artist
     */
    public function getArtist()
    {
        return $this->artist;
    }
}
