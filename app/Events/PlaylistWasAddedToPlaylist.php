<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * @property array songsIds
 */
class PlaylistWasAddedToPlaylist
{
    use InteractsWithSockets, SerializesModels;

    public $songsIds;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $songsIds)
    {
        $this->songsIds = $songsIds;
    }
}
