<?php

namespace App\Events;

use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserCreated
{
    use InteractsWithSockets, SerializesModels;
    /**
     * @var User
     */
    public $user;
    public $price;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param $price
     */
    public function __construct(User $user, int $price = 0)
    {
        $this->user = $user;
        $this->price = $price;
    }
}
