<?php
namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\User;
use App\Classes\Traits\Events\ArtistStats;
use UnexpectedValueException;

class ArtistUnSubscribe
{
    use SerializesModels, ArtistStats;

    /**
     * @var User
     */
    protected $artist;

    /**
     * ArtistSubscribe constructor.
     * Creates a new event instance.
     *
     * @param User $artist
     * @throws UnexpectedValueException
     */
    public function __construct($artist)
    {
        if (!$this->checkIfArtistHasRelatedStats($artist)) {
            throw new UnexpectedValueException('Artist must be an instance of User.');
        }

        $this->artist = $artist;
    }

    /**
     * @return User
     */
    public function getArtist()
    {
        return $this->artist;
    }
}
