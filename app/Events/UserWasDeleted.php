<?php

namespace App\Events;

use App\Repository\UserRepository;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserWasDeleted
{
    use InteractsWithSockets, SerializesModels;
    /**
     * @var UserRepository
     */
    public $repository;
    public $userCode;
    public $auth0_id;

    /**
     * UserWasDeleted constructor.
     * @param $userCode
     * @param $auth0Id
     * @param UserRepository $repository
     */
    public function __construct($userCode, $auth0Id, UserRepository $repository)
    {
        $this->userCode = $userCode;
        $this->repository = $repository;
        $this->auth0_id = $auth0Id;
    }
}
