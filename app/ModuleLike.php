<?php
namespace App;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModuleLike
 * @method static Builder findByUserAndSong ($user, $song)
 * @see Like::scopeFindByUserAndSong
 * @package App
 * @todo this class can be divided into several classes in different modules,
 * @todo because this class adds some extra functionality to the base model
 */
class ModuleLike extends Like
{
    // TODO: seems like it copy of Like->scopeByUserToSong()
    public function scopeFindByUserAndSong($query, $user, $song)
    {
        return $query->where('user_code', '=', $user)
            ->where('song_code', '=', $song);
    }
}
