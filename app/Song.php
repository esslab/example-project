<?php

namespace App;

use App\Services\ImageService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Classes\Database\Eloquent\SongsCollection;

/**
 * Class Song
 *
 * @method Builder notPremium()|static Song notPremium()
 * @see Song::scopeNotPremium()
 * @method Builder whereUserCode($code)|static Song whereUserCode($code)
 */
class Song extends Model
{
    use SoftDeletes;

    protected $primaryKey = "code";

    protected $fillable = [
        'code',
        'file',
        'cover',
        'duration',
        'title',
        'producer',
        'can_download',
        'can_listen',
        'transcodingCompleted'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    public $incrementing = false;

    protected $hidden = [
        'created_at',
        'updated_at',
        'user_code',
        'pivot',
        'deleted_at',
        'can_download',
        'competitionsStats',
        'activeSick6',
        'transcodingCompleted'
    ];
    protected $with = ['activeSick6', 'stat'];

    protected $appends = ['available_actions', 'deleted', 'sick6', 'type', 'time_difference', 'sick6_stat'];

    protected $table = self::TABLE_NAME;

    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';

    const TABLE_NAME = 'songs';

    public function getDeletedAttribute()
    {
        return $this->trashed();
    }

    public function genres()
    {
        return $this->belongsToMany('App\Genre', 'songs_genres', 'song_code', 'genre_code')
            ->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(Artist::class, 'user_code', 'code');
    }

    public function stat()
    {
        return $this->hasOne('App\SongStat', 'song_code', 'code');
    }

    public function getSick6StatAttribute()
    {
        if ($this->sick6) {
            $competition = $this->activeSick6()->where('song_code', $this->code)->first();
            return $this
                ->competitionsStats()
                ->where('competition_code', $competition->code)
                ->first();
        }
        return [];
    }

    public function competitionsStats()
    {
        return $this->hasMany(CompetitionsList::class, 'song_code', 'code');
    }

    public function playlists()
    {
        return $this->belongsToMany('App\Playlist', 'playlists_songs', 'song_code', 'playlist_code')
            ->withTimestamps()
            ->withPivot('privacy');
    }

    public function likes()
    {
        return $this->hasMany(Like::class, 'song_code', 'code');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language', 'languages_songs', 'song_code', 'lang_code')
            ->withTimestamps();
    }

    public function competitions()
    {
        return $this->belongsToMany(Competition::class, 'competitions_list', 'song_code', 'competition_code')
            ->withPivot(['liked', 'played', 'voted', 'added_to_playlist'])
            ->withTimestamps();
    }

    public function activeSick6()
    {
        return $this->belongsToMany(Competition::class, 'competitions_list', 'song_code', 'competition_code')
            ->withPivot(['liked', 'played', 'voted', 'added_to_playlist'])
            ->activeSick6()
            ->withTimestamps();
    }

    public function activeCompetitions()
    {
        return $this->belongsToMany(Competition::class, 'competitions_list', 'song_code', 'competition_code')
            ->where('status', 1)
            ->withPivot('liked', 'played', 'voted', 'added_to_playlist')
            ->withTimestamps();
    }

    public function players()
    {
        return $this->belongsToMany(Player::class, 'player_song', 'song_code', 'player_code')->withTimestamps();
    }

    public function getAvailableActionsAttribute()
    {
        $authCheck = Auth::check();
        $user = Auth::user();
        return [
            'like'    => $authCheck ? $user->can('like', $this) : false,
            'dislike' => $authCheck ? $user->can('dislike', $this) : false,
            'vote'    => $authCheck ? $user->can('vote', $this) : false,
            'unvote'  => $authCheck ? $user->can('unvote', $this) : false,
            'listen'  => $authCheck ? $user->can('listen', $this) : ($this->can_listen === 'all' && !$this->deleted),

            /*
             * Leave this property in comment
             * Now we have no such a functionality but in version 2 of the App we will return it back
             *
             * */
            // 'download' => Auth::check() ? Auth::user()->can('download', $this) :
            //     $this->can_download == 'all' && !$this->deleted,
        ];
    }

    public function getSick6Attribute()
    {
        return $this->activeSick6->isNotEmpty();
    }

    public function getTypeAttribute()
    {
        return 'song';
    }

    public function getFileAttribute($file)
    {
        if ($this->available_actions['listen'] && !$this->deleted) {
            $file = str_replace_first(config('app.app_files_url'), '', $file);
            return $this->transcodingCompleted ?
                str_replace(['source', '.mp3', '.m4a'], ['converted', '_192k.mp3', '_192k.mp3'], $file) : $file;
        }

        return null;
    }

    public function scopeOrderByTracksOrder($query, $order)
    {
        if ($order) {
            $query->orderBy(DB::raw("FIELD(songs.code, {$order})"));
        }
    }

    //@todo With new project structure this should be transfered to different place (maybe Service or Domain model)
    /**
     * Return array of unique Artists ids by songs ids
     * @param $ids
     * @return array
     */
    public function getUniqueArtistsIdsBySongsIds(array $ids)
    {
        return $this->whereIn('code', $ids)->pluck('user_code')->unique()->toArray();
    }

    public function scopeSearchByTitle($query, $search)
    {
        return $query->select(DB::raw('songs.*, MATCH(songs.title) AGAINST(? IN BOOLEAN MODE) as relev'))
            ->whereRaw("MATCH(songs.title) AGAINST(? IN BOOLEAN MODE)", [$search, $search])
            ->orderBy('relev', 'desc');
    }

    public function plays()
    {
        return $this->hasMany(SongPlay::class, 'song_code', 'code');
    }

    /**
     * Extending a standard collection with new functionality
     * @param array $models
     * @return SongsCollection
     */
    public function newCollection(array $models = [])
    {
        return new SongsCollection($models);
    }

    public function addedToPlaylists()
    {
        return $this->hasMany(SongAddedToPlaylist::class, 'song_code', 'code');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeNotPremium($query)
    {
        return $query->where('can_listen', 'all');
    }

    public function getCoverAttribute($value)
    {
        return empty($value) ? $value : ImageService::getCoverThumbDimensions($value);
    }

    public function setFileAttribute($value)
    {
        $convertedFileName = str_replace(config('app.app_bucket_url'), config('app.app_files_url'), $value);
        $this->attributes['file'] = $convertedFileName;
    }

    public function setCoverAttribute($value)
    {
        $convertedCoverName = str_replace(config('app.app_bucket_url'), config('app.app_files_url'), $value);
        $this->attributes['cover'] = $convertedCoverName;
    }

    /**
     * Returns a time difference between current time and created_at
     * @return int
     */
    public function getTimeDifferenceAttribute()
    {
        return Carbon::now()->diffInSeconds($this->created_at);
    }
}
