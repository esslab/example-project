<?php

namespace App\Console\Commands;

class CreateEmptyUsersStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:add-stat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add missing statistic entry';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = \App\User::all();
        foreach ($users as $user) {
            if ($user->stat()->get()->isEmpty()) {
                $user->stat()->create([]);
            }
        }
        $this->info("Add missing statistic - Done");
    }
}
