<?php

namespace App\Console\Commands;

use App\Competition;
use App\CompetitionsList;
use App\Song;
use Illuminate\Support\Collection;
use App\Repository\Modules\Competitions\ConsoleCompetition;
use Carbon\Carbon;
use DB;
use Exception;
use App\Exceptions\Console\CompetitionsListSavingException;

class CompetitionsFilling extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competitions:filling';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Filling planed upcoming competition with the songs.';

    /**
     * @var Carbon current date-time
     */
    private $today;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->today = Carbon::now();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->today->isSunday()) {
            $this->warnLog("Today is not a Sunday. Command [{$this->signature}] skipped.");
            return 0;
        }
        // Fill Top50
        $this->fillingByType('Top50');

        // Fill Weekly
        $this->fillingByType('Weekly');

        // Fill Bimonthly
        $this->fillingByType('Bimonthly');

        // Fill Yearly
        $this->fillingByType('Yearly');
        return 0;
    }

    private function fillTop50($competition)
    {
        // Log competition
        $this->logFilling($competition);

        $songs = Song::notPremium()->distinct()->get();

        if ($songs->isEmpty()) {
            // Nothing found stop processing competition
            $this->log('There is no available songs for processing. Finishing processing current competition.');
        } else {
            // Process songs
            $this->addSongsToCompetition($competition, $songs);
        }

        $this->log('End process competition(' . $competition->code . ').');
    }

    /**
     * @param $competition
     */
    private function fillWeekly($competition)
    {
        // Log competition
        $this->logFilling($competition);

        // Weekly sick6 takes data from top50 before the latest
        $competitionForFillingWeekly = Competition::with('songs')
            ->ofType('Top50')
            ->where('date_start', '<', $competition->date_start)
            ->orderBy('date_start', 'desc')
            ->limit(2)
            ->get()
            ->pop();
        // Get available songs
        $songs = $competitionForFillingWeekly->songs()
            ->orderBy('pivot_liked', 'desc')
            ->orderBy('pivot_played', 'desc')
            ->orderBy('pivot_added_to_playlist', 'desc')
            ->limit(Competition::TOP6_MAX_SONGS_COUNT)
            ->get();

        $this->log('Start process');

        // Process songs
        if ($songs->isEmpty()) {
            $this->log('Nothing to fill');
        } else {
            $this->addSongsToCompetition($competition, $songs);
        }
        $this->log('End process competition(' . $competition->code . ').');
    }

    /**
     * @param $competition
     */
    private function fillBimonthly($competition)
    {
        // Log competition
        $this->logFilling($competition);
        $closedWeeklyCompetitions = ConsoleCompetition::with(['songs' => function ($q) {
            $q->orderBy('pivot_voted', 'desc')
                ->orderBy('pivot_played', 'desc')
                ->orderBy('pivot_added_to_playlist', 'desc')
                ->orderBy('pivot_position', 'asc');
        }])
            ->lastSixFinished($competition->date_start)
            ->ofType('weekly')
            ->get();

        // Log Search results
        $logMessage = 'Fetched closed weekly competitions count = ' . $closedWeeklyCompetitions->count() . '.';
        $this->log($logMessage);

        if ($closedWeeklyCompetitions->isEmpty()) {
            $this->log('There is no closed Weekly competitions.');
        } else {
            $songs = $this->getWinnersSongs($closedWeeklyCompetitions, $competition);

            $this->addSongsToCompetition($competition, $songs);

            // For Bimonthly and Yearly competitions we need calculate position field
            // and update played and add_to_playlist
            $this->updateSongsOrderInCompetition($competition);
        }
        $this->log('End process competition(' . $competition->code . ').');
    }

    /**
     * @param $competition
     */
    private function fillYearly($competition)
    {
        // Log competition
        $this->logFilling($competition);

        // Get last 6 closed Bimonthly competitions
        $closedBimonthlyCompetitions = ConsoleCompetition::with(['songs' => function ($q) {
            $q->orderBy('pivot_voted', 'desc')
                ->orderBy('pivot_played', 'desc')
                ->orderBy('pivot_added_to_playlist', 'desc')
                ->orderBy('pivot_position', 'asc');
        }])
            ->lastSixFinished($competition->date_start)
            ->ofType('bimonthly')
            ->get();

        // Log Search results
        $logMessage = 'Fetched closed bimonthly competitions count = ' . $closedBimonthlyCompetitions->count() . '.';
        $this->log($logMessage);

        if ($closedBimonthlyCompetitions->isEmpty()) {
            $this->log('There are no closed Bimonthly competitions.');
        } else {
            $songs = $this->getWinnersSongs($closedBimonthlyCompetitions, $competition);

            $this->addSongsToCompetition($competition, $songs);

            // For Bimonthly and Yearly competitions we need calculate position field
            // and update played and add_to_playlist
            $this->updateSongsOrderInCompetition($competition);
        }
        $this->log('End process competition(' . $competition->code . ').');
    }

    /**
     * @param ConsoleCompetition $competition
     * @param Collection $songs
     * @throws CompetitionsListSavingException
     */
    private function addSongsToCompetition($competition, $songs)
    {
        // Songs are not empty process each song
        $this->log('Songs founded = ' . $songs->count() . '.');

        if ($competition->type === 'Top50' && $competition->songs()->exists()) {
            $this->warnLog(
                "Competition already has songs. Skip competition({$competition->code}) filling."
            );
            return;
        }

        // reduce songs number if competition already has some songs
        if ($competition->type !== 'Top50' && ($competitionSongsCount = $competition->songs()->count())) {
            $this->warnLog(
                "Competition already has $competitionSongsCount songs. Chosen songs will be reduced by this number."
            );
            // return if competition completely filled up with songs
            if ($competitionSongsCount === Competition::TOP6_MAX_SONGS_COUNT) {
                return;
            }
            $filteredSongs = $this->filterFoundedSongs($competition->songs, $songs);

            $songs = $this->getSongsCanBeAdded($songs, $filteredSongs, $competitionSongsCount);

            // reorder songs positions in competition
            $this->reorderSongsPositionsInCompetition($competition, $songs->count());
        }
        // Saving songs
        DB::beginTransaction();
        try {
            // Add songs to competition
            $songs->each($this->addSongToCompetition($competition));

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            throw new CompetitionsListSavingException($e);
        }
    }

    /**
     * @param ConsoleCompetition $competition
     * @return \Closure
     */
    private function addSongToCompetition(ConsoleCompetition $competition)
    {
        /**
         * @param Song $song
         * @param int  $key
         */
        return function ($song, $key) use ($competition) {
            // calc start position for Weekly only competition (it takes from Top50 songs order)
            $position = $competition->type === 'Weekly' ? intval($key + 1) : 0;
            // Bimonthly, Yearly competitions take Votes from previous competition (Weekly, Bimonthly respectively)
            $votes = str_contains($competition->type, ['Bimonthly', 'Yearly']) ? $song->pivot->voted : 0;
            $played = str_contains($competition->type, ['Bimonthly', 'Yearly']) ? $song->pivot->played : 0;
            $adds = str_contains($competition->type, ['Bimonthly', 'Yearly']) ? $song->pivot->added_to_playlist : 0;

            // Prepare array of pivot table data for Competition-Song relation
            $competitionSongPivotData = [
                'liked' => 0,
                'added_to_playlist' => $adds,
                'played' => $played,
                'voted' => $votes,
                'position' => $position
            ];

            $competition->songs()->save($song, $competitionSongPivotData);

            $songLog = sprintf(
                'Song(%s), Artist(%d), Title(%s), added to Competition(%s).',
                $song->code,
                $song->user_code,
                $song->title,
                $competition->code
            );

            $this->log($songLog);
        };
    }

    /**
     * @param $type
     * @return Collection|null
     */
    private function getCompetitions($type)
    {
        // For Weekly competitions we need to feel Future competition, not for current week
        $addWeeks = $type === 'Weekly' ? 1 : 0;
        $startDate = $this->startOfWeek()->addWeeks($addWeeks)->startOfDay();
        $endDate = $this->endOfWeek()->addWeeks($addWeeks)->endOfDay();
        return ConsoleCompetition::findWhichCanBeFilled($type, $startDate, $endDate)->get();
    }

    /**
     * @param string $type
     * @return void
     */
    private function fillingByType($type)
    {
        $type = studly_case($type);
        $this->log("Filling $type competitions start.");

        $collection = $this->getCompetitions($type);

        $this->log("Fetched competitions count = {$collection->count()}.");
        if ($collection->isEmpty()) {
            $this->log('Nothing to fill.');
        } else {
            $this->log("Process $type competitions start.");

            $fillType = 'fill' . $type;
            $this->$fillType($collection->first());

            $this->log("Process $type competitions finished successfully.");
        }
        $this->log("Filling $type competitions finished successfully.");
    }

    /**
     * Return StartOfWeek as Sunday no matter first day of the week is Sunday or Saturday
     *
     * @return Carbon $instance
     */
    private function startOfWeek()
    {
        $date = $this->today->copy();
        $startOfWeek = $date->startOfWeek();
        return $startOfWeek->isSunday() ? $startOfWeek : $startOfWeek->addWeek()->subDay();
    }

    /**
     * Return EndOfWeek as Saturday no matter last day of the week is Sunday or Saturday
     *
     * @return Carbon $instance
     */
    private function endOfWeek()
    {
        $date = $this->today->copy();
        $endOfWeek = $date->endOfWeek();
        return $endOfWeek->isSaturday() ? $endOfWeek : $endOfWeek->addWeek()->subDay();
    }

    /**
     * Checks whether User songs already participates in Competition
     *
     * @param Song $song
     * @param Collection $competitionSongs
     * @return bool
     */
    private function isArtistInCompetition(Song $song, $competitionSongs)
    {
        $userSongsCodes = $song->user->songs->pluck('code');
        return $competitionSongs->whereIn('code', $userSongsCodes)->isNotEmpty();
    }

    /**
     * @param Collection $closedCompetitions
     * @param ConsoleCompetition $competitionToFill
     * @return Collection
     */
    private function getWinnersSongs($closedCompetitions, ConsoleCompetition $competitionToFill)
    {
        $winnersSongs = collect([]);

        foreach ($closedCompetitions as $closedCompetition) {
            if (!$closedCompetition->songs()->exists()) {
                $this->warnLog("Competition({$closedCompetition->code}) has no songs.");
                continue;
            }
            foreach ($closedCompetition->songs as $closedCompetitionSong) {
                if ($this->isArtistInCompetition($closedCompetitionSong, $winnersSongs)) {
                    $logMessage = sprintf(
                        "Artist of song [%s] already present in competition [%s]. Try to get next song.",
                        $closedCompetitionSong->code,
                        $competitionToFill->code
                    );
                    $this->log($logMessage);
                    continue;
                }
                $winnersSongs->push($closedCompetitionSong);
                continue 2;
            }
        }

        return $winnersSongs;
    }

    /**
     * Update songs order in competition corresponding to their 'voted->played->added_to_playlist' rating
     * @param ConsoleCompetition $competition
     */
    private function updateSongsOrderInCompetition(ConsoleCompetition $competition)
    {
        $this->log("Start updating songs order in {$competition->type} competition({$competition->code}).");
        $competitionSongs = $competition->songs()
            ->orderBy('pivot_voted', 'desc')
            ->orderBy('pivot_played', 'desc')
            ->orderBy('pivot_added_to_playlist', 'desc')
            ->get();

        $competitionSongs->each(function ($song, $key) use ($competition) {
            $competition->songs()->updateExistingPivot($song->code, [
                'position' => intval($key + 1),
                'played' => 0,
                'added_to_playlist' => 0
            ]);
        });
        $this->log("End updating songs order.");
    }

    /**
     * @param $competition
     */
    private function logFilling($competition)
    {
        $competitionLog = sprintf(
            'Start process competition: Code(%s), Title(%s), Status(%d), DateStart(%s), DateEnd(%s).',
            $competition->code,
            $competition->title,
            $competition->status,
            $competition->date_start,
            $competition->date_end
        );
        $this->log($competitionLog);
    }

    /**
     * Change positions of the songs which were added to competition before (manually by admin) auto-command
     * @param $competition
     * @param $songsCount
     */
    private function reorderSongsPositionsInCompetition($competition, $songsCount)
    {
        $lastSongPosition = Competition::TOP6_MAX_SONGS_COUNT - $songsCount;
        CompetitionsList::where('competition_code', $competition->code)->increment('position', $songsCount);
    }

    /**
     * Filter founded songs if Command found the same songs that already were added by Admin earlier
     * @param Collection $competitionSongs
     * @param Collection $songs
     * @return Collection|static
     */
    public function filterFoundedSongs(Collection $competitionSongs, Collection $songs)
    {
        $filteredSongs = $songs->filter(function ($songsItem, $key) use ($competitionSongs) {
            return !$competitionSongs->contains(function ($competitionSongsItem) use ($songsItem) {
                return $competitionSongsItem->code === $songsItem->code;
            });
        });
        return $filteredSongs->values();
    }

    /**
     * Return songs that can be added to the Competition after filtering or not
     * @param Collection $songs
     * @param Collection $filteredSongs
     * @param integer    $competitionSongsCount
     * @return Collection
     */
    public function getSongsCanBeAdded(Collection $songs, Collection $filteredSongs, $competitionSongsCount)
    {
        if ($filteredSongs->count() == $songs->count()) {
            return $songs->slice(0, (Competition::TOP6_MAX_SONGS_COUNT - $competitionSongsCount));
        } elseif ($filteredSongs->count() != $songs->count() &&
            (Competition::TOP6_MAX_SONGS_COUNT - $competitionSongsCount) != $filteredSongs->count()) {
            return $filteredSongs->slice(0, (Competition::TOP6_MAX_SONGS_COUNT - $competitionSongsCount));
        } else {
            return $filteredSongs;
        }
    }
}
