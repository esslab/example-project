<?php

namespace App\Console\Commands;

use App\Competition;
use App\Exceptions\Console\CompetitionsSavingException;
use App\Repository\Modules\Competitions\ConsoleCompetition;
use Carbon\Carbon;
use DB;
use Exception;
use Faker\Factory as Faker;

class CompetitionsPlanning extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competitions:planning {--force : Allow running command without restrictions}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Planning and creating competitions events.';

    /**
     * @var Carbon
     */
    private $today;
    /**
     * @var boolean
     */
    private $force = false;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->today = Carbon::today();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws CompetitionsSavingException
     */
    public function handle()
    {
        $this->force = $this->option('force');

        if (!$this->force && !$this->today->isSaturday()) {
            $this->warnLog("Today is not a Saturday. Command [Competitions:planning] was skipped.");
            return 0;
        }
        if (!$this->force && $this->skipPlanning()) {
            $this->warnLog("Command [Competitions:planning] was skipped. We still have a long time plan.");
            return 0;
        }

        $competitionsPlannedDates = $this->planning($this->getStartDateForPlanning());

        $this->saving($competitionsPlannedDates);

        return 0;
    }

    /*
     * Planning all the competitions dates together
     *
     * top50: 1-8, 11-16, 19-24, 27-32, 35-40, 43-48
     * weekly: 3-8, 11-16, 19-24, 27-32, 35-40, 43-48
     * bimonthly: 9, 17, 25, 33, 41, 49
     * breaks: 10, 18, 26, 34, 42, 50, 52
     * yearly: 51
     *
     * */
    private function planning(Carbon $date, $tillWeek = Competition::TOTAL_PLANNING_WEEKS_NUMBER)
    {
        $this->log('Start planning competitions.');

        $top50Weeks = [1,2,3,4,5,6,7,8,11,12,13,14,15,16,19,20,21,22,23,24,27,28,29,30,31,32,
            35,36,37,38,39,40,43,44,45,46,47,48];
        $bimonthlyWeeks = [9, 17, 25, 33, 41, 49];
        $yearlyWeek = 51;
        $competitionsDates = [];
        $weeksToSkipForWeekly = Competition::ofType('yearly')->exists() ? [1] : [1,2];

        for ($week = 1; $week <= $tillWeek; $week++) {
            $date = $date->addWeek();

            $start = $this->startOfWeek($date);
            $end = $this->endOfWeek($date);

            if (in_array($week, $top50Weeks)) {
                $competitionsDates[$week]['top50'] = $this->getCompetitionsDatesByType(
                    $date,
                    $start,
                    $end,
                    $week,
                    'top50'
                );

                if (!in_array($week, $weeksToSkipForWeekly)) {
                    $competitionsDates[$week]['weekly'] = $this->getCompetitionsDatesByType(
                        $date,
                        $start,
                        $end,
                        $week,
                        'weekly'
                    );
                }
            }
            if (in_array($week, $bimonthlyWeeks)) {
                $competitionsDates[$week]['bimonthly'] = $this->getCompetitionsDatesByType(
                    $date,
                    $start,
                    $end,
                    $week,
                    'bimonthly'
                );
            }
            if ($week === $yearlyWeek) {
                $competitionsDates[$week]['yearly'] = $this->getCompetitionsDatesByType(
                    $date,
                    $start,
                    $end,
                    $week,
                    'yearly'
                );
            }
        }

        if (empty($competitionsDates)) {
            $this->warnLog("Competitions dates is empty.");
        }

        $this->log('End planning competitions.');

        return $competitionsDates;
    }

    /**
     * @param array  $competitionsPlannedDates
     * @throws CompetitionsSavingException
     * @internal param Collection $plan
     */
    private function saving(array $competitionsPlannedDates)
    {
        $this->log('Start creating competitions from planned dates.');

        foreach ($competitionsPlannedDates as $week) {
            foreach ($week as $plannedCompetition) {
                $dateStart = $plannedCompetition['start'];
                $dateEnd = $plannedCompetition['end'];
                $type = studly_case($plannedCompetition['type']);

                $competition = new ConsoleCompetition();

                $competition->code = Faker::create()->uuid;
                $competition->title = $this->createTitleFromDate($dateStart, $dateEnd);
                $competition->date_start = $dateStart;
                $competition->date_end = $dateEnd;
                $competition->type = $type;
                $competition->status = ConsoleCompetition::STATUS_PLANNED;

                try {
                    DB::beginTransaction();
                    $competition->save();
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    throw new CompetitionsSavingException($e);
                }

                $message = 'Created competition for week(%d).'
                    . 'With code(%s), title(%s), date_start(%s), date_end(%s), type(%s) and status(%d).';
                $this->log(
                    sprintf(
                        $message,
                        $plannedCompetition['planned-week'],
                        $competition->code,
                        $competition->title,
                        $competition->date_start,
                        $competition->date_end,
                        $competition->type,
                        $competition->status
                    )
                );
            }
        }
        $this->log('End creating competitions.');
    }

    /**
     * Creates a title for competition
     * @example January 1 - 8 or 29 May - 11 June
     * @param string $start Start date of the competition
     * @param string $end End date of the competition
     * @return string
     */
    private function createTitleFromDate($start, $end)
    {
        $start = Carbon::parse($start);
        $end = Carbon::parse($end);

        $startDay = $start->day;
        $endDay = $end->day;

        $startMonth = $start->month;
        $endMonth = $end->month;

        $startMonthName = $start->format('F');
        $endMonthName = $end->format('F');

        if ($startMonth !== $endMonth) {
            return sprintf('%s %d - %s %d', $startMonthName, $startDay, $endMonthName, $endDay);
        } else {
            return sprintf('%s %d - %d', $startMonthName, $startDay, $endDay);
        }
    }

    /**
     * Return result of Check whether to skip planning competitions
     * @return bool
     */
    private function skipPlanning()
    {
        $countYearlyOpen = ConsoleCompetition::ofType('Yearly')->withStatus(Competition::STATUS_PLANNED)->count();
        $countBimonthlyOpen = ConsoleCompetition::ofType('Bimonthly')->withStatus(Competition::STATUS_PLANNED)->count();
        return $countYearlyOpen >= 1 && $countBimonthlyOpen > 2;
    }

    /**
     * Returns Start of week as Sunday no matter it is Monday or Sunday
     * @param Carbon $date
     * @return Carbon $instance
     */
    private function startOfWeek(Carbon $date)
    {
        $startOfWeek = $date->copy()->startOfWeek();
        return $startOfWeek->isSunday() ? $startOfWeek : $startOfWeek->subDay();
    }

    /**
     * Returns End of week as Saturday no matter it is Saturday or Sunday
     * @param Carbon $date
     * @return Carbon $instance
     */
    private function endOfWeek(Carbon $date)
    {
        $endOfWeek = $date->copy()->endOfWeek();
        return $endOfWeek->isSaturday() ? $endOfWeek : $endOfWeek->subDay();
    }

    /**
     * @return Carbon
     */
    private function getStartDateForPlanning()
    {
        $lastYearlyCompetition = ConsoleCompetition::findLastsByType('Yearly')->first();
        $startDateForPlanning = is_null($lastYearlyCompetition) ?
            $this->today : Carbon::parse($lastYearlyCompetition->date_start)->addWeek();
        return $startDateForPlanning;
    }

    /**
     * @param Carbon  $date
     * @param Carbon  $dateStart
     * @param Carbon  $dateEnd
     * @param Integer $week
     * @param string  $type
     * @return mixed
     */
    private function getCompetitionsDatesByType(Carbon $date, Carbon $dateStart, Carbon $dateEnd, $week, $type)
    {
        $competitionsDates = [
            'type'         => $type,
            'start'        => $dateStart,
            'end'          => $dateEnd,
            'planned-week' => $week,
            'week-of-year' => $date->weekOfYear
        ];
        $this->log(
            sprintf(
                '%s competition planned dates: %s(%s) - %s(%s); PlannedWeek = %d. Week = %d(%d).',
                studly_case($type),
                $dateStart,
                $dateStart->format('l'),
                $dateEnd,
                $dateEnd->format('l'),
                $week,
                $date->weekOfYear,
                $date->year
            )
        );

        return $competitionsDates;
    }
}
