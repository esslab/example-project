<?php

namespace App\Console\Commands;

use App\ArtistStat;
use App\Repository\Modules\Profiles\ArtistProfileFollowers;

class CalculateArtistFollows extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'followers:count';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count artists followers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting update followed attribute...');
        $follows = \App\User::withCount('followedByUsers')->get();
        foreach ($follows as $follow) {
            $follow->stat()->update([
                'followed' => $follow->followed_by_users_count
            ]);
        }
        $this->info('Followed attribute was updated');
    }
}
