<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DB;

class UpdateApiBuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'services:update_api_build {build_number}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update API build version in DB';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Updating API build number ...');
        $apiBuildNumber = $this->argument('build_number');
        $now = Carbon::now();
        DB::table('api_builds')->insert([
            'build_number' => $apiBuildNumber,
            'created_at'   => $now,
            'updated_at'   => $now
        ]);
        return 0;
    }
}
