<?php

namespace App\Console\Commands;

use App\ArtistStat;
use App\Like;
use App\PlaylistsSong;
use App\Song;
use App\Vote;

class CountSongsStatistic extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'songs:count_stat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count songs statistic';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $songs = Song::with('stat')->get();

        $this->countLikes($songs);
        $this->info("Count songs likes - Done");

        $this->countVotes($songs);
        $this->info("Count songs votes - Done");

        $this->countPlaylists($songs);
        $this->info("Count added to playlists - Done");

        $this->info("Count songs statistic - Done");
    }

    private function countLikes($songs)
    {
        foreach ($songs as $song) {
            $likes = Like::where('song_code', $song->code)->count();
            $song->stat()->first()->update(['liked' => $likes]);
        }

        //count artist common likes
        $songs = Song::with('stat')->get();
        $artistStats = ArtistStat::all();
        foreach ($artistStats as $artistStat) {
            $likes = 0;
            foreach ($songs as $song) {
                if ($artistStat->user_code == $song->user_code) {
                    $likes += $song->stat()->first()->liked;
                }
            }
            $artistStat->liked = $likes;
            $artistStat->save();
        }
    }

    private function countVotes($songs)
    {
        foreach ($songs as $song) {
            $votes = Vote::where('song_code', $song->code)->count();
            $song->stat()->first()->update(['voted' => $votes]);
        }

        //count artist common votes
        $songs = Song::with('stat')->get();
        $artistStats = ArtistStat::all();
        foreach ($artistStats as $artistStat) {
            $votes = 0;
            foreach ($songs as $song) {
                if ($artistStat->user_code == $song->user_code) {
                    $votes += $song->stat()->first()->voted;
                }
            }
            $artistStat->voted = $votes;
            $artistStat->save();
        }
    }

    private function countPlaylists($songs)
    {
        foreach ($songs as $song) {
            $playlists = PlaylistsSong::where('song_code', $song->code)->count();
            $song->stat()->first()->update(['added_to_playlist' => $playlists]);
        }

        //count artist common added to playlists
        $songs = Song::with('stat')->get();
        $artistStats = ArtistStat::all();
        foreach ($artistStats as $artistStat) {
            $playlist = 0;
            foreach ($songs as $song) {
                if ($artistStat->user_code == $song->user_code) {
                    $playlist += $song->stat()->first()->added_to_playlist;
                }
            }
            $artistStat->added_to_playlist = $playlist;
            $artistStat->save();
        }
    }
}
