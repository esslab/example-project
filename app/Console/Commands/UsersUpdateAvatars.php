<?php

namespace App\Console\Commands;

use App;
use App\Auth0ManagementAPI;

class UsersUpdateAvatars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:update-avatars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update users avatars: fill empty and change relative to full paths(Production only).';

    /**
     * Files domain url
     *
     * @var string
     */
    private $appFilesDomainUrl = 'https://files.songline.fm/';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!App::environment('production')) {
            $this->error('This command expects to be run on Production environment.');
        }

        $auth0 = new Auth0ManagementAPI();
        $users = \App\User::all();
        $countEmpty = 0;
        $countFullPath = 0;
        foreach ($users as $user) {
            if (empty($user->avatar)) {
                $auth0User = $auth0->getUser($user->auth0_id);
                $user->avatar = $auth0User['picture'];
                $user->save();
                ++$countEmpty;
            } elseif (starts_with($user->avatar, 'avatars/')) {
                $user->avatar = $this->appFilesDomainUrl . $user->avatar;
                $user->save();
                $countFullPath++;
            }
        }
        $this->info("$countEmpty users with empty avatars and $countFullPath with relative avatars paths was updated.");
    }
}
