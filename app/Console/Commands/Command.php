<?php
namespace App\Console\Commands;

use Illuminate\Console\Command as BaseCommand;
use Carbon\Carbon;

class Command extends BaseCommand
{
    protected function log($message, $verbosity = null)
    {
        $time = Carbon::now()->toDateTimeString();
        $this->info('[' . $time . ']: ' . $message, $verbosity);
    }

    protected function warnLog($message, $verbosity = null)
    {
        $time = Carbon::now()->toDateTimeString();
        $this->warn('[' . $time . ']: ' . $message, $verbosity);
    }

    protected function errLog($message, $verbosity = null)
    {
        $time = Carbon::now()->toDateTimeString();
        $this->error('[' . $time . ']: ' . $message, $verbosity);
    }
}
