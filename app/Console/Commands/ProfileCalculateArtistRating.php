<?php

namespace App\Console\Commands;

use App\Repository\Modules\Profiles\ConsoleArtistProfile;
use Illuminate\Database\Eloquent\Collection;
use App\Repository\Modules\Profiles\ConsoleArtistSong;
use Exception;

/**
 * Class ProfileCalculateArtistRating
 * @package App\Console\Commands
 * This command is for scheduler execution, but can be run manually.
 * This command must be executed each day at 23:00
 */
class ProfileCalculateArtistRating extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profile:calculate-artist-rating';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates artists rating.';

    private $limit = 2;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start calculating artists ratings.');

        $artistsCount = $this->countAllArtists();
        $this->info(sprintf('Total artists found: %d.', $artistsCount));

        if ($artistsCount) {
            $this->calculateArtistsRating($artistsCount);
        } else {
            $this->info('There is no data to process.');
        }

        $this->info('Stop calculating artists ratings.');
    }

    private function calculateArtistsRating($totalCount)
    {
        for ($offset = 0; $offset < $totalCount; $offset += $this->limit) {
            $start = $offset;
            $end = ($totalCount - $offset) >= $this->limit ? ($offset + $this->limit) : $totalCount;
            $this->info(sprintf('Start processing pack from %d to %d.', $start, $end));

            $artists = $this->getArtists($offset, $this->limit);
            $this->info(sprintf('Fetched artists count = %d.', $artists->count()));

            if (!$artists->isEmpty()) {
                $artists->each($this->calculateArtistRating());
            }

            $this->info('End processing pack.');
            sleep(3);
        }
    }

    /**
     * @return \Closure
     */
    private function calculateArtistRating()
    {
        /**
         * @param ConsoleArtistProfile $artist
         */
        return function ($artist) {
            $this->info(sprintf('Start processing artist (%s).', $artist->code));

            $songs = $this->getArtistsSongs($artist->code);
            $this->info(sprintf('Fetched songs count = %d.', $songs->count()));

            if ($songs->isEmpty()) {
                $this->info('Nothing to process.');
            } else {
                $rating = 0;

                $songs->each($this->calculateSongRating($rating));

                $this->info(sprintf('Calculated rating = %d.', $rating));

                $this->updateRating($artist, $rating);
            }

            $this->info(sprintf('End processing artist (%s).', $artist->code));
        };
    }

    /**
     * @param integer $rating
     * @return \Closure
     */
    private function calculateSongRating(&$rating)
    {
        /**
         * @param ConsoleArtistSong $song
         */
        return function ($song) use (&$rating) {
            $this->info(sprintf('Start processing song (%s).', $song->code));

            $songRating = $song->liked + $song->voted + $song->added_to_playlist;
            $this->info(sprintf('Song rating = %d.', $songRating));

            $rating += $songRating;

            $this->info(sprintf('End processing song (%s).', $song->code));
        };
    }

    /**
     * @param ConsoleArtistProfile $artist
     * @param integer $rating
     */
    private function updateRating($artist, $rating)
    {
        try {
            $artist->stat->rating = $rating;
            $artist->stat->save();

            $this->info(sprintf('Rating for user(%s) successfully updated to (%d).', $artist->code, $rating));
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
    }

    private function getArtistsSongs($artist)
    {
        return ConsoleArtistSong::allSongsForArtist($artist)
            ->get();
    }

    /**
     * Returns a pack of available artists
     * @param integer $offset
     * @param integer $limit
     * @return Collection
     */
    private function getArtists($offset, $limit)
    {
        return ConsoleArtistProfile::paginatedArtistsProfiles($offset, $limit)
            ->get();
    }

    /**
     * Counts all artists
     * @return int
     */
    private function countAllArtists()
    {
        return ConsoleArtistProfile::allArtistsProfiles()
            ->count();
    }
}
