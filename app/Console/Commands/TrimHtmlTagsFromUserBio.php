<?php

namespace App\Console\Commands;

use App\User;

class TrimHtmlTagsFromUserBio extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:trim-html-tags';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trims Html tags from User bio field.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $usersToUpdate = User::where('bio', 'RLIKE', '<[^>]*>')->get();
        if ($usersToUpdate->isEmpty()) {
            $this->log("No users for update.");
            return 0;
        }
        $pattern = '/<[^>]*>/';
        $replacement = '';
        $numberOfUpdatedUsers = 0;
        $numberOfReplacements = 0;

        foreach ($usersToUpdate as $user) {
            $user->bio = preg_replace($pattern, $replacement, $user->bio, -1, $countReplacements);
            $user->save();
            $numberOfReplacements += $countReplacements;
            $numberOfUpdatedUsers++;
        }

        $this->log(
            sprintf(
                "Number of updated users: %d, Total number of updates: %d",
                $numberOfUpdatedUsers,
                $numberOfReplacements
            )
        );
        return 0;
    }
}
