<?php

namespace App\Console\Commands;

//use Illuminate\Console\Command;

class CreatePlansForAllUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plans:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create plans for all users who don\'t have one';

    /**
     * @var \App\Repository\StripeRepository
     */
    protected $stripe;

    /**
     * CreatePlansForAllUsers constructor.
     */
    public function __construct(\App\Repository\StripeRepository $stripeRepository)
    {
        parent::__construct();

        $this->stripe = $stripeRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = \App\User::all();
        foreach ($users as $user) {
            try {
                $this->stripe->retrievePlan($user->code);
            } catch (\Stripe\Error\Base $e) {
                $this->stripe->createPlan($user, 0);
            }
        }
        $this->info("Users was migrated");
    }
}
