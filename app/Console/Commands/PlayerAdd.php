<?php

namespace App\Console\Commands;

use App\User;
use App\Player;

class PlayerAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add-player';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adding player to users without it.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = new User();
        $users = $user->all();
        foreach ($users as $user) {
            $userPlayer = $user->player()->get();
            if (count($userPlayer) == 0) {
                $this->log("Creating player for user {$user->code}..");
                $player = new Player();
                $player->user_code = $user->code;
                $player->timePaused = 0;
                $player->isShuffle = 0;
                $player->repeatState = 0;
                $player->trackPaused = 0;
                $player->tracksOrder = '';
                $player->save();
            }
        }
        return 0;
    }
}
