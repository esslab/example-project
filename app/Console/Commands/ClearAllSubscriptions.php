<?php

namespace App\Console\Commands;

use App\ArtistsFollows;
use App\Subscription;

class ClearAllSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:clear {--all: also remove free subscriptions and do unfollow for them}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear payed subscriptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dump("Starting...");
        \App\Repository\StripeRepository::cancelAllSubscriptions();

        $subsciptions = Subscription::all();
        foreach ($subsciptions as $subsciption) {
            if ($subsciption->price == 0) {
                if ($this->option('all')) {
                    $this->deleteFollow($subsciption);
                    $subsciption->delete();
                }
            } else {
                $subsciption->delete();
            }
        }
        dump("All subscriptions was cancelled");
    }

    /**
     * @param $subsciption
     */
    private function deleteFollow($subsciption)
    {
        ArtistsFollows::where([
            'user_code' => $subsciption->user_id,
            'artist_code' => $subsciption->stripe_plan
        ])->delete();
    }
}
