<?php

namespace App\Console\Commands;

use App\Charge;
use App\Repository\StripeRepository;
use App\User;
use Carbon\Carbon;

class FillChargesTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'charges:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear table and fill it from stripe invoice.payment_succeeded events';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    const UNSIGNED_MEDIUM_INT = 16777215;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $stripeRepository = new StripeRepository();
        $event['id'] = null;
        Charge::truncate();
        do {
            $events = $stripeRepository->getEvents('invoice.payment_succeeded', 100, $event['id']);
            foreach ($events['data'] as $event) {
                $user = User::where('stripe_id', $event['data']['object']['customer'])->first();
                if ($user && $event['data']['object']['amount_due'] != 0) {
                    $charge = new Charge();
                    $charge->user_code = $user->code;
                    $charge->plan = $event['data']['object']['lines']['data'][0]['plan']['id'];
                    $charge->amount = $event['data']['object']['amount_due'];
                    $charge->currency = $event['data']['object']['currency'];
                    if ($charge->amount >= self::UNSIGNED_MEDIUM_INT) {
                        $this->info($event['data']['object']['charge'] . ' charge was skipped - too large amount');
                        continue;
                    }
                    $charge->created = Carbon::createFromTimestamp($event['data']['object']['date'])
                        ->toDateTimeString();
                    $charge->save();
                } else {
                    continue;
                }
            }
        } while ($events['data']);
        $this->info("Charges table was updated");
    }
}
