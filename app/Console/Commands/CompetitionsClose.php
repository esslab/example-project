<?php

namespace App\Console\Commands;

use App\ArtistStat;
use App\Exceptions\Console\CompetitionsSavingException;
use App\Repository\Modules\Competitions\ConsoleCompetition;
use App\Services\CompetitionService;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Logger;

class CompetitionsClose extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competitions:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Close current competition and determine the winner.';

    /**
     * @var Carbon instance
     */
    private $today;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->today = Carbon::today();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Check that this command is executed at Saturday
        if (!$this->today->isSaturday()) {
            $this->warnLog("Today is not a Saturday. Command [{$this->signature}] was skipped.");
            return 0;
        }
        // Log start procedure
        $this->log("Start {$this->signature} command.");

        // Get competitions available for closing
        $competitions = $this->getAvailableCompetitions();

        // Process Competitions
        $this->processCompetitions($competitions, $this->close());

        $this->log("Finish {$this->signature} command.");
        return 0;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection $competitions
     * @param \Closure
     * @throws CompetitionsSavingException
     */
    private function processCompetitions($competitions, $callback)
    {
        if ($competitions->isEmpty()) {
            $this->warnLog('There is no available competitions for processing.');
        } else {
            // Log competitions count
            $this->log('Fetched competitions count = ' . $competitions->count() . '.');

            DB::beginTransaction();
            try {
                $competitions->each($callback);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new CompetitionsSavingException($e);
            }
        }
    }

    /**
     * @return \Closure
     */
    private function close()
    {
        /**
         * @param ConsoleCompetition $competition
         * @param integer $index
         */
        return function ($competition, $index) {

            // Log current competition
            $competitionLog = sprintf(
                'Start process competition: Code(%s), Title(%s), Status(%d), DateStart(%s), DateEnd(%s), Type(%s)',
                $competition->code,
                $competition->title,
                $competition->status,
                $competition->date_start,
                $competition->date_end,
                $competition->type
            );
            $this->log($competitionLog);

            $winner = $this->getWinnerSong($competition);
            if ($winner) {
                $this->log('Winner is defined: Song(' . $winner->song_code . '), Artist(' . $winner->user_code . ').');

                $competition->winner_song_code = $winner->song_code;
                $competition->winner_artist_code = $winner->user_code;
                $artistStat = ArtistStat::find($winner->user_code);
                if ($artistStat) {
                    $artistStat->winner++;
                    $artistStat->save();
                } else {
                    // Report data inconsistency
                    $message = "Artist({$winner->user_code}) has no `artist_stat` record.";
                    Logger::logMessage($message);
                }
            }
            $competition->status = ConsoleCompetition::STATUS_FINISHED;
            $competition->save();

            $this->log('End process competition(' . $competition->code . ').');
        };
    }

    /**
     * @param ConsoleCompetition $competition
     * @return Model|null
     */
    private function getWinnerSong($competition)
    {
        // Logging start operation
        $this->log('Start processing song:');
        $competitionService = new CompetitionService;
        $song = $competitionService->getCompetitionWinner($competition);

        if ($song === null) {
            $this->warnLog('No winner song for competition ' . $competition->code);
            return null;
        } else {
            $songLog = sprintf(
                'Code (%s), Artist(%d), Title(%s), CreatedAt(%s), UpdatedAt(%s).',
                $song->code,
                $song->user_code,
                $song->title,
                $song->created_at,
                $song->updated_at
            );
            $this->log($songLog);
        }

        // Logging Song results
        $message = sprintf(
            'Votes(%d), Likes(%d), Plays(%d), AddsToPlaylist(%d), StartPosition(%d)',
            $song->voted,
            $song->liked,
            $song->played,
            $song->added_to_playlist,
            $song->position
        );
        $this->log($message);
        $this->log('Successfully finished process Song(' . $song->song_code . ').');

        return $song;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getAvailableCompetitions()
    {
        //Competition ends at Saturday
        $dateEnd = $this->endOfWeek()->endOfDay();
        return ConsoleCompetition::findWhichCanBeClosed($dateEnd)->get();
    }

    /**
     * Return EndOfWeek as Saturday no matter last day of the week is Sunday or Saturday
     *
     * @return Carbon $instance
     * @internal param Carbon $today
     */
    private function endOfWeek()
    {
        $date = $this->today->copy();
        $endOfWeek = $date->endOfWeek();
        return $endOfWeek->isSaturday() ? $endOfWeek : $endOfWeek->subDay();
    }
}
