<?php

namespace App\Console\Commands;

use Storage;

class S3CreateDirectories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 's3:create-directories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create directories structure on S3 bucket';

    /**
     * @var array
     */
    protected $folders = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->folders = [
            '/images/source',
            '/avatars/source',
            '/covers/source',
            '/video/source',
            '/audio/source'
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $s3 = Storage::disk('s3');
        foreach ($this->folders as $folder) {
            if (!$s3->exists($folder)) {
                $s3->makeDirectory($folder);
            }
        }
        $this->info('Directories structure created.');
    }
}
