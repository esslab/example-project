<?php

namespace App\Console\Commands;

use App\Competition;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Repository\Modules\Competitions\ConsoleCompetition;
use DB;
use App\Exceptions\Console\CompetitionsSavingException;
use Exception;

class CompetitionsOpen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'competitions:open';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Open competition which will start from the next week.';

    /**
     * @var Carbon $instance
     */
    private $today;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->today = Carbon::today();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Check that this command is executed at Sunday
        if (!$this->today->isSunday()) {
            $this->warnLog('Today is not a Sunday.');
            return 0;
        }

        // Process Top50 competitions
        $this->processTop50();

        // Process Weekly competitions
        $this->processWeekly();

        // Process Bimonthly competitions
        $this->processBimonthly();

        // Process Yearly competitions
        $this->processYearly();

        return 0;
    }

    protected function processTop50()
    {
        $this->processCompetitionsByType('Top50');
    }

    protected function processWeekly()
    {
        $this->processCompetitionsByType('Weekly');
    }

    protected function processBimonthly()
    {
        $this->processCompetitionsByType('Bimonthly');
    }

    protected function processYearly()
    {
        $this->processCompetitionsByType('Yearly');
    }

    /**
     * @param string $type
     * @throws CompetitionsSavingException
     */
    protected function processCompetitionsByType($type)
    {
        $openMessage = sprintf('Start competitions:open operation for %s.', $type);
        $this->log($openMessage);

        $competitions = $this->getAvailableCompetitions($type);

        $this->processCompetitions($competitions);

        $finishMessage = sprintf('Finish competitions:open operation for %s.', $type);
        $this->log($finishMessage);
    }

    /**
     * @param Collection|null $competitions
     * @throws CompetitionsSavingException
     */
    private function processCompetitions($competitions)
    {
        $this->log('Start processing competitions.');

        if ($competitions->isEmpty()) {
            $this->warnLog('Nothing to process, competitions are empty.');
        } else {
            $this->log('Was found ' . $competitions->count() . ' competitions');

            DB::beginTransaction();
            try {
                $competitions->each($this->startCompetition());

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                throw new CompetitionsSavingException($e);
            }
        }

        $this->log('Finish processing competitions.');
    }

    private function startCompetition()
    {
        /**
         * @param ConsoleCompetition $competition
         */
        return function ($competition) {
            // Logging start procedure
            $logMessage = 'Start processing '
                . $competition->type . ' competition(' . $competition->code . ').';
            $this->log($logMessage);

            $competition->status = ConsoleCompetition::STATUS_STARTED;
            $competition->save();

            // Logging finish procedure
            $logMessage = 'Successfully finish processing '
                . $competition->type . ' competition(' . $competition->code . ').';
            $this->log($logMessage);
        };
    }

    /**
     * @param string $type
     * @return Collection
     * @internal param Carbon $today
     */
    private function getAvailableCompetitions($type)
    {
        // Get date
        $dateStart = $this->today->startOfDay();

        return ConsoleCompetition::findWhichCanBeOpened($dateStart, $type)->get();
    }
}
