<?php

namespace App\Console\Commands;

use DB;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHTTP\Exception\GuzzleException;
use App\Auth0ManagementAPI;
use Faker\Factory as Faker;
use Illuminate\Http\File;
use Storage;

class ProfileGrabAvatarImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'profile:grab-avatar-images {domain} {--limit=5}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab profile avatars and save them to s3 bucket';

    /**
     * This will contain Auth0Management instance
     *
     * @var Auth0ManagementAPI
     */
    protected $auth0Management;

    /**
     * This will contain GuzzleHttp client instance
     * @var Client;
     */
    protected $client;

    /**
     * This will contain image domain
     *
     * @example https://staging-files.songline.fm/
     * @var string
     */
    protected $domain;

    /**
     * @var integer
     */
    protected $limit;

    /*
     * Current iterating User
     * */
    private $user;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->domain = $this->argument('domain');
        $this->limit = $this->option('limit');

        $this->domain = rtrim($this->domain, '/');

        $users = $this->getUsersList();
        $usersCount = $users->count();

        if ($usersCount === 0) {
            $this->warnLog('There is no users for grabbing images.');
            return 0;
        }
        $this->log("Will be processed $usersCount users.");

        $users->each($this->processUserClosure());

        return 0;
    }

    protected function processUserClosure()
    {
        return function ($user) {
            $this->user = $user;
            $avatar = $this->download();

            if ($avatar) {
                $avatar['s3File'] = $this->upload($avatar);

                unlink($avatar['tmpFile']);

                $fileUrl = $this->domain . '/' . $avatar['s3File'];
                DB::table('users')
                    ->where('code', $this->user->code)
                    ->update(['avatar' => $fileUrl]);

                $this->log(sprintf('Users (%d) avatar updated to %s.', $this->user->code, $fileUrl));
            }
        };
    }

    protected function download()
    {
        $sourcePath = $this->user->avatar;
        $targetPath = $this->getPath($this->user->code);

        if ($this->isFacebookUrl($sourcePath)) {
            $sourcePath = $this->getFacebookSourcePath();
        }
        $downloadedFile = $this->downloadAvatar($sourcePath, $targetPath);

        if (!$downloadedFile && !$this->isFacebookUrl($sourcePath)) {
            $downloadedFile = $this->downloadAuth0Avatar($targetPath);
        }

        return $downloadedFile;
    }

    protected function downloadAvatar($source, $target)
    {
        $this->log(sprintf('Start downloading from %s.', $source));
        $client = new Client;
        $extension = $this->getExtension($source);
        $target = sprintf('%s.%s', $target, $extension);

        try {
            $client->request('GET', $source, ['sink' => $target]);

            $this->log(sprintf('File successfully downloaded and saved into %s.', $target));
            return [
                "tmpFile"    => $target,
                "extension"  => $extension,
                "s3Folder"   => config('aws.s3.directories.avatars'),
                "s3FileName" => sprintf('%s.%s', Faker::create()->uuid, $extension)
            ];
        } catch (GuzzleException $e) {
            $message = sprintf(
                'Unable to download file[user_code=%d, auth0_id=%s]. Response with status %d and message %s.',
                $this->user->code,
                $this->user->auth0_id,
                $e->getCode(),
                $e->getMessage()
            );
            $this->errLog($message);
            return null;
        }
    }

    protected function downloadAuth0Avatar($target)
    {
        $this->log(sprintf('Try to download file from auth0 user %s.', $this->user->auth0_id));
        $auth0Management = new Auth0ManagementAPI;

        try {
            $user = $auth0Management->getUser($this->user->auth0_id);
            $source = $user['picture_large'];

            if (!is_string($source) || empty($source)) {
                throw new Exception('Unable to get image source from profile.');
            }
        } catch (ClientException $e) {
            $message = sprintf(
                'Unable to get auth0 profile %s avatar. Reason: %s with status %d',
                $this->user->auth0_id,
                $e->getMessage(),
                $e->getCode()
            );
            $this->errLog($message);
            return null;
        } catch (Exception $e) {
            $this->errLog($e->getMessage());
            return null;
        }

        return $this->downloadAvatar($source, $target);
    }

    protected function upload($configs)
    {
        return Storage::disk('s3')
            ->putFileAs($configs['s3Folder'], new File($configs['tmpFile']), $configs['s3FileName'], 'public');
    }

    protected function getUsersList()
    {
        $users = DB::table('users')
            ->select('auth0_id', 'code', 'avatar')
            ->where('avatar', 'not like', $this->domain . '%')
            ->limit($this->limit)
            ->get();

        return $users;
    }

    protected function getExtension($source)
    {
        $info = pathinfo($source);
        if (!is_array($info) || !isset($info['extension'])) {
            return 'jpg';
        }
        $extension = $info['extension'];
        if (str_contains($extension, '?')) {
            $extension = substr($extension, 0, strpos($extension, '?'));
        }
        return $extension;
    }

    protected function getPath($code)
    {
        $dirPath = storage_path('tmp');
        if (!is_dir($dirPath)) {
            mkdir($dirPath);
        }
        return storage_path('tmp'. DIRECTORY_SEPARATOR . $code);
    }

    private function isFacebookUrl($sourcePath)
    {
        return str_contains($sourcePath, ['fbcdn', 'facebook']);
    }

    private function getFacebookSourcePath()
    {
        $fbId = substr($this->user->auth0_id, strpos($this->user->auth0_id, '|') + 1);
        return "https://graph.facebook.com/v3.0/$fbId/picture?width=512";
    }
}
