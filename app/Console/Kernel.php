<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Classes\Traits\MonitorsSchedule;

class Kernel extends ConsoleKernel
{
    use MonitorsSchedule;

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CompetitionsPlanning::class,
        Commands\CompetitionsFilling::class,
        Commands\CompetitionsOpen::class,
        Commands\CompetitionsClose::class,
        Commands\ProfileCalculateArtistRating::class,
        Commands\CreatePlansForAllUsers::class,
        Commands\CreateEmptyUsersStats::class,
        Commands\PlayerAdd::class,
        Commands\CountSongsStatistic::class,
        Commands\FillChargesTable::class,
        Commands\CalculateArtistFollows::class,
        Commands\ClearAllSubscriptions::class,
        Commands\S3CreateDirectories::class,
        Commands\UsersUpdateAvatars::class,
        Commands\TrimHtmlTagsFromUserBio::class,
        Commands\UpdateApiBuild::class,
        Commands\ProfileGrabAvatarImages::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('profile:calculate-artist-rating')
            ->dailyAt('23:00')
            ->withoutOverlapping();

        // Planing Top50 competitions
        $schedule->command('competitions:planning')
            ->weekly()
            ->saturdays()
            ->at('23:50')
            ->appendOutputTo(config('scheduler.report.file'))
            ->withoutOverlapping();

        // Planing Weekly competitions
        $schedule->command('competitions:close')
            ->weekly()
            ->saturdays()
            ->at('23:55')
            ->appendOutputTo(config('scheduler.report.file'))
            ->withoutOverlapping();

        // Planing Weekly competitions
        $schedule->command('competitions:filling')
            ->weekly()
            ->sundays()
            ->at('00:00')
            ->appendOutputTo(config('scheduler.report.file'))
            ->withoutOverlapping();

        // Planing Weekly competitions
        $schedule->command('competitions:open')
            ->weekly()
            ->sundays()
            ->at('00:10')
            ->appendOutputTo(config('scheduler.report.file'))
            ->withoutOverlapping();

        $this->monitor($schedule);
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
