<?php
namespace App;

use App;
use Auth0\SDK\Exception\CoreException;
use App\Exceptions\AuthenticationTokenException;
use Illuminate\Support\Str;

class JWTUser
{
    public $name;
    public $nickname;
    public $picture;
    public $email_verified;
    public $iss;
    public $sub;
    public $aud;
    public $exp;
    public $iat;
    public $email;
    public $provider;

    public function __construct($token)
    {
        $decoded = $this->decodeJWT($token);
        if (!$this->validateDecodedUser($decoded)) {
            throw new AuthenticationTokenException('The JWT User object structure is invalid.');
        } else {
            $this->parseDecodedUser($decoded);
        }
    }

    /**
     * @param object $decodedUser
     */
    protected function parseDecodedUser($decodedUser)
    {
        $userData = get_object_vars($decodedUser);

        $this->iss = $decodedUser->iss;
        $this->sub = $decodedUser->sub;
        $this->aud = $decodedUser->aud;
        $this->exp = $decodedUser->exp;
        $this->iat = $decodedUser->iat;

        $this->name = $decodedUser->name;
        $this->nickname = $decodedUser->nickname;
        $this->picture = $decodedUser->picture;
        $this->email_verified = $decodedUser->email_verified;

        $this->provider = Str::substr($this->sub, 0, strpos($this->sub, '|'));
        $this->nickname = $userData['https://songline.fm/nickname'] ?? $this->name;
        
        $socialEmail = "https://songline.fm/social_email";

        if (!empty($decodedUser->email)) {
            $this->email = $decodedUser->email;
        } elseif (!empty($decodedUser->$socialEmail)) {
            $this->email = $decodedUser->$socialEmail;
        }
    }

    /**
     * @param object $decodedUser
     * @return bool
     */
    protected function validateDecodedUser($decodedUser)
    {
        return array_has(get_object_vars($decodedUser), ['sub', 'nickname', 'picture', 'email_verified', 'name'])
            && Str::contains($decodedUser->sub, '|')
            && $this->validateDecodedUserEmail($decodedUser);
    }

    /**
     * @param object $decodedUser
     * @return bool
     */
    protected function validateDecodedUserEmail($decodedUser)
    {
        if (!empty($decodedUser->email)) {
            return true;
        }

        $socialEmail = "https://songline.fm/social_email";
        if (!empty($decodedUser->$socialEmail)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $token
     * @return object
     * @throws CoreException
     */
    protected function decodeJWT($token)
    {
        $auth0 = App::make('auth0');
        return $auth0->decodeJWT($token);
    }
}
