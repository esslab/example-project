<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongStat extends Model
{
    protected $table = 'songs_stats';

    protected $primaryKey = 'song_code';

    protected $fillable = ["played", "liked", "shared", "added_to_playlist", "voted"];

    public $incrementing = false;

    protected $hidden = ['created_at', 'updated_at', 'song_code'];

    //@todo With new project structure this should be transfered to different place (maybe Service or Domain model)
    /**
     * Increment added_to_playlist property for specified songs
     * @param array $songIds
     * @return $this
     */
    public function incrementAddedToPlaylistPropertyForSongs(array $songIds)
    {
        $this->whereIn('song_code', $songIds)->each(function ($songStat) {
            $songStat->increment('added_to_playlist');
        });
        return $this;
    }
}
