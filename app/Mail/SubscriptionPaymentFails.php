<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionPaymentFails extends Mailable
{
    use Queueable, SerializesModels;

    public $artist;

    /**
     * SubscriptionWasCanceled constructor.
     * @param $artist
     */
    public function __construct($artist)
    {
        $this->artist = $artist;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.subscription_payment_fails');
    }
}
