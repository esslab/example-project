<?php
namespace App\Listeners;

use App\ArtistStat;
use App\Events\ArtistFollow;
use App\Events\ArtistUnfollow;
use App\Events\ArtistWinCompetition;
use App\Events\SongAddedToPlaylist;
use App\Events\SongWasLiked;
use App\Events\ArtistSubscribe;
use App\Events\ArtistUnSubscribe;
use App\Events\SongWasUnliked;
use App\Events\SongWasUnvoted;
use App\Events\SongWasVoted;
use App\Exceptions\CompetitionUpdateException;
use App\User;
use Illuminate\Events\Dispatcher;
use Logger;

class ArtistEventSubscriber
{
    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\ArtistSubscribe',
            'App\Listeners\ArtistEventSubscriber@onArtistSubscribe'
        );

        $events->listen(
            'App\Events\ArtistUnSubscribe',
            'App\Listeners\ArtistEventSubscriber@onArtistUnSubscribe'
        );

        $events->listen(
            'App\Events\ArtistFollow',
            'App\Listeners\ArtistEventSubscriber@onArtistFollow'
        );

        $events->listen(
            'App\Events\ArtistUnfollow',
            'App\Listeners\ArtistEventSubscriber@onArtistUnfollow'
        );

        $events->listen(
            'App\Events\SongAddedToPlaylist',
            'App\Listeners\ArtistEventSubscriber@onArtistSongAddedToPlaylist'
        );

        $events->listen(
            'App\Events\ArtistWinCompetition',
            'App\Listeners\ArtistEventSubscriber@onArtistWinCompetition'
        );

        $events->listen(
            'App\Events\SongWasLiked',
            'App\Listeners\ArtistEventSubscriber@onSongWasLiked'
        );

        $events->listen(
            'App\Events\SongWasUnliked',
            'App\Listeners\ArtistEventSubscriber@onSongWasUnliked'
        );

        $events->listen(
            'App\Events\SongWasVoted',
            'App\Listeners\ArtistEventSubscriber@onSongWasVoted'
        );

        $events->listen(
            'App\Events\SongWasUnvoted',
            'App\Listeners\ArtistEventSubscriber@onSongWasUnvoted'
        );
    }

    /**
     * @param ArtistSubscribe $event
     */
    public function onArtistSubscribe($event)
    {
        $artist = $event->getArtist();
        $artist->stat->increment('subscribed');
    }

    /**
     * @param ArtistUnSubscribe $event
     */
    public function onArtistUnSubscribe($event)
    {
        $artist = $event->getArtist();
        if ($artist->stat->subscribed > 0) {
            $artist->stat->decrement('subscribed');
        }
    }

    /**
     * @param ArtistFollow $event
     */
    public function onArtistFollow($event)
    {
        $artist = $event->getArtist();
        $artist->stat->increment('followed');
    }

    /**
     * @param ArtistUnfollow $event
     */
    public function onArtistUnfollow($event)
    {
        $artist = $event->getArtist();
        if ($artist->stat->followed > 0) {
            $artist->stat->decrement('followed');
        }
    }

    /**
     * @param SongAddedToPlaylist $event
     */
    public function onArtistSongAddedToPlaylist($event)
    {
        $song = $event->song;
        $song->user->stat->increment('added_to_playlist');
    }

    /**
     * @param ArtistWinCompetition $event
     */
    public function onArtistWinCompetition($event)
    {
        $artist = $event->artist;

        if (!$artist->stat) {
            $msg = "Artist({$artist->code}) has no `artist_stat` record.";
            throw new CompetitionUpdateException($msg);
        }
        $artist->stat->increment('winner');
    }

    /**
     * @param SongWasLiked $event
     */
    public function onSongWasLiked(SongWasLiked $event)
    {
        $song = $event->song;
        $artist = User::find($song->user_code);
        $artist->stat()->increment('liked');
    }

    /**
     * @param SongWasUnliked $event
     */
    public function onSongWasUnliked(SongWasUnliked $event)
    {
        $song = $event->song;
        $artistStat = ArtistStat::find($song->user_code);

        if (empty($artistStat)) {
            $message = "Artist [{$song->user_code}] has no Stats.";
            Logger::logMessage($message, ['sentry', 'file'], [], 'warning');
            return;
        }

        if (empty($artistStat->liked)) {
            $message = 'Decreasing artist_stat liked field when it is empty or 0.';
            Logger::logMessage($message);
            return;
        }

        --$artistStat->liked;
        $artistStat->save();
    }

    /**
     * @param SongWasVoted $event
     */
    public function onSongWasVoted(SongWasVoted $event)
    {
        $song = $event->song;
        $artist = User::find($song->user_code);
        $artist->stat()->increment('voted');
        $artist->stat()->increment('liked');
    }

    /**
     * @param SongWasUnvoted $event
     */
    public function onSongWasUnvoted(SongWasUnvoted $event)
    {
        $song = $event->song;
        $artistStat = ArtistStat::find($song->user_code);

        if (empty($artistStat)) {
            $message = "Artist [{$song->user_code}] has no Stats.";
            Logger::logMessage($message, ['sentry', 'file'], [], 'warning');
            return;
        }

        if (empty($artistStat->voted)) {
            $message = 'Decreasing artist_stat voted field when it is empty or 0.';
            Logger::logMessage($message);
            return;
        }
        --$artistStat->voted;

        if (empty($artistStat->liked)) {
            $message = 'Decrementing artist_stat liked field when unvoted when it is empty or 0.';
            Logger::logMessage($message);
            return;
        }
        --$artistStat->liked;

        $artistStat->save();
    }
}
