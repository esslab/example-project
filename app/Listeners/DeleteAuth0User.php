<?php

namespace App\Listeners;

use App\Events\UserWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteAuth0User
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  UserWasDeleted  $event
     * @return void
     */
    public function handle(UserWasDeleted $event)
    {
        // Delete user
        $event->repository->deleteUser($event->userCode, $event->auth0_id);
    }
}
