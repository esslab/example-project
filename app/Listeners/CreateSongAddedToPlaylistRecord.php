<?php

namespace App\Listeners;

use App\Events\SongAddedToPlaylist;

class CreateSongAddedToPlaylistRecord
{
    /**
     * Handle the event.
     *
     * @param  SongAddedToPlaylist  $event
     * @return void
     */
    public function handle(SongAddedToPlaylist $event)
    {
        $song = $event->song;
        $song->addedToPlaylists()->create([]);
    }
}
