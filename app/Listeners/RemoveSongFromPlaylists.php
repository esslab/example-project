<?php

namespace App\Listeners;

use App\Events\SongDeleted;

class RemoveSongFromPlaylists
{
    /**
     * Handle the event.
     *
     * @param  SongDeleted  $event
     * @return void
     */
    public function handle(SongDeleted $event)
    {
        $song = $event->song;
        $song->playlists()->each(function ($playlist) use ($song) {
            $playlist->songs()->detach($song->code);
        });
    }
}
