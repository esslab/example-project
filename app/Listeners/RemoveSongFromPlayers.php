<?php

namespace App\Listeners;

use App\Events\SongDeleted;

class RemoveSongFromPlayers
{
    /**
     * Handle the event.
     *
     * @param  SongDeleted  $event
     * @return void
     */
    public function handle(SongDeleted $event)
    {
        $song = $event->song;
        $song->players()->each(function ($player) use ($song) {
            $player->songs()->detach($song->code);
        });
    }
}
