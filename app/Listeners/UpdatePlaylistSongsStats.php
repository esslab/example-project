<?php

namespace App\Listeners;

use App\ArtistStat;
use App\Events\PlaylistWasAddedToPlaylist;
use App\Song;
use App\SongStat;

class UpdatePlaylistSongsStats
{
    private $song;
    private $songStat;
    private $artistStat;

    /**
     * Create the event listener.
     *
     * @param Song       $song
     * @param SongStat   $songStat
     * @param ArtistStat $artistStat
     */
    public function __construct(Song $song, SongStat $songStat, ArtistStat $artistStat)
    {
        $this->song = $song;
        $this->songStat = $songStat;
        $this->artistStat = $artistStat;
    }

    /**
     * Handle the event.
     *
     * @param  PlaylistWasAddedToPlaylist  $event
     * @return void
     */
    public function handle(PlaylistWasAddedToPlaylist $event)
    {
        // get Artists ids by songs
        $artistsIds = $this->song->getUniqueArtistsIdsBySongsIds($event->songsIds);
        
        // update SongStat
        $this->songStat->incrementAddedToPlaylistPropertyForSongs($event->songsIds);

        // update Artist stat
        $this->artistStat->incrementAddedToPlaylistPropertyForArtists($artistsIds);
    }
}
