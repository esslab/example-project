<?php

namespace App\Listeners;

use App\CompetitionsList;
use App\Events\SongAddedToPlaylist;

class UpdateSongStat
{
    /**
     * Handle the event.
     *
     * @param  SongAddedToPlaylist  $event
     * @return void
     */
    public function handle(SongAddedToPlaylist $event)
    {
        $song = $event->song;
        $song->stat()->increment('added_to_playlist');

        // if songs participates in Competition, update this field too
        $competition = $song->competitions()->active()->ofType('Top50')->first();
        if (!is_null($competition)) {
            CompetitionsList::where([
                'competition_code' => $competition->code,
                'song_code'        => $song->code,
            ])->increment('added_to_playlist');
        }
    }
}
