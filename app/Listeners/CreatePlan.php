<?php

namespace App\Listeners;

use App\Events\UserCreated;
use App\Exceptions\PlanAlreadyExistException;
use App\Repository\StripeRepository;

class CreatePlan
{
    /**
     * Handle the event.
     *
     * @param UserCreated|CreatePlan $event
     * @throws \Stripe\Error\Base
     */
    public function handle(UserCreated $event)
    {
        $stripe = new StripeRepository();
        $plan = $stripe->retrievePlan($event->user->code);
        if ($plan) {
            throw new PlanAlreadyExistException($event->user->code);
        } else {
            $stripe->createPlan($event->user, $event->price);
        }
    }
}
