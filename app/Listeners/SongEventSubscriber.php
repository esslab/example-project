<?php
namespace App\Listeners;

use App\Events\SongWasLiked;
use App\Events\SongWasUnliked;
use App\Events\SongWasUnvoted;
use App\Events\SongWasVoted;
use App\Services\SongService;
use Illuminate\Events\Dispatcher;

class SongEventSubscriber
{
    /**
     * @var SongService
     */
    private $songService;

    public function __construct(SongService $songService)
    {
        $this->songService = $songService;
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\SongWasLiked',
            'App\Listeners\SongEventSubscriber@onSongWasLiked'
        );
        $events->listen(
            'App\Events\SongWasUnliked',
            'App\Listeners\SongEventSubscriber@onSongWasUnliked'
        );
        $events->listen(
            'App\Events\SongWasVoted',
            'App\Listeners\SongEventSubscriber@onSongWasVoted'
        );
        $events->listen(
            'App\Events\SongWasUnvoted',
            'App\Listeners\SongEventSubscriber@onSongWasUnvoted'
        );
    }

    /**
     * @param SongWasLiked $event
     */
    public function onSongWasLiked(SongWasLiked $event)
    {
        $song = $event->song;
        // Update liked counter
        $song->stat()->increment('liked');

        $this->songService->incrementSongStatInCompetition($song, 'liked');
    }

    /**
     * @param SongWasUnliked $event
     */
    public function onSongWasUnliked(SongWasUnliked $event)
    {
        $song = $event->song;

        $this->songService->decrementSongStat($song, 'liked');

        $this->songService->decrementSongStatInCompetition($song, 'liked');
    }

    /**
     * @param SongWasVoted $event
     */
    public function onSongWasVoted(SongWasVoted $event)
    {
        $song = $event->song;

        $song->stat()->increment('voted');

        $this->songService->incrementSongStatInCompetition($song, 'voted');
    }

    /**
     * @param SongWasUnvoted $event
     */
    public function onSongWasUnvoted(SongWasUnvoted $event)
    {
        $song = $event->song;

        $this->songService->decrementSongStat($song, 'voted');

        $this->songService->decrementSongStatInCompetition($song, 'voted');
    }
}
