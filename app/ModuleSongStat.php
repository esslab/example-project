<?php
namespace App;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModuleSongStat
 * @method static Builder findBySong ($song)
 * @see ModuleSongStat::scopeFindBySong
 * @package App
 * @todo this class can be divided into several classes in different modules,
 * @todo because this class adds some extra functionality to the base model
 */
class ModuleSongStat extends SongStat
{
    public function scopeFindBySong($query, $song)
    {
        return $query->where('song_code', '=', $song);
    }
}
