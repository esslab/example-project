<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistsFollows extends Model
{
    protected $primaryKey = 'user_code';

    protected $fillable = ["user_code", "artist_code"];

    public $incrementing = false;

    public function user()
    {
        return $this->hasOne('App\User', 'code', 'user_code', 'users');
    }

    public function artist()
    {
        return $this->hasOne('App\User', 'code', 'artist_code', 'users');
    }
}
