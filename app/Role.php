<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $primaryKey = 'code';

    public $incrementing = false;

    protected $fillable = ['title', 'code'];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user', 'role_code', 'user_code');
    }

    /**
     * @param Builder $q
     * @return Builder
     */
    public function scopeAdmin($q)
    {
        return $this->scopeByTitle($q, 'Admin');
    }

    /**
     * @param Builder $q
     * @param string $title
     * @return Builder
     */
    public function scopeByTitle($q, $title)
    {
        return $q->where('title', $title);
    }
}
