<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaLike extends Model
{
    protected $fillable = ['item_code', 'user_code'];

    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_code', 'code');
    }

    public function item()
    {
        return $this->belongsTo(GalleryItem::class, 'item_code', 'code');
    }
}
