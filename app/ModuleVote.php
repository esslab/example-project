<?php
namespace App;

use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModuleVote
 * @property string competition_code
 * @property string song_code
 * @property string user_code
 * @method static Builder findByCompetitionAndSongAndUser ($competition, $song, $user)
 * @see ModuleVote::scopeFindByCompetitionAndSongAndUser
 * @package App
 * @todo this class can be divided into several classes in different modules,
 * @todo because this class adds some extra functionality to the base model
 */
class ModuleVote extends Vote
{
    public function scopeFindByCompetitionAndSongAndUser($query, $competition, $song, $user)
    {
        return $query->where('competition_code', '=', $competition)
            ->where('song_code', '=', $song)
            ->where('user_code', '=', $user);
    }
}
