<?php
namespace App\Exceptions;

class CompetitionUpdateException extends InternalErrorException
{
    /**
     * CompetitionUpdateException constructor.
     *
     * @param string|null $message
     */
    public function __construct($message = null)
    {
        $message = $message ?? "Competition can't be updated properly.";
        parent::__construct($message);
    }
}
