<?php
namespace App\Exceptions;

class UserStatsException extends InternalErrorException
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
