<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class NotFoundException extends HttpException
{
    /**
     * NotFoundException constructor.
     *
     * @param string|null    $message
     * @param null|Exception $exception
     */
    public function __construct($message = null, Exception $exception = null)
    {
        $message = $message ?? 'Not found.';
        parent::__construct(Response::HTTP_NOT_FOUND, $message, $exception, [], Response::HTTP_NOT_FOUND);
    }
}
