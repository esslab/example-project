<?php
namespace App\Exceptions\Console;

use Exception;

class CompetitionsBadWeekStepValue extends Exception
{
    public function __construct()
    {
        parent::__construct('Wrong weeks step value');
    }
}
