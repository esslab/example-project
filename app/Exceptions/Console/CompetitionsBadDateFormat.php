<?php
namespace App\Exceptions\Console;

use Exception;

class CompetitionsBadDateFormat extends Exception
{
    public function __construct()
    {
        parent::__construct('Provided wrong date format.');
    }
}
