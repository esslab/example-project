<?php
namespace App\Exceptions\Console;

use Exception;

class CompetitionsListSavingException extends Exception
{
    public function __construct(Exception $exception)
    {
        parent::__construct(
            'During saving competitions list occurs exception with message = ' . $exception->getMessage(),
            $exception->getCode(),
            $exception
        );
    }
}
