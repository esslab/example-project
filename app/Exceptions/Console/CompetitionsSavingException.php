<?php
namespace App\Exceptions\Console;

use Exception;

class CompetitionsSavingException extends Exception
{
    public function __construct(Exception $exception)
    {
        parent::__construct(
            'During saving competitions occurs exception with message = ' . $exception->getMessage(),
            500,
            $exception
        );
    }
}
