<?php
namespace App\Exceptions\Console;

use Exception;

class CompetitionsEmptyPlanException extends Exception
{
    public function __construct()
    {
        parent::__construct('Competitions plan is empty');
    }
}
