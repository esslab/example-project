<?php

namespace App\Exceptions;

use App;
use App\Contracts\ISentryReport;
use App\Contracts\ISlackReport;
use App\Classes\Traits\SentryReport as SentryReportTrait;
use App\Classes\Traits\SlackReport as SlackReportTrait;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response as SymphonyHttpResponse;
use Logger;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    use SlackReportTrait, SentryReportTrait;
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Validation\ValidationException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($this->shouldntReport($exception)) {
            return;
        }

        if ($exception instanceof ISlackReport) {
            $title = get_class($exception);
            $this->reportToSlack($exception->getMessage(), $title, $exception->getSlackFields());
        }

        $this->reportExceptionToSentry($exception);

        Logger::logException($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return JsonResponse|SymphonyHttpResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ModelNotFoundException) {
            return $this->renderModelNotFoundException($exception);
        }
        if ($exception instanceof NotFoundHttpException) {
            return $this->renderNotFoundException($exception);
        }
        $statusCode = ($exception->getCode() === 0) ? SymphonyHttpResponse::HTTP_BAD_REQUEST : $exception->getCode();
        $validationErrors = null;
        $headers = [];
        $message = null;

        if ($exception instanceof ValidationException) {
            $validationErrors = $exception->validator->errors()->getMessages();
            $statusCode = SymphonyHttpResponse::HTTP_UNPROCESSABLE_ENTITY;
        } elseif ($exception instanceof QueryException) {
            if (App::environment('production')) {
                $message = "Internal server error.";
            } else {
                $pdoException = $exception->getPrevious();
                $message = "DB update error: " . $pdoException->getMessage();
            }
            $statusCode = SymphonyHttpResponse::HTTP_INTERNAL_SERVER_ERROR;
        } elseif ($exception instanceof AuthorizationException) {
            $statusCode = SymphonyHttpResponse::HTTP_FORBIDDEN;
        } elseif ($exception instanceof HttpException) {
            $statusCode = $exception->getStatusCode();
            $headers = $exception->getHeaders();
        }

        $error = $this->getFormattedError($exception, $statusCode, $message, $validationErrors);

        return Response::json(compact("error"), $statusCode)->withHeaders($headers);
    }

    /**
     * @param Exception $exception
     * @param           $statusCode
     * @param           $message
     * @param           $validationErrors
     * @return array
     */
    private function getFormattedError(Exception $exception, $statusCode, $message, $validationErrors = null)
    {
        return [
            "statusCode"       => $statusCode,
            "message"          => $message ?? $exception->getMessage(),
            "validationErrors" => $validationErrors
            //Todo: enable this fields later
            // "code"          => 3526,
            // "link"          => "http://docs.mysite.com/errors/3526/"
            // "description"   => "A UserID is required to edit a user.",
        ];
    }

    /**
     * @param ModelNotFoundException $exception
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderModelNotFoundException(ModelNotFoundException $exception)
    {
        $statusCode = 404;

        switch ($exception->getModel()) {
            case 'App\Repository\Modules\Competitions\ApiCompetitionSingle':
            case 'App\Repository\Modules\Competitions\ApiCompetitionsCollection':
            case 'App\Competition':
                $message = 'Competition was not found.';
                break;
            case 'App\Song':
                $message = 'Song was not found.';
                break;
            case 'App\GalleryItem':
                $message = 'Media item was not found.';
                break;
            default:
                $message = $exception->getMessage();
        }

        $error = $this->getFormattedError($exception, $statusCode, $message);

        return Response::json(compact("error"), $statusCode);
    }

    /**
     * @param NotFoundHttpException $exception
     * @return \Illuminate\Http\JsonResponse
     */
    private function renderNotFoundException(NotFoundHttpException $exception)
    {
        $statusCode = $exception->getStatusCode() ?? 400;
        $message = $exception->getMessage();
        if (empty($message)) {
            $message = "Can't find anything for the request. Probably resource identifier has incorrect format.";
        }
        $error = $this->getFormattedError($exception, $statusCode, $message);

        return Response::json(compact("error"), $statusCode);
    }
}
