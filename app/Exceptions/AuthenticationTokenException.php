<?php
namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Exception;
use Illuminate\Http\Response;

class AuthenticationTokenException extends HttpException
{
    public function __construct($message)
    {
        parent::__construct(Response::HTTP_FORBIDDEN, $message, null, [], Response::HTTP_FORBIDDEN);
    }
}
