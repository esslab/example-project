<?php
namespace App\Exceptions;

class ExtractSongUuidException extends InternalErrorException
{
    public function __construct($message = "Can't extract UUID from file name.")
    {
        parent::__construct($message);
    }
}
