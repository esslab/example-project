<?php
namespace App\Exceptions;

use App\Contracts\ISentryReport;

class InternalErrorException extends \Exception implements ISentryReport
{
    public function __construct($message = "Internal server error.", $code = 500)
    {
        parent::__construct($message, $code);
    }
}
