<?php
namespace App\Exceptions\Repository;

use Exception;
use Illuminate\Http\Response;

class Auth0BadAPIRequestException extends Exception
{
    public function __construct(string $url, $result, int $code)
    {
        $message = "Auth0 API call to end-point $url returned with status = $code.";
        if (is_array($result)) {
            $message .= " Error: ". $result['error'] . ". Error description: " . $result['error_description'];
        } else {
            $message .= " Error: ". $result;
        }

        parent::__construct($message, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
