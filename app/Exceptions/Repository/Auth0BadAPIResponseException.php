<?php
namespace App\Exceptions\Repository;

use Exception;
use Illuminate\Http\Response;

class Auth0BadAPIResponseException extends Exception
{
    public function __construct()
    {
        parent::__construct("Failed to fetch Auth0 API response.", Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
