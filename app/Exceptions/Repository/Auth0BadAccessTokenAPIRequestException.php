<?php
namespace App\Exceptions\Repository;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Auth0BadAccessTokenAPIRequestException extends HttpException
{
    public function __construct(\Exception $exception)
    {
        parent::__construct(
            Response::HTTP_BAD_REQUEST,
            $exception->getMessage(),
            $exception,
            [],
            $exception->getCode()
        );
    }
}
