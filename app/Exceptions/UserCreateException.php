<?php
namespace App\Exceptions;

class UserCreateException extends InternalErrorException
{
    public function __construct(\Exception $exception)
    {
        $message = 'Some errors occurs during creating a new User.';
        if ($exceptionMessage = $exception->getMessage()) {
            $message .= "\n$exceptionMessage";
        }
        parent::__construct($message);
    }
}
