<?php
namespace App\Exceptions;

class StripeIncorrectEnvironmentException extends InternalErrorException
{
    public function __construct()
    {
        $message = 'You are trying to use incorrect environment keys. ' .
            'Please check your Stripe environment keys.';
        parent::__construct($message);
    }
}
