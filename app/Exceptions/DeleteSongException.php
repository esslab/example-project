<?php
namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Http\Response;

class DeleteSongException extends HttpException
{
    /**
     * DeleteSongException constructor.
     *
     * @param string|null $message
     */
    public function __construct($message = null)
    {
        parent::__construct(Response::HTTP_BAD_REQUEST, $message, null, [], Response::HTTP_BAD_REQUEST);
    }
}
