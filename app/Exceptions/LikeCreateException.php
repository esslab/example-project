<?php
namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;

class LikeCreateException extends HttpException
{
    /**
     * LikeCreateException constructor.
     *
     * @param string    $message
     * @param int       $code
     * @param Exception $exception
     */
    public function __construct($message = null, $code = 400, Exception $exception = null)
    {
        if (!$message) {
            $message = $exception->getMessage();
        }
        if (empty($message)) {
            $message = 'Like add exception.';
        }

        parent::__construct($code, $message, $exception, [], $code);
    }
}
