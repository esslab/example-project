<?php
namespace App\Exceptions;

use App\Contracts\ISentryReport;
use App\Contracts\ISlackReport;

class PlanAlreadyExistException extends InternalErrorException implements ISlackReport, ISentryReport
{
    protected $message;
    /**
     * The attachment's fields.
     *
     * @var array
     */
    protected $slackFields;

    /**
     * PlanAlreadyExistException constructor.
     *
     * @param string $plan
     */
    public function __construct($plan)
    {
        $this->message = 'Stripe plan [' . $plan . '] already exists and user name is differ from plan name.';

        $this->slackFields = ['Plan' => $plan];

        parent::__construct($this->message);
    }

    /**
     * @return array
     */
    public function getSlackFields()
    {
        return $this->slackFields;
    }
}
