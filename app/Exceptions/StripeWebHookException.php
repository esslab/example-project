<?php

namespace App\Exceptions;

use App\Contracts\ISentryReport;
use Exception;

class StripeWebHookException extends Exception implements ISentryReport
{
    /**
     * StripeWebHookException constructor.
     * @param string $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
