<?php

namespace App\Exceptions;

use Carbon\Carbon;
use Exception;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class InvalidTokenException extends UnauthorizedHttpException
{
    public function __construct($token = null, Exception $e = null, $message = null)
    {
        $sentry = app('sentry');
        $sentry->user_context([
            'time' => Carbon::now()->toDateTimeString(),
            'token' => $token
        ]);
        $message = $this->message($e, $message);
        $challenge = 'Bearer '. $token;

        parent::__construct($challenge, $message, $e);
    }

    /**
     * @param Exception $e
     * @param string    $message
     * @return string
     */
    private function message(Exception $e = null, $message = null): string
    {
        if ($message) {
        } elseif ($e) {
            $message = 'Invalid token. ' . $e->getMessage();
        } else {
            $message = 'Invalid token.';
        }

        return $message;
    }
}
