<?php

namespace App\Contracts;

interface ISlackReport
{
    public function getSlackFields();
}
