<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SongPlay extends Model
{
    protected $table = 'song_plays';
    protected $guarded = '*';
    public $incrementing = false;
}
