<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlaylistsSong extends Model
{
    protected $table = self::TABLE_NAME;

    protected $primaryKey = null;

    public $incrementing = false;

    const TABLE_NAME = 'playlists_songs';

    const FIELD_PLAYLIST_CODE = 'playlist_code';
    const FIELD_SONG_CODE = 'song_code';
    const FIELD_PRIVACY = 'privacy';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';
}
