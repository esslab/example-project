<?php

namespace App;

use App\Services\ImageService;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GalleryItem
 * @package App
 * @property string code
 * @property string gallery_code
 * @property string user_code
 * @property integer status
 * @property string type
 * @property string source_file
 * @property integer privacy
 * @property string thumbnail
 * @property string created_at
 * @property string updated_at
 * @property string title
 * @property string duration
 * @property string access
 * @property string transcodingCompleted
 * @method static GalleryItem byUsers($ids)|Builder byUsers($ids)
 * @see GalleryItem::scopeByUsers($ids)
 * @method static GalleryItem recent()|Builder recent()
 * @see GalleryItem::scopeRecent()
 */
class GalleryItem extends Model
{
    protected $table = 'galleries_items';

    protected $primaryKey = 'code';

    protected $fillable = ["status", "privacy", "title", "access", "duration", "transcodingCompleted"];

    protected $hidden = ['user_code', 'status', 'privacy', 'created_at', 'updated_at', 'source_file'];

    protected $appends = ['comments', 'can_view', 'sources', 'time_difference', 'available_actions', 'stat'];

    protected $with = ['user'];

    public $incrementing = false;

    public function gallery()
    {
        return $this->belongsTo(Gallery::class, 'gallery_code', 'code');
    }

    public function scopeForUser($query, $user)
    {
        return $query->where(['user_code' => $user]);
    }

    public function getAvailableActionsAttribute()
    {
        $authCheck = Auth::check();
        $user = Auth::user();
        return [
            'like'    => $authCheck ? $user->can('like', $this) : false,
            'dislike' => $authCheck ? $user->can('dislike', $this) : false,
            'view'    => $this->can_view,
        ];
    }

    public function getCommentsAttribute()
    {
        return [];
    }

    public function getStatAttribute()
    {
        return [
            'likes' => MediaLike::where('item_code', $this->code)->count()
        ];
    }

    public function user()
    {
        return $this->hasOne(Artist::class, 'code', 'user_code');
    }

    public function getSourcesAttribute()
    {
        return ImageService::getGalleryItemThumbDimensions($this->source_file);
    }

    public function getCanViewAttribute()
    {
        if (!isset($this->attributes['can_view'])) {
            $this->attributes['can_view'] = (
                $this->access == 'all' ||
                (Auth::check() && Auth::user()->subscribed($this->user_code)) ||
                (Auth::check() && Auth::user()->code == $this->user_code)
            );
        }
        return $this->attributes['can_view'];
    }

    public function setSourceFileAttribute($value)
    {
        $convertedSourceFileName = str_replace(config('app.app_bucket_url'), config('app.app_files_url'), $value);
        $this->attributes['source_file'] = $convertedSourceFileName;
    }

    /**
     * @param Builder $query
     * @param array $artistsIds
     * @return mixed|Builder|static
     */
    public function scopeByUsers($query, array $artistsIds)
    {
        return $query->whereIn('user_code', $artistsIds);
    }

    /**
     * @param Builder $query
     * @return mixed|Builder|static
     */
    public function scopeRecent($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeFavoritesList($query, $user)
    {
        return $query->join('artists_follows', 'galleries_items.user_code', '=', 'artists_follows.artist_code')
            ->where('artists_follows.user_code', $user)
            ->select(['galleries_items.*']);
    }

    /**
     * Returns a time difference between current time and created_at
     * @return int
     */
    public function getTimeDifferenceAttribute()
    {
        return Carbon::now()->diffInSeconds($this->created_at);
    }
}
