<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PayoutDetails extends Model
{
    public $incrementing = false;
    protected $table = 'users_payout_details';
    protected $primaryKey = 'user_code';
    public $fillable = [
        'first_name',
        'last_name',
        'bank_name',
        'swift',
        'account_number',
        'birthday'
    ];

    public function setBirthdayAttribute($value)
    {
        $this->attributes['birthday'] = Carbon::parse($value)->format('Y-m-d');
    }
}
