<?php

if (! function_exists('filename_from_path')) {
    /**
     * Return file name from path string.
     *
     * @param string $string
     * @return string | null
     */
    function filename_from_path($string)
    {
        $fileInfo = pathinfo($string);
        return $fileInfo['filename'] ?? null;
    }
}
