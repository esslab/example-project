<?php
namespace App\Repository;

use App\Auth0ManagementAPI;
use App\Events\UserCreated;
use App\Events\ArtistFollow;
use App\Exceptions\UserCreateException;
use App\Exceptions\UserStatsException;
use App\Role;
use App\Services\CompetitionService;
use App\Services\SongService;
use App\User as UserModel;
use Auth;
use Auth0\Login\Contract\Auth0UserRepository;
use App\User;
use App\Competition;
use App\Auth0AuthenticationAPI;
use DB;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserRepository implements Auth0UserRepository
{
    private $auth0_id;
    private $provider;

    const ERROR_DUPLICATE_ENTRY = 1062;

    public function getUserByDecodedJWT($jwtUser)
    {
        $this->auth0_id = $jwtUser->sub;
        $this->provider = $this->getProviderFromAuth0UserId($jwtUser);

        return $this->upsertUser($jwtUser);
    }

    /**
     * @param array $userInfo
     * @return User
     */
    public function getUserByUserInfo($userInfo)
    {
        return $this->upsertUser($userInfo['profile']);
    }

    /**
     * @param $userData
     * @return User
     * @throws \Exception
     */
    protected function upsertUser($userData): User
    {
        $user = User::where('auth0_id', $this->auth0_id)->orWhere('email', $userData->email)->first();
        if ($user != null) {
            return $user;
        }
        $userData->picture = !empty($userData->picture) ? $this->saveAvatarToStorage($userData->picture) : null;
        $user = $this->fillUserObject($userData);
        $this->createUser($user);

        return $user;
    }

    /**
     * @param $user
     * @throws Exception
     */
    private function createUser($user)
    {
        $stripeRepository = new StripeRepository();
        $userRoleCode = Role::byTitle('User')->first()->code;
        DB::beginTransaction();
        try {
            $user->save();
            $user->roles()->attach($userRoleCode);
            $user->stat()->create([]);
            $this->createPlayer($user);
            $customer = $stripeRepository->createCustomer($user);
            $user->stripe_id = $customer->id;
            $user->update();

            $this->followTop6BimonthlyArtists($user);
            event(new UserCreated($user));

            DB::commit();
        } catch (\Illuminate\Database\QueryException $databaseException) {
            DB::rollBack();
            if ($databaseException->errorInfo[1] === self::ERROR_DUPLICATE_ENTRY) {
                throw new HttpException(Response::HTTP_CONFLICT, 'User already exist. Please try to reload the page.');
            }
            throw $databaseException;
        } catch (Exception $e) {
            DB::rollBack();
            throw new UserCreateException($e);
        }
    }

    /**
     * @param $identifier
     * @return User|array|null
     * @todo never used -> can be deleted
     */
    public function getUserByIdentifier($identifier)
    {
        $user = app()->make('auth0')->getUser();
        if ($user === null) {
            return null;
        }
        $user = $this->getUserByUserInfo($user);
        if ($user && $user->code == $identifier) {
            return $user;
        }

        return null;
    }

    /**
     * Make new User following for Top 6 artists from last Bimonthly competition
     * @param User $user
     */
    private function followTop6BimonthlyArtists(User $user)
    {
        $competition = Competition::with('songs.user')->ofType('Bimonthly')->recent()->first();
        if (!is_null($competition) && $competition->songs->isNotEmpty()) {
            $competition->songs->each(function ($song, $key) use ($user) {
                if (!$user->isFollowingArtist($song->user->code)) {
                    $song->user->followedByUsers()->attach($user->code);
                    event(new ArtistFollow($song->user));
                }
            });
        }
    }

    /**
     * @param $providedUserData
     * @return User
     * @throws \App\Exceptions\Repository\Auth0BadAPIRequestException
     * @throws \App\Exceptions\Repository\Auth0BadAPIResponseException
     */
    private function fillUserObject($providedUserData)
    {
        $user = new UserModel;
        $user->auth0_id  = $this->auth0_id;
        $user->email     = $providedUserData->email;
        $user->name      = !empty($providedUserData->nickname) ? $providedUserData->nickname
            : $providedUserData->name;
        $user->location  = '';
        $user->avatar    = $providedUserData->picture;
        $user->provider  = $this->provider;

        return $user;
    }

    /**
     * @param $user
     */
    private function createPlayer($user)
    {
        $newPlayerData = [
            'timePaused'  => 0,
            'trackPaused' => 0,
            'tracksOrder' => '',
            'isShuffle'   => 0,
            'repeatState'    => 0
        ];
        $player = $user->player()->create($newPlayerData);
        $competitionService = new CompetitionService;
        $songs = $competitionService->lastCompetition6Songs();
        if (!$songs) {
            $songsCount = 6;
            $songService = new SongService;
            $songs = $songService->newSongs($songsCount);
        }

        $songsCodes = $songs->pluck('code')->all();
        $player->songs()->sync($songsCodes);
    }

    /**
     * Delete User from Auth0 service and delete plan in Stripe service
     *
     * @param $userCode
     * @param $auth0UserId
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function deleteUser($userCode, $auth0UserId)
    {
        $auth0ManagementAPI = new Auth0ManagementAPI();
        $auth0ManagementAPI->deleteUser($auth0UserId);
        $stripe = new StripeRepository();
        $stripe->deletePlan($userCode);
    }

    /**
     * @param $jwtUser
     * @return string
     */
    private function getProviderFromAuth0UserId($jwtUser): string
    {
        return substr($jwtUser->sub, 0, strpos($jwtUser->sub, '|'));
    }

    /**
     * @param $artist
     * @param $user
     * @throws UserStatsException
     */
    public static function follow($artist, $user = null): void
    {
        $user = $user ?? Auth::id();
        $artist->followedByUsers()->attach($user);
        if ($artist->stat()->count()) {
            $artist->stat()->increment('followed');
        } else {
            throw new UserStatsException('User has no stats data');
        }
    }

    /**
     * @param $avatar
     * @return string
     */
    private function saveAvatarToStorage($avatar): string
    {
        $faker = \Faker\Factory::create();
        $ext = pathinfo($avatar, PATHINFO_EXTENSION);
        $ext = $ext ? $ext : 'jpg';
        $saveUrl = 'avatars/source/' . $faker->uuid . '.' . $ext;
        Storage::disk('s3')->put($saveUrl, file_get_contents($avatar));
        return config('app.app_files_url') . $saveUrl;
    }
}
