<?php
namespace App\Repository;

use Aws\S3\PostObjectV4;
use Aws\S3\S3Client;
use Illuminate\Http\Response;

class AwsRepository
{
    /**
     * @return S3Client
     */
    public static function getS3Client()
    {
        return new S3Client([
            'version'     => config('aws.version'),
            'region'      => config('aws.region'),
            'credentials' => [
                'key'    => config('aws.AWS_ACCESS_KEY_ID'),
                'secret' => config('aws.AWS_SECRET_ACCESS_KEY')
            ]
        ]);
    }

    /**
     * @param $directory
     * @return Response
     */
    public static function getAmazonPresignedPOST($directory = '')
    {
        $s3 = AwsRepository::getS3Client();
        $bucket =  config('aws.s3.bucket');
        $formInputs = ['acl' => 'public-read'];
        $options = [
            ['acl' => 'public-read'],
            ['bucket' => $bucket],
            ['starts-with', '$key', $directory],
            ['starts-with', '$Cache-Control', '']
        ];
        $expires = '+2 hours';
        $postObject = new PostObjectV4(
            $s3,
            $bucket,
            $formInputs,
            $options,
            $expires
        );
        // to get form attributes make $postObject->getFormAttributes();
        $formInputs = $postObject->getFormInputs();
        return $formInputs;
    }
}
