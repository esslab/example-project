<?php
namespace App\Repository;

use App\Exceptions\StripeIncorrectEnvironmentException;
use App\User;
use Auth;
use Illuminate\Support\Facades\App;
use Stripe\Error\InvalidRequest;

class StripeRepository
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $this->throwExceptionIfTryingToUseDevKeysLocally();
    }

    /**
     * @param $user
     * @return \Stripe\Customer
     */
    public function createCustomer($user)
    {
        return \Stripe\Customer::create(array(
            "email" => $user->email,
        ));
    }

    public function deleteAllCustomers()
    {
        do {
            $customers = \Stripe\Customer::all(array("limit" => 100));
            foreach ($customers['data'] as $customer) {
                $customer->delete();
            }
        } while ($customers['has_more']);
    }

    public function deleteAllPlans()
    {
        do {
            $plans = \Stripe\Plan::all(array("limit" => 100));
            foreach ($plans['data'] as $plan) {
                $plan->delete();
            }
        } while ($plans['has_more']);
    }

    public static function cancelAllSubscriptions()
    {
        do {
            $subs = \Stripe\Subscription::all(array("limit" => 100));
            foreach ($subs['data'] as $sub) {
                $sub->cancel();
            }
        } while ($subs['has_more']);
    }

    /**
     * @param $customer
     * @param $plan_id
     * @return \Stripe\Subscription
     */
    public function createSubscription($customer, $plan_id)
    {
        return \Stripe\Subscription::create(array(
            "customer" => $customer,
            "plan" => $plan_id
        ));
    }

    /**
     * @param User $user
     * @param $price
     * @return \Stripe\Plan
     * @throws \Stripe\Error\Base
     */
    public function createPlan(User $user, $price = 0)
    {
        try {
            return \Stripe\Plan::create(array(
                "name" => $user->name,
                "id" => $user->code,
                "interval" => "month",
                "currency" => "usd",
                "statement_descriptor" => 'SongLine subscription',
                "amount" => $price,
            ));
        } catch (\Stripe\Error\Base $e) {
            throw $e;
        }
    }

    /**
     * @param $plan
     * @return \Stripe\Plan
     * @throws \Stripe\Error\Base
     */
    public function retrievePlan($plan)
    {
        try {
            return \Stripe\Plan::retrieve($plan);
        } catch (\Stripe\Error\Base $e) {
            if ($e->getHttpStatus() == 404) {
                return null;
            }
            throw $e;
        }
    }

    /**
     * @param $artist
     * @param $request
     * @return \Stripe\Plan
     * @throws \Stripe\Error\Base
     */
    //TODO: refactor to static, pass price, not request object
    public function updatePlan($artist, $request)
    {
        try {
            $this->deletePlan($artist->code);
            return $this->createPlan($artist, $request->price);
        } catch (\Stripe\Error\Base $e) {
            throw $e;
        }
    }

    /**
     * @param $plan_id
     * @return \Stripe\Plan
     */
    public function getPlan($plan_id)
    {
        try {
            return \Stripe\Plan::retrieve($plan_id);
        } catch (\Stripe\Error\Base $e) {
            abort($e->getHttpStatus(), $e->getMessage());
        }
    }

    /**
     * @param $customer
     * @param null $limit
     * @param null $dateFromTimestamp
     * @param null $dateToTimestamp
     * @return \Stripe\Collection
     */
    public function getInvoices($customer, $limit = null, $dateFromTimestamp = null, $dateToTimestamp = null)
    {
        return \Stripe\Invoice::all([
            "customer"  => $customer,
            "limit"     => $limit,
            "date[gte]" => $dateFromTimestamp,
            "date[lt]" => $dateToTimestamp
        ]);
    }

    public function getEvents(
        $type,
        $limit = null,
        $startingAfter = null,
        $dateFromTimestamp = null,
        $dateToTimestamp = null
    ) {
        return \Stripe\Event::all([
            "type"  => $type,
            "limit"     => $limit,
            "starting_after" => $startingAfter,
            "created[gte]" => $dateFromTimestamp,
            "created[lt]" => $dateToTimestamp,
        ]);
    }

    /**
     * @param $artistCode
     */
    public function deletePlan($artistCode)
    {
        try {
            $plan = \Stripe\Plan::retrieve($artistCode);
            $plan->delete();
        } catch (InvalidRequest $e) {
            return;
        }
    }

    /**
     * Delete all data from Stripe service
     */
    public function deleteAllStripeData()
    {
        $this->cancelAllSubscriptions();
        $this->deleteAllCustomers();
        $this->deleteAllPlans();
    }

    /**
     * @param null $cardId
     * @return bool
     */
    public function removeCard($cardId = null)
    {
        $customer = \Stripe\Customer::retrieve(Auth::user()->stripe_id);
        if ($customer->sources->data) {
            $cardId = $cardId ?? $customer->sources->data[0]->id;
            return $customer->sources->retrieve($cardId)->delete();
        } else {
            return false;
        }
    }

    /**
     * @param User $customer
     * @param null $cardId
     * @return array|\Stripe\StripeObject
     */
    public function retrieveCard(User $customer, $cardId = null)
    {
        if (!empty($customer->stripe_id)) {
            $customer = \Stripe\Customer::retrieve($customer->stripe_id);
            if (!empty($customer->sources->data)) {
                $cardId = $customer->sources->data[0]->id;
            }
            if ($cardId) {
                return $customer->sources->retrieve($cardId);
            }
        }
        return null;
    }

    private function throwExceptionIfTryingToUseDevKeysLocally()
    {
        if (App::isLocal() && ('pk_test_7R7fIKecwfuVCgEtBxG9GvJt' !== config('services.stripe.key'))) {
            throw new StripeIncorrectEnvironmentException();
        }
    }
}
