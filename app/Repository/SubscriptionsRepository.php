<?php
namespace App\Repository;

use App\Subscription;
use App\User;
use Auth;
use Carbon\Carbon;

class SubscriptionsRepository
{
    public function __construct()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
    }

    /**
     * @param $plan
     * @param null $user
     * @return Subscription
     */
    public static function subscribeWithoutCard($plan, $user = null)
    {
        $user = $user ?? Auth::user();
        $newSubscription = new Subscription();
        $newSubscription->stripe_id = null;
        $newSubscription->price = 0;
        $newSubscription->user_id = $user->code;
        $newSubscription->stripe_plan = $newSubscription->name = $plan;
        $newSubscription->save();
        return $newSubscription;
    }

    /**
     * @param $subscriptions
     * @param User $artist
     */
    public static function unsubscribeUsers($subscriptions, User $artist)
    {
        foreach ($subscriptions as $subscription) {
            if (empty($subscription->price)) {
                $subscription->ends_at = Carbon::now();
            } else {
                $subscription->cancel();
            }
            if ($artist->plan_cost == 0) {
                self::addFreeSubscriptionAndFollow($artist, $subscription);
            } else {
                $subscription->resubscribe = true;
                $subscription->save();
            }
        }
    }

    /**
     * @param $artist
     * @param null $user
     * @throws \Exception
     */
    public static function addFreeSubscription(User $artist, User $user = null)
    {
        $user = $user ?? Auth::user();
        if ($artist->plan_cost == 0) {
            if ($user->subscription($artist->code) && $user->subscription($artist->code)->onGracePeriod()) {
                $user->subscription($artist->code)->delete();
            }
            SubscriptionsRepository::subscribeWithoutCard($artist->code, $user);
        }
    }

    /**
     * @param $artist
     * @param $subscription
     */
    private static function addFreeSubscriptionAndFollow(User $artist, $subscription)
    {
        $user = User::find($subscription->user_id);
        self::addFreeSubscription($artist, $user);
        if (!$user->isFollowingArtist($artist->code)) {
            UserRepository::follow($artist, $user);
        }
    }
}
