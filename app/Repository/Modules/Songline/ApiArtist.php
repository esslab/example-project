<?php

namespace App\Repository\Modules\Songline;

use App\User;

class ApiUser extends User
{
    public $hidden = ['created_at', 'updated_at', 'user_code'];
}
