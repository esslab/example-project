<?php

namespace App\Repository\Modules\Profiles;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use App\ArtistStat;

/**
 * Class ConsoleArtistProfile
 * @package App\Repositories\Modules\Profiles
 * // PROPERTIES
 * @property string code
 * @property ArtistStat stat
 * // METHODS
 * @method static Builder allArtistsProfiles ()
 * @see ConsoleArtistProfile::scopeAllArtistsProfiles
 * @method static Builder paginatedArtistsProfiles ($offset, $limit)
 * @see ConsoleArtistProfile::scopePaginatedArtistsProfiles
 */
class ConsoleArtistProfile extends User
{
    protected $table = 'users';

    public function stat()
    {
        return $this->hasOne(ArtistStat::class, 'user_code', 'code');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeArtistsProfiles($query)
    {
        return $query->with('stat');
    }

    /**
     * @param Builder $query
     * @param integer $offset
     * @param integer $limit
     * @return Builder
     */
    public function scopePaginatedArtistsProfiles($query, $offset, $limit)
    {
        return $this->scopeArtistsProfiles($query)
            ->orderBy('users.created_at', 'asc')
            ->offset($offset)
            ->limit($limit);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeAllArtistsProfiles($query)
    {
        return $this->scopeArtistsProfiles($query);
    }
}
