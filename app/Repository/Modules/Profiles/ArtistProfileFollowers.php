<?php
namespace App\Repository\Modules\Profiles;

use App\ArtistsFollows;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ArtistsProfileFollowers
 * @package App\Repository\Modules\Profiles
 * @method static Builder canUserFollowArtist ($artist, $user)
 * @see ArtistProfileFollowers::scopeCanUserFollowArtist
 */
class ArtistProfileFollowers extends ArtistsFollows
{
    protected $table = 'artists_follows';

    /**
     * @param Builder $query
     * @param string $artist
     * @param string $user
     * @return Builder
     */
    public function scopeCanUserFollowArtist($query, $artist, $user)
    {
        return $query->where('artist_code', $artist)
            ->where('user_code', $user);
    }
}
