<?php

namespace App\Repository\Modules\Profiles;

use App\Song;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ConsoleArtistSong
 * @package App\Repository\Modules\Profiles
 * // PROPERTIES
 * @property string code
 * @property string user_code
 * @property integer liked
 * @property integer voted
 * @property integer added_to_playlist
 * // METHODS
 * @method static Builder allSongsForArtist ($artist)
 * @see ConsoleArtistSong::scopeAllSongsForArtist
 */
class ConsoleArtistSong extends Song
{
    protected $table = 'songs';

    protected $casts = [
        'liked' => 'integer',
        'voted' => 'integer',
        'added_to_playlist' => 'integer'
    ];

    /**
     * @param Builder $query
     * @param string $artist
     * @return Builder
     */
    public function scopeAllSongsForArtist($query, $artist)
    {
        return $query->join('songs_stats', 'songs.code', '=', 'songs_stats.song_code')
            ->select([
                'songs.code',
                'songs.user_code',
                'songs_stats.liked',
                'songs_stats.voted',
                'songs_stats.added_to_playlist'
            ])
            ->where('user_code', $artist);
    }
}
