<?php
namespace App\Repository\Modules\Profiles;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Class ApiArtists
 * @property string user_code
 * @package App\Repository\Modules\Profiles
 * @method static Builder profiles ($request)
 * @see ApiArtists::scopeProfiles
 */
class ApiArtists extends User
{
    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeWithGenresLanguagesAndStats($query)
    {
        return $query->with('stat', 'genres', 'languages');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithGenresAndLanguages($query)
    {
        return $query->with('genres', 'languages');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithStats($query)
    {
        return $query->leftJoin('artists_stats', 'users.code', 'artists_stats.user_code');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeWithSongs($query)
    {
        return $query->rightJoin('songs', 'users.code', 'songs.user_code')
            ->distinct()
            ->select('users.*', 'artists_stats.*', 'users.code as code');
    }

    /**
     * @param Builder $query
     * @param array $genres
     * @return Builder
     */
    public function scopeFilterByGenres($query, $genres)
    {
        return $query->whereHas('genres', $this->filterByGenres($genres));
    }

    /**
     * @param Builder $query
     * @param array $languages
     * @return Builder
     */
    public function scopeFilterByLanguages($query, $languages)
    {
        return $query->whereHas('languages', $this->filterByLanguages($languages));
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeWinnerArtist($query)
    {
        return $query->whereHas('stat', $this->winnerArtist());
    }

    /**
     * @param Builder $query
     * @param array $orders
     * @return Builder
     */
    public function scopeOrderArtistsProfiles($query, $orders)
    {
        foreach ($orders as $order) {
            $direction = 'asc';
            if (substr($order, 0, 1) == '-') {
                $direction = 'desc';
            }

            $field = substr($order, 1);

            switch ($field) {
                case 'name':
                    $query->orderBy('users.name', $direction);
                    break;
                case 'rating':
                    $query->orderBy('artists_stats.rating', $direction);
                    break;
                case 'created_at':
                default:
                    $query->orderBy('users.created_at', $direction);
            }
        }
        return $query;
    }

    /**
     * @param Builder $query
     * @param integer $offset
     * @param integer $limit
     * @return Builder
     */
    public function scopePaginateArtistsProfiles($query, $offset, $limit)
    {
        return $query->offset($offset)->limit($limit);
    }

    /**
     * @param Builder $query
     * @param Request $request
     * @return Builder
     */
    public function scopeProfiles($query, $request)
    {
        $query = $this->scopeWithGenresAndLanguages($query);
        $query = $this->scopeWithStats($query);
        $query = $this->scopeArtists($query);

        // Filtering
        if ($request->has('genres')) {
            $query = $this->scopeFilterByGenres($query, $request->input('genres'));
        }

        if ($request->has('languages')) {
            $query = $this->scopeFilterByLanguages($query, $request->input('languages'));
        }

        if ($request->has('top6') && $request->input('top6')) {
            $query = $this->scopeWinnerArtist($query);
        }

        // Ordering
        $query = $this->scopeOrderArtistsProfiles($query, $request->input('sort', ['-created_at', '-name']));

        // Limiting
        $query = $this->scopePaginateArtistsProfiles(
            $query,
            $request->input('offset', 0),
            $request->input('limit', 10)
        );

        return $query;
    }

    // public function getCanFollowAttribute()
    // {
    //     return Auth::check()
    //         && strcmp($this->user_code, Auth::id()) !== 0
    //         && ArtistProfileFollowers::canUserFollowArtist($this->user_code, Auth::id())->count() == 0;
    // }

    /**
     * @param array $genres
     * @return \Closure
     */
    private function filterByGenres($genres)
    {
        /**
         * @param Builder $query
         */
        return function ($query) use ($genres) {
            $query->whereIn('code', $genres);
        };
    }

    /**
     * @param array $languages
     * @return \Closure
     */
    private function filterByLanguages($languages)
    {
        /**
         * @param Builder $query
         */
        return function ($query) use ($languages) {
            $query->whereIn('code', $languages);
        };
    }

    /**
     * @return \Closure
     */
    private function winnerArtist()
    {
        /**
         * @param Builder $query
         */
        return function ($query) {
            $query->where('winner', '>', 0);
        };
    }
}
