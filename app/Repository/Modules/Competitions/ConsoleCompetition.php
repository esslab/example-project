<?php
namespace App\Repository\Modules\Competitions;

use App\Competition;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ConsoleCompetition
 * @property string code
 * @property string title
 * @property string date_start
 * @property string date_end
 * @property string winner_song_code
 * @property string winner_artist_code
 * @property string type
 * @property int status
 * @property Collection songs
 * @method static Builder findLastsByType ($type)
 * @see ConsoleCompetition::scopeFindLastsByType
 * @method static Builder findByDateAndType ($date, $type)
 * @see ConsoleCompetition::scopeFindByDateAndType
 * @method static Builder findWhichCanBeFilled ($type, $startDate, $endDate)
 * @see ConsoleCompetition::scopeFindWhichCanBeFilled
 * @method static Builder findWhichCanBeOpened ($date, $type)
 * @see ConsoleCompetition::scopeFindWhichCanBeOpened
 * @method static Builder findWhichCanBeClosed ($date)
 * @see ConsoleCompetition::scopeFindWhichCanBeClosed
 * @method static Builder lastSixFinished ($dateStart)
 * @see ConsoleCompetition::scopeLastSixFinished
 * @package App\Repository\Modules\Competitions
 */
class ConsoleCompetition extends Competition
{
    /**
     * @param $query
     * @param $type
     * @return Builder
     */
    public function scopeFindLastsByType($query, $type)
    {
        return $this->scopeOfType($query, $type)
            ->orderBy('date_end', 'desc');
    }

    /**
     * @param $query
     * @param $type
     * @return Builder
     */
    public function scopeFindFirstsByType($query, $type)
    {
        return $this->scopeOfType($query, $type)
            ->orderBy('date_end', 'asc');
    }


    /**
     * @param $query
     * @param $date
     * @param $type
     * @return Builder
     */
    public function scopeFindByDateAndType($query, $date, $type)
    {
        return $this->scopeOfType($query, $type)
            ->where('date_end', '>=', $date);
    }

    /**
     * @param Builder $query
     * @param $type
     * @param $startDate
     * @param $endDate
     * @return Builder
     */
    public function scopeFindWhichCanBeFilled($query, $type, $startDate, $endDate)
    {
        return $this->scopeOfType($query, $type)
            ->where([
                [self::FIELD_STATUS, self::STATUS_PLANNED],
                [Competition::FIELD_DATE_START, $startDate],
                [Competition::FIELD_DATE_END, $endDate],
            ])
            ->orderBy('date_start')
            ->limit(1);
    }

    /**
     * @param Builder $query
     * @param string $dateEnd
     * @return Builder
     */
    public function scopeFindWhichCanBeClosed($query, $dateEnd)
    {
        $query->where([
            [self::FIELD_STATUS, '<>', self::STATUS_FINISHED],
            [self::FIELD_DATE_END, '<=', $dateEnd]
        ]);

        return $query;
    }

    /**
     * @param Builder $query
     * @param string  $dateStart
     * @param string  $type
     * @return Builder
     */
    public function scopeFindWhichCanBeOpened($query, $dateStart, $type)
    {
        return $query->where([
            [self::FIELD_STATUS, self::STATUS_PLANNED],
            [self::FIELD_TYPE, $type],
            [self::FIELD_DATE_START, $dateStart]
        ])
            ->whereHas('songs');
    }

    /**
     * @param Builder $query
     * @param string $dateStart
     * @return Builder
     */
    public function scopeLastSixFinished($query, $dateStart)
    {
        return $query->where(self::FIELD_STATUS, self::STATUS_FINISHED)
            ->where(self::FIELD_DATE_END, '<', $dateStart)
            ->orderBy(self::FIELD_DATE_START, 'desc')
            ->limit(6);
    }
}
