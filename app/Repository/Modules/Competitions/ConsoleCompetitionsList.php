<?php

namespace App\Repository\Modules\Competitions;

use App\CompetitionsList;

/**
 * Class ConsoleCompetitionsList
 * @property string competition_code
 * @property string song_code
 * @property string created_at
 * @property string updated_at
 * @package App\Repository\Modules\Competitions
 */
class ConsoleCompetitionsList extends CompetitionsList
{
}
