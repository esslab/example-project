<?php
namespace App\Repository\Modules\Competitions;

use App\Vote;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ConsoleVotes
 * @method static Builder findVotesForSongInCompetition ($competition, $song, $start, $end)
 * @see ConsoleVotes::scopeFindVotesForSongInCompetition
 * @package App\Repository\Modules\Competitions
 */
class ConsoleVotes extends Vote
{
    /**
     * Search for votes for particular song in a limited period of time
     * @param Builder $query
     * @param string $competition Competition's code
     * @param string $song Song's code
     * @param string $start Date Start
     * @param string $end Date End
     * @return Builder
     */
    public function scopeFindVotesForSongInCompetition($query, $competition, $song, $start, $end)
    {
        return $query->where(self::FIELD_COMPETITION_CODE, $competition)
            ->where(self::FIELD_SONG_CODE, $song)
            ->where(self::FIELD_CREATED_AT, '>=', $start)
            ->where(self::FIELD_CREATED_AT, '<=', $end);
    }
}
