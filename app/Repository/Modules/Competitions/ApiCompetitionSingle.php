<?php

namespace App\Repository\Modules\Competitions;

use Illuminate\Database\Eloquent\Builder;
use App\Competition;
use App\Song;
use Auth;
use Carbon\Carbon;

/**
 * Class ApiCompetitionSingle
 * @property string date_start
 * @property string date_end
 * @property string type
 * @package App\Repository\Modules\Competitions
 * @method static ApiCompetitionSingle detailed()
 * @see ApiCompetitionSingle::scopeDetailed
 * @method static ApiCompetitionSingle currentOrPrevious($date)
 * @see ApiCompetitionSingle::scopeCurrentOrPrevious
 */
class ApiCompetitionSingle extends Competition
{
    protected $appends = ['next', 'previous'];

    /**
     * Returns a single Active(current) or Previous competition.
     *
     * @param Builder $query
     * @param Carbon  $dateEnd
     * @return Builder
     */
    public function scopeCurrentOrPrevious($query, Carbon $dateEnd)
    {
        return $this->scopeDetailed($query)
            ->where([
                ['status', '<>', Competition::STATUS_PLANNED],
                ['date_end', '<=', $dateEnd]
            ])
            ->orderBy('date_end', 'desc');
    }

    /**
     * Returns a detailed information about competition,
     * including songs data.
     * @param Builder $query
     * @return Builder
     */
    public function scopeDetailed($query)
    {
        $user = Auth::user();
        $userId = $user->code ?? false;

        if ($user && $user->isAdmin() && starts_with($_SERVER['REQUEST_URI'], '/api/admin')) {
            $query->with([
                'songs.user',
                'songs.competitionsStats' => function ($q) {
                    $q->select(
                        'song_code',
                        'competition_code',
                        'position',
                        'liked',
                        'voted',
                        'played',
                        'added_to_playlist'
                    )
                        ->orderBy('voted', 'desc')
                        ->orderBy('liked', 'desc')
                        ->orderBy('played', 'desc')
                        ->orderBy('added_to_playlist', 'desc')
                        ->orderBy('position');
                }
            ]);
        } else {
            $query->with([
                'songs',
                'songs.user',
                'songs.competitionsStats' => function ($q) {
                    $q->select(
                        'song_code',
                        'competition_code',
                        'position',
                        'liked',
                        'voted',
                        'played',
                        'added_to_playlist'
                    )
                        ->orderBy('voted', 'desc')
                        ->orderBy('played', 'desc')
                        ->orderBy('added_to_playlist', 'desc')
                        ->orderBy('position');
                },
                'songs.genres',
                'songs.languages',
                'songs.competitions'      => function ($query) use ($userId) {
                    $query->select('code', 'title', 'date_start', 'date_end');
                    $query->where('status', '1');
                    if ($userId) {
                        $query->with([
                            'votes' => function ($query) use ($userId) {
                                $query->where('user_code', '=', $userId);
                            }
                        ]);
                    }
                }
            ]);
            if ($userId) {
                $query->with([
                    'songs.likes' => function ($query) use ($userId) {
                        $query->where('user_code', '=', $userId);
                    }
                ]);
            }
        }

        return $query;
    }

    /**
     * Return next attribute
     * @return array|null
     */
    public function getNextAttribute()
    {
        return ApiCompetitionsCollection::nextCompetition($this->date_end)->ofType($this->type)->first();
    }

    /**
     * Return previous attribute
     * @return array|null
     */
    public function getPreviousAttribute()
    {
        return null;
    }

    public function songs()
    {
        return $this->belongsToMany(Song::class, 'competitions_list', 'competition_code', 'song_code')
            ->withPivot(['position', 'liked', 'voted', 'played', 'added_to_playlist'])
            ->orderBy('voted', 'desc')
            ->orderBy('played', 'desc')
            ->orderBy('added_to_playlist', 'desc')
            ->orderBy('position')
            ->withTrashed()
            ->withTimestamps();
    }
}
