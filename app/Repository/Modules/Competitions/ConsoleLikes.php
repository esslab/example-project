<?php

namespace App\Repository\Modules\Competitions;

use App\Like;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ConsoleLikes
 * @package App\Repository\Modules\Competitions
 * @method static Builder findLikesForSongInCompetition ($song, $start, $end)
 * @see ConsoleLikes::scopeFindLikesForSongInCompetition
 */
class ConsoleLikes extends Like
{
    /**
     * @param Builder $query
     * @param string $song
     * @param string $start
     * @param string $end
     * @return Builder
     */
    public function scopeFindLikesForSongInCompetition($query, $song, $start, $end)
    {
        return $query->where(self::FIELD_SONG_CODE, $song)
            ->where(self::FIELD_CREATED_AT, '>=', $start)
            ->where(self::FIELD_CREATED_AT, '<=', $end);
    }
}
