<?php

namespace App\Repository\Modules\Competitions;

use App\PlaylistsSong;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ConsolePlaylistsSongs
 * @package App\Repository\Modules\Competitions
 * @method static Builder findAddedToPlayListSongsInCompetition ($song, $start, $end)
 * @see ConsolePlaylistsSongs::scopeFindAddedToPlayListSongsInCompetition
 */
class ConsolePlaylistsSongs extends PlaylistsSong
{
    /**
     * @param Builder $query
     * @param string $song
     * @param string $start
     * @param string $end
     * @return Builder
     */
    public function scopeFindAddedToPlayListSongsInCompetition($query, $song, $start, $end)
    {
        return $query->where(self::FIELD_SONG_CODE, $song)
            ->where(self::FIELD_CREATED_AT, '>=', $start)
            ->where(self::FIELD_CREATED_AT, '<=', $end);
    }
}
