<?php

namespace App\Repository\Modules\Competitions;

use App\Competition;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

/**
 * Class ApiCompetitionsCollection
 * @package App\Repository\Modules\Competitions
 * @method static ApiCompetitionsCollection archivedCompetitionsByType($type)
 * @see ApiCompetitionsCollection::scopeArchivedCompetitionsByType
 * @method static ApiCompetitionsCollection nextCompetition($date)
 * @see ApiCompetitionsCollection::scopeNextCompetition
 * @method static ApiCompetitionsCollection previousCompetition($date)
 * @see ApiCompetitionsCollection::scopePreviousCompetition
 * @method static ApiCompetitionsCollection competitionsByTypeAndStatus ($type, $status)
 * @see ApiCompetitionsCollection::scopeCompetitionsByTypeAndStatus
 */
class ApiCompetitionsCollection extends Competition
{
    /**
     * @param Builder $query
     * @param string $type
     * @param integer $status
     * @return Builder
     */
    public function scopeCompetitionsByTypeAndStatus($query, $type, $status)
    {
        if ($status === 0) {
            return $this->scopePlannedCompetitionsByType($query, $type);
        } else {
            return $this->scopeArchivedCompetitionsByType($query, $type);
        }
    }

    /**
     * @param Builder $query
     * @param string $type
     * @return Builder
     */
    public function scopePlannedCompetitionsByType($query, $type)
    {
        return $query->where('type', $type)
            ->where('status', '0')
            ->orderBy('date_start', 'asc');
    }

    /**
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    public function scopeArchivedCompetitionsByType($query, $type)
    {
        return $query->where('type', $type)
            ->where('status', '2')
            ->orderBy('date_end', 'desc');
    }

    /**
     * @param Builder $query
     * @param Carbon  $date
     * @return Builder
     */
    public function scopeNextCompetition($query, Carbon $date)
    {
        return $query->where('date_start', '>', $date)->orderBy('date_start');
    }

    /**
     * @param Builder $query
     * @param Carbon  $date
     * @return Builder
     */
    public function scopePreviousCompetition($query, Carbon $date)
    {
        return $query->where('date_end', '<', $date)->orderBy('date_end', 'desc');
    }
}
