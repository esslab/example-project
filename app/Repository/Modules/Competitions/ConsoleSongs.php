<?php
namespace App\Repository\Modules\Competitions;

use App\Song;
use Illuminate\Database\Eloquent\Builder;
use App\Competition;

/**
 * Class ConsoleSongs
 * @property string code
 * @property string artist_code
 * @property string title
 * @property string created_at
 * @property string updated_at
 * @property integer liked
 * @property integer voted
 * @property integer played
 * @method static Builder findSongsForTop50Competition ($startDate, $endDate)
 * @package App\Repository\Modules\Competitions
 */
class ConsoleSongs extends Song
{
    /**
     * @param $query
     * @return mixed
     */
    public function scopeFindSongsForTop50Competition($query)
    {
        $query->join('songs_stats', 'songs.code', '=', 'songs_stats.song_code')
            ->orderBy('songs_stats.liked', 'desc')
            ->orderBy('songs_stats.played', 'desc')
            ->select(
                'songs.*',
                'songs_stats.liked as liked',
                'songs_stats.played as played'
            )
            ->where('songs.can_listen', 'all')
            ->limit(50);

        return $query;
    }
}
