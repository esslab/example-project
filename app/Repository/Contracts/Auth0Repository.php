<?php
namespace App\Repository\Contracts;

use OAuth2\Client;
use Auth0\SDK\API\Authentication;

class Auth0Repository
{
    const METHOD_GET = Client::HTTP_METHOD_GET;
    const METHOD_POST = Client::HTTP_METHOD_POST;
    const METHOD_PUT = Client::HTTP_METHOD_PUT;
    const METHOD_DELETE = Client::HTTP_METHOD_DELETE;

    const FORM_TYPE_APPLICATION = Client::HTTP_FORM_CONTENT_TYPE_APPLICATION;
    const FORM_TYPE_MULTIPART = Client::HTTP_FORM_CONTENT_TYPE_MULTIPART;

    protected $authAPI;
    protected $domain;
    protected $token;

    public function __construct($clientId = null, $clientSecret = null, $domain = null, $authHeader = null)
    {
        if (!$clientId) {
            $clientId = config('laravel-auth0.client_id');
        }

        if (!$clientSecret) {
            $clientSecret = config('laravel-auth0.client_secret');
        }

        $this->setAuthAPI((string)$clientId, (string)$clientSecret);

        if (!$domain) {
            $domain = config('laravel-auth0.domain');
        }

        $this->setDomain((string)$domain);

        if (!$authHeader) {
            $authHeader = request()->header('Authorization');
        }

        $this->setToken((string)$authHeader);
    }

    public function setAuthAPI(string $clientId, string $clientSecret)
    {
        $this->authAPI = new Client($clientId, $clientSecret);
    }

    /**
     * @return Client
     */
    public function getAuthAPI()
    {
        return $this->authAPI;
    }

    public function setDomain(string $domain)
    {
        $this->domain = $domain;
    }

    public function getDomain()
    {
        return $this->domain;
    }

    public function setToken(string $authHeader)
    {
        $this->token = trim(str_replace('Bearer ', '', $authHeader));
    }

    public function getToken()
    {
        return $this->token;
    }

    public function getManagementApiAccessToken()
    {
        $authDomain = $this->getDomain();

        // Getting access token to Management API
        $authentication = new Authentication(
            $authDomain
        );
        $data = $authentication->oauth_token(
            config("laravel-auth0.clients.non_interactive.client_id"),
            config("laravel-auth0.clients.non_interactive.client_secret"),
            'client_credentials',
            null,
            config("laravel-auth0.clients.non_interactive.audience")
        );

        return $data['access_token'];
    }
}
