<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CompetitionsList extends Model
{
    protected $table = self::TABLE_NAME;

    protected $primaryKey = null;
    
    protected $fillable = [
        'competition_code',
        'song_code',
        'played',
        'voted'
    ];

    public $incrementing = false;

    const TABLE_NAME = 'competitions_list';

    const FIELD_COMPETITION_CODE = 'competition_code';
    const FIELD_SONG_CODE = 'song_code';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';

    /**
     * @param $query
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    public function scopeFindSongsForWeeklyCompetition($query, $startDate, $endDate)
    {
        $query->join('competitions', 'competitions.code', '=', 'competitions_list.competition_code')
            ->where(Competition::FIELD_STATUS, '!=', Competition::STATUS_STARTED)
            ->where(Competition::FIELD_TYPE, '=', Competition::TYPE_TOP50)
            ->where(Competition::FIELD_DATE_START, '>=', $startDate)
            ->orderBy('competitions_list.liked', 'desc')
            ->orderBy('competitions_list.played', 'desc')
            ->limit(6)
            ->where(Competition::FIELD_DATE_END, '<=', $endDate);
        return $query;
    }

    /**
     * @param $query
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    public function scopeGetCompetitionSongs($query, $type, $startDate, $endDate)
    {
        $startDate = Carbon::parse($startDate)->toDateTimeString();
        $endDate = Carbon::parse($endDate)->toDateTimeString();
        $query->join('competitions', 'competitions.code', '=', 'competitions_list.competition_code')
            ->join('songs', 'songs.code', '=', 'competitions_list.song_code')
            ->join('songs_genres', 'songs.code', '=', 'songs_genres.song_code')
            ->join('genres', 'genres.code', '=', 'songs_genres.genre_code')
            ->join('users', 'users.code', '=', 'songs.user_code')
            ->whereIn('competitions.' . Competition::FIELD_STATUS, [
                Competition::STATUS_FINISHED,
                Competition::STATUS_STARTED
            ])
            ->where(Competition::FIELD_TYPE, '=', $type)
            ->where(Competition::FIELD_DATE_START, '>=', $startDate)
            ->where(Competition::FIELD_DATE_END, '<=', $endDate)
            ->orderBy('competitions_list.position')
            ->orderBy('competitions_list.voted', 'desc')
            ->orderBy('competitions_list.liked', 'desc')
            ->orderBy('competitions_list.played', 'desc')
            ->orderBy('songs.created_at')
            ->select(
                'songs.*',
                'users.name as user_name',
                'competitions.title as competition_title',
                'competitions_list.*',
                'genres.title as genres'
            )
            ->distinct();

        return $query;
    }
}
