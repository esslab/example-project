<?php

namespace App;

class Artist extends User
{
    protected $appends = [];

    protected $visible = ['code', 'name', 'plan_cost', 'avatar'];
}
