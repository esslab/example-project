<?php

namespace App;

use App\Notifications\SubscriptionPriceChanged;
use App\Services\ImageService;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

/**
 * Class User
 * @package App
 * @method static User searchByName($request)
 * @see User::scopeSearchByName
 * @method static User artists()
 * @see User::scopeArtists
 */
class User extends Authenticatable
{
    use Notifiable, Billable;

    protected $with = ['activeSubscriptions', 'roles'];

    protected $table = "users";

    protected $primaryKey = "code";

    public $incrementing = true;

    protected $fillable = ['bio', 'auth0_id', 'email', 'name', 'location', 'avatar', 'provider'];

    protected $appends = ['is_subscribed', 'available_actions', 'multichannel'];

    public $hidden = [
        'card_brand',
        'card_last_four',
        'created_at',
        'stripe_id',
        'trial_ends_at',
        'updated_at',
        'activeSubscriptions',
        'roles'
    ];

    public function playlists()
    {
        return $this->hasMany('App\Playlist', 'user_code', 'code');
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'user_id')->orderBy('created_at', 'desc');
    }

    public function activeSubscriptions()
    {
        return $this->hasMany(Subscription::class, 'user_id')->orderBy('created_at', 'desc')
            ->activeSubscriptions($this->code);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function followedArtists()
    {
        return $this->belongsToMany(User::class, 'artists_follows', 'user_code', 'artist_code')
            ->withTimestamps();
    }

    /**
     * Check whether User following artist
     *
     * @param int $artist
     * @return boolean
     */
    public function isFollowingArtist($artist)
    {
        return $this->followedArtists()->where('artist_code', $artist)->exists();
    }

    public function likes()
    {
        return $this->hasMany(Like::class, 'user_code', 'code');
    }

    public function player()
    {
        return $this->hasOne(Player::class, 'user_code', 'code');
    }

    public function languages()
    {
        return $this->belongsToMany('App\Language', 'languages_users', 'user_code', 'lang_code')
            ->withTimestamps();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_code', 'role_code');
    }

    public function isAdmin()
    {
        return $this->roles->where('title', 'Admin')->isNotEmpty();
    }

    public function isDeployer()
    {
        return $this->roles->where('title', 'Frontend deployer')->isNotEmpty();
    }

    public function getIsArtistAttribute($value)
    {
        return (bool)$value;
    }

    public function gallery()
    {
        return $this->hasOne(Gallery::class, 'user_code', 'code');
    }

    public function getAvailableActionsAttribute()
    {
        return [
            'subscribe' => $this->can('subscribe', Auth::user()),
            'unsubscribe' => $this->can('unsubscribe', Auth::user()),
            'follow' => $this->can('follow', Auth::user()),
            'unfollow' => $this->can('unfollow', Auth::user()),
        ];
    }

    public function songs()
    {
        return $this->hasMany('App\Song', 'user_code', 'code');
    }

    public function genres()
    {
        return $this->belongsToMany('App\Genre', 'artists_genres', 'user_code', 'genre_code')
            ->withTimestamps();
    }

    public function stat()
    {
        return $this->hasOne('App\ArtistStat', 'user_code', 'code');
    }

    public function payoutDetails()
    {
        return $this->hasOne('App\PayoutDetails', 'user_code', 'code');
    }

    public function followedByUsers()
    {
        return $this->belongsToMany('App\User', 'artists_follows', 'artist_code', 'user_code')
            ->withTimestamps();
    }

    public function scopeSubscribedByUser($query, $user)
    {
        return $query->rightJoin('subscriptions', 'users.code', '=', 'subscriptions.stripe_plan')
            ->where('subscriptions.user_id', $user)
            ->where(function ($query) {
                $query->where('ends_at', '>', Carbon::now())
                    ->orWhere('ends_at', null)
                    ->orWhere('resubscribe', true);
            })->select('users.*', 'subscriptions.ends_at', 'subscriptions.price', 'subscriptions.resubscribe');
    }

    /**
     * @param $query
     * @param $search
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeSearchByName($query, $search)
    {
        return $query->select(DB::raw('users.*, MATCH(users.name) AGAINST(? IN BOOLEAN MODE) as relev'))
            ->whereRaw("MATCH(users.name) AGAINST(? IN BOOLEAN MODE)", [$search, $search])
            ->orderBy('relev', 'desc');
    }

    public function scopeGetSubscribers($query, $user)
    {
        return $query->whereHas('subscriptions', function ($query) use ($user) {
            $query->where('stripe_plan', $user)
                ->where(function ($query) {
                    $query->where('ends_at', '>', Carbon::now())
                        ->orWhere('ends_at', null);
                });
        });
    }

    /**
     * Returns non specific unread notifications
     * @return mixed
     */
    public function scopeGetUnreadNotifications()
    {
        $notification = Auth::user()->unreadNotifications()
            ->where('type', '!=', SubscriptionPriceChanged::class)
            ->get();
        $notification = $notification->mapWithKeys(function ($value) {
            $data = [
                'data' => $value->data
                ];
            $type = explode('\\', $value->type);
            $result[end($type)] = $data;
            return $result;
        });
        return $notification;
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeArtists($query)
    {
        return $query->where('is_artist', 1);
    }

    /**
     * @return bool
     */
    public function getIsSubscribedAttribute()
    {
        $subscription = $this->activeSubscriptions->first();
        return $subscription && $subscription->user_id == Auth::id();
    }

    public function getAvatarAttribute($value)
    {
        return ImageService::getAvatarThumbDimensions($value);
    }

    public function setAvatarAttribute($value)
    {
        $convertedAvatarName = str_replace(config('app.app_bucket_url'), config('app.app_files_url'), $value);
        $this->attributes['avatar'] = $convertedAvatarName;
    }

    /**
     * @param Builder $query
     * @param string $email
     * @return Builder
     */
    public function scopeByEmail($query, $email)
    {
        return $query->where('email', $email);
    }
    /**
     * @param Builder $query
     * @param array $emails
     * @return Builder
     */
    public function scopeByEmails($query, array $emails)
    {
        return $query->whereIn('email', $emails);
    }

    public function getMultichannelAttribute($value)
    {
        return $this->isAdmin() || $this->isDeployer();
    }
}
