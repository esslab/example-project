<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $primaryKey = 'code';

    public $incrementing = false;

    protected $hidden = ['created_at', 'updated_at', 'pivot'];

    public function songs()
    {
        return $this->belongsToMany('App\Song', 'songs_genres', 'genre_code', 'song_code')
            ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'artists_genres', 'genre_code', 'user_code')
            ->withTimestamps();
    }
}
