<?php
namespace App\Providers;

use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Playlist' => 'App\Policies\PlaylistPolicy',
        'App\Repository\Modules\Profiles\ApiArtists' => 'App\Policies\UserPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Song' => 'App\Policies\SongPolicy',
        'App\GalleryItem' => 'App\Policies\GalleryItemPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
