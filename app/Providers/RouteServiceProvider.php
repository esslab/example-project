<?php

namespace App\Providers;

use App\GalleryItem;
use App\Song;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    private $uuidPattern = '^[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12}$';

    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::pattern('user', '^[0-9]{8}$');
        Route::pattern('subscription', '^[0-9]{8}$');
        // Pattern for uuid code
        Route::pattern('uuid', $this->uuidPattern);
        Route::pattern('competition', $this->uuidPattern);
        Route::pattern('song', $this->uuidPattern);
        Route::pattern('playlist', $this->uuidPattern);
        Route::pattern('type', '^(Top50|Weekly|Bimonthly|Yearly|top50|weekly|bimonthly|yearly)$');

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}
