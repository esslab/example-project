<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use App\Classes\Facades\Logger;
use Queue;
use Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::failing(function ($event) {
            Logger::reportToSlack(
                'Job ' . $event->job->payload()['data']['commandName'] .' was failed',
                'Failed job'
            );
        });

        Validator::extend('uuid', function ($attribute, $value, $parameters, $validator) {
            return 1 === preg_match('/^[a-f0-9]{8}(-[a-f0-9]{4}){3}-[a-f0-9]{12}$/', $value);
        });

        Validator::extend('user_code', function ($attribute, $value, $parameters, $validator) {
            return 1 === preg_match('/^[a-f0-9]{1,24}$/', $value);
        });

        Validator::extend('token', function ($attribute, $value, $parameters, $validator) {
            return $value == Config::get('aws.lambda_token');
        });

        Validator::extend(
            'not_of_type_closed_top50',
            'App\Validators\CompetitionValidator@notClosedTop50',
            'Top50 closed competition can\'t be modified.'
        );

        Validator::extend(
            'planned_weekly_competition',
            'App\Validators\CompetitionValidator@plannedWeeklyCompetition',
            'Provided competition not of type Weekly and status planned(status = 0).'
        );

        Validator::extend(
            'not_completely_filled_with_songs',
            'App\Validators\CompetitionValidator@checkSongsCount',
            'Competition can\'t take more then 6 songs.'
        );

        Validator::extend(
            'unique_song_or_artist_in_competition',
            'App\Validators\CompetitionValidator@isSongOrArtistInCompetition',
            'Song or Artist already participate in competition.'
        );

        Validator::extend('not_update_status_closed', function ($attribute, $value, $parameters, $validator) {
            $competitionStatus = (int)$parameters[0];
            $competitionClosedStatus = 2;
            if (1 === (int)$value) {
                return $competitionStatus !== $competitionClosedStatus;
            } else {
                return true;
            }
        }, "Closed competition can't be opened.");

        Validator::extend('subscription_price_values', function ($attribute, $value, $parameters, $validator) {
            return ($value == 0 || ($value >= 50 && $value <= 99999));
        }, 'Invalid price of subscription.');

        // dumps SQL queries in Local environment
        if (App::isLocal()) {
            \DB::connection()->enableQueryLog();
            \Event::listen('kernel.handled', function ($request, $response) {
                if ($request->has('sql-debug')) {
                    $queries = \DB::getQueryLog();
                    dd($queries);
                }
            });
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \Auth0\Login\Contract\Auth0UserRepository::class,
            \App\Repository\UserRepository::class
        );

        $this->app->bind('Logger', function () {
            return new \App\Classes\Tools\Logger();
        });
    }
}
