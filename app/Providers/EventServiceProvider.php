<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Laravel\Cashier\Subscription;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\PlaylistWasAddedToPlaylist' => [
            'App\Listeners\UpdatePlaylistSongsStats',
        ],
        'App\Events\UserWasDeleted' => [
            'App\Listeners\DeleteAuth0User',
        ],
        'App\Events\UserCreated' => [
            'App\Listeners\CreatePlan',
        ],
        'App\Events\SongAddedToPlaylist' => [
            'App\Listeners\UpdateSongStat',
            'App\Listeners\CreateSongAddedToPlaylistRecord',
        ],
        'App\Events\SongDeleted' => [
            'App\Listeners\RemoveSongFromPlaylists',
            'App\Listeners\RemoveSongFromPlayers',
        ],
    ];

    /**
     * The subscriber classes to register.
     *
     * @var array
     */
    protected $subscribe = [
        'App\Listeners\ArtistEventSubscriber',
        'App\Listeners\SongEventSubscriber'
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Subscription::creating(function ($model) {
            $faker = \Faker\Factory::create();
            if (property_exists($model, 'code')) {
                $model->code = $faker->uuid;
            }
        });
    }
}
