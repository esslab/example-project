<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersSubscription extends Model
{
    protected $primaryKey = "user_code, subscription_code";

    public $incrementing = false;

    protected $fillable = [];
}
