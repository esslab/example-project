<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionPriceChanged extends Notification
{
    use Queueable;

    private $artist;
    private $user;
    private $message;

    /**
     * SubscriptionPriceChanged constructor.
     * @param $user
     * @param $artist
     */
    public function __construct($user, $artist)
    {
        $this->user = $user;
        $this->artist = $artist;
        $this->message = 'We are forced to inform you, that subscription price for artist ' . $this->artist->name .
            ' was changed. Please resubscribe to continue listen premium content. ' .
            'You are still on your "grace period" until the subscription fully expires.';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url('https://songline.fm/profile/' . $this->artist->code . '/about');

        return (new MailMessage)
            ->greeting('Hi there!')
            ->line($this->message)
            ->action('Resubscribe now', $url)
            ->line('Thank you for using songline.fm');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'new_price' => $this->artist->plan_cost,
            'artist_name' => $this->artist->name,
            'artist_code' => $this->artist->code,
        ];
    }
}
