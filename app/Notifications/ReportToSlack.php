<?php

namespace App\Notifications;

use App;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Log;

class ReportToSlack extends Notification
{
    use Queueable;

    public $message;
    public $title;
    /**
     * @var
     */
    private $fields;

    /**
     * ReportToSlack constructor.
     *
     * @param       $message
     * @param null  $title
     * @param array $fields
     */
    public function __construct($message, $title = null, $fields = [])
    {
        $this->message = $message;
        $this->title = $title;
        $this->fields = $fields;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->error()
            ->content($this->message)
            ->attachment(function ($attachment) {
                $attachment->title($this->title)->fields($this->setEnvToFields());
            });
    }

    private function setEnvToFields()
    {
        return array_add($this->fields, 'Environment', App::environment());
    }
}
