<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionPriceChangedToFree extends Notification
{
    use Queueable;

    private $artist;
    private $user;

    /**
     * SubscriptionPriceChanged constructor.
     * @param $user
     * @param $artist
     */
    public function __construct($user, $artist)
    {
        $this->user = $user;
        $this->artist = $artist;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hi there!')
            ->line('Woohoo! Artist ' . $this->artist->name .
                ' made subscription as free. Now you won\'t be charged anymore.')
            ->line('Thank you for using songline.fm');
    }
}
