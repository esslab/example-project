<?php
namespace App\Services;

use App;
use App\Events\SongWasLiked;
use App\Events\SongWasUnliked;
use App\Events\SongWasUnvoted;
use App\Events\SongWasVoted;
use App\Exceptions\ExtractSongUuidException;
use App\Exceptions\LikeCreateException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\ModuleLike;
use App\ModuleVote;
use App\Song;
use App\User;
use Auth;
use DB;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Logger;

class SongService
{
    const ERROR_DUPLICATE_ENTRY = 1062;

    /**
     * @param int $songsCount
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function newSongs($songsCount = 10)
    {
        return Song::with(['user', 'stat', 'genres', 'languages'])
            ->notPremium()
            ->latest()
            ->limit($songsCount)
            ->get();
    }

    /**
     * @param Song $song
     * @param $statType
     */
    public function incrementSongStatInCompetition(Song $song, $statType)
    {
        $competitions = $song->activeCompetitions()->get();

        foreach ($competitions as $competition) {
            if (($competition->type === 'Top50' && ($statType === 'liked' || $statType === 'played')) ||
                ($competition->type !== 'Top50' && $statType !== 'liked')
            ) {
                $competition->songs()->updateExistingPivot(
                    $song->code,
                    [
                        $statType => ++$competition->pivot->$statType
                    ]
                );
            }
        }
    }


    /**
     * @param Song $song
     * @param $statType
     */
    public function decrementSongStatInCompetition(Song $song, $statType)
    {
        $competitions = $song->activeCompetitions()->get();

        foreach ($competitions as $competition) {
            if (($competition->type === 'Top50' && $statType === 'liked') ||
                ($competition->type !== 'Top50' && $statType !== 'liked')
            ) {
                if ($competition->pivot->$statType === 0) {
                    continue;
                }
                $competition->songs()->updateExistingPivot(
                    $song->code,
                    [
                        $statType => --$competition->pivot->$statType
                    ]
                );
            }
        }
    }

    /**
     * @param $song
     * @return bool
     * @throws Exception|LikeCreateException|QueryException
     */
    public function likeSong($song)
    {
        DB::beginTransaction();
        try {
            // Add users like
            ModuleLike::firstOrCreate([
                'song_code' => $song->code,
                'user_code' => Auth::id()
            ]);

            event(new SongWasLiked($song));

            DB::commit();
            return true;
        } catch (QueryException $dbException) {
            DB::rollBack();
            // if for some reason first request for create Like was not handled before the second request was come
            // from the same User we will abort the second request
            if ($dbException->errorInfo[1] === self::ERROR_DUPLICATE_ENTRY) {
                throw new LikeCreateException('Looks like you are trying to like the song twice.', 400, $dbException);
            } else {
                throw $dbException;
            }
        } catch (Exception $error) {
            DB::rollBack();
            throw $error;
        }
    }

    /**
     * @param $song
     * @return bool
     * @throws Exception
     */
    public function unlikeSong($song)
    {
        DB::beginTransaction();
        try {
            // Removes users like
            ModuleLike::findByUserAndSong(Auth::id(), $song->code)->delete();

            event(new SongWasUnliked($song));

            DB::commit();
            return true;
        } catch (Exception $error) {
            DB::rollBack();
            throw $error;
        }
    }

    /**
     * @param Song    $song
     * @param Request $request
     * @throws \Throwable
     */
    public function updateSong(Song $song, Request $request)
    {
        DB::transaction(function () use ($song, $request) {
            $song->update($request->input());

            if ($request->has('genres')) {
                $song->genres()->sync($request->genres);
            }

            if ($request->has('langs')) {
                $song->languages()->sync($request->langs);
            }
        });
    }

    /**
     * @param Song $song
     * @param User $user
     * @return bool
     * @throws Exception
     */
    public function voteSong(Song $song, User $user)
    {
        DB::beginTransaction();
        try {
            $competition = $song->competitions()->activeSick6()->first();

            // Add vote record
            $vote = new ModuleVote();
            $vote->competition_code = $competition->code;
            $vote->song_code = $song->code;
            $vote->user_code = $user->code;
            $vote->save();

            event(new SongWasVoted($song));

            $this->addLikeIfVoted($song, $user);

            DB::commit();
            return true;
        } catch (QueryException $dbException) {
            DB::rollBack();
            // if for some reason first request for create Like was not handled before the second request was come
            // from the same User we will abort the second request
            if ($dbException->errorInfo[1] === self::ERROR_DUPLICATE_ENTRY) {
                throw new LikeCreateException('Looks like you are trying to vote the song twice.', 400, $dbException);
            } else {
                throw $dbException;
            }
        } catch (Exception $error) {
            DB::rollBack();
            throw $error;
        }
    }

    /**
     * @param Song $song
     * @param User $user
     * @return bool
     * @throws Exception
     */
    public function unvoteSong(Song $song, User $user)
    {
        DB::beginTransaction();
        try {
            $competition = $song->competitions()->activeSick6()->first();

            ModuleVote::findByCompetitionAndSongAndUser($competition->code, $song->code, $user->code)
                ->delete();

            event(new SongWasUnvoted($song));

            $this->removeLikeIfUnvoted($song, $user);

            DB::commit();
            return true;
        } catch (Exception $error) {
            DB::rollBack();
            throw $error;
        }
    }

    /**
     * @param Song $song
     * @param $user
     */
    private function addLikeIfVoted(Song $song, User $user)
    {
        if ($user->can('like', $song)) {
            // Add users like
            $like = new ModuleLike();
            $like->song_code = $song->code;
            $like->user_code = $user->code;
            $like->save();

            // Update liked counter
            $song->stat()->increment('liked');
        }
    }

    /**
     * @param Song $song
     * @param $user
     */
    private function removeLikeIfUnvoted(Song $song, User $user)
    {
        if ($user->can('dislike', $song)) {
            ModuleLike::byUserToSong($user, $song)->delete();

            // Update liked counter
            $this->decrementSongStat($song, 'liked');
        }
    }

    /**
     * @param Song   $song
     * @param string $stat
     */
    public function decrementSongStat(Song $song, string $stat)
    {
        if (empty($song->stat)) {
            $message = "Song [{$song->code}] has no Stats.";
            Logger::logMessage($message, ['sentry', 'file'], [], 'warning');
            return;
        }

        $songStat = $song->stat;
        if (empty($songStat->$stat)) {
            $message = "Decrementing song_stat {$stat} field when it is empty or 0.";
            Logger::logMessage($message);
            return;
        }
        --$songStat->$stat;
        $songStat->save();
    }

    /**
     * @param string $file
     * @return string
     * @throws ExtractSongUuidException
     */
    public static function getUuidFromFileName(string $file)
    {
        $fileInfo = pathinfo($file);

        if (!isset($fileInfo['filename'])) {
            throw new ExtractSongUuidException("Can't extract UUID from file name [$file].");
        }

        return $fileInfo['filename'];
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function getSongs(Request $request)
    {
        $query = Song::with(['user', 'stat', 'genres', 'languages']);

        if ($request->has('from')) {
            $query->whereDate('songs.updated_at', '>=', $request->get('from'));
        }

        if ($request->has('to')) {
            $query->whereDate('songs.updated_at', '<=', $request->get('to'));
        }

        if ($request->has('artist')) {
            $query->whereUserCode($request->get('artist'));
        }

        if ($request->has('genres')) {
            $query = $this->filterSongsByGenres($query, $request->get('genres'));
        }

        if ($request->has('languages')) {
            $query = $this->filterSongsByLanguages($query, $request->get('languages'));
        }

        // Filter by IDs
        if ($request->has('ids')) {
            $query->whereIn('songs.code', $request->input('ids'));
        }

        $sorting = collect($request->input('sort', ['-created_at', '+title']));

        $query = $this->sortSongs($query, $sorting);

        $query->offset($request->input('offset', 0));
        $query->limit($request->input('limit', 10));

        return $query->get();
    }

    /**
     * @param Builder $query
     * @param array   $genresIds
     * @return Builder
     */
    public function filterSongsByGenres(Builder $query, $genresIds)
    {
        $query->whereHas('genres', function (Builder $query) use ($genresIds) {
            $query->whereIn('genre_code', $genresIds);
        });

        return $query;
    }

    /**
     * @param Builder $query
     * @param array   $langsIds
     * @return Builder
     */
    public function filterSongsByLanguages(Builder $query, $langsIds)
    {
        $query->whereHas('languages', function (Builder $query) use ($langsIds) {
            $query->whereIn('lang_code', $langsIds);
        });

        return $query;
    }

    /**
     * @param Builder    $query
     * @param Collection $sorting
     * @return Builder
     */
    public function sortSongs(Builder $query, Collection $sorting)
    {
        if ($sorting->contains('random')) {
            // Sorting randomly
            $query->inRandomOrder();
        } else {
            $sorting->each(function ($sort) use ($query) {
                $direction = $sort[0] === '-' ? 'desc' : 'asc';

                $field = substr($sort, 1);

                if ($field === 'rating') {
                    // to sort by related model fields will need to join the table
                    $query->join('songs_stats', 'songs.code', '=', 'songs_stats.song_code')
                        ->select('songs.*');
                    //Sorting by virtual "rating" as sum of fields
                    $field = DB::raw(
                        '(songs_stats.liked + songs_stats.voted + songs_stats.played + songs_stats.added_to_playlist)'
                    );
                }
                $query->orderBy($field, $direction);
            });
        }

        return $query;
    }

    /**
     * @param array   $artistsIds
     * @param integer $offset
     * @param integer $limit
     * @return \Illuminate\Database\Eloquent\Collection|Collection|static[]
     */
    public function userFavoriteArtistsSongs(array $artistsIds, $offset, $limit)
    {
        $query = Song::with(['user.genres', 'user.languages', 'genres', 'languages'])
            ->whereIn('user_code', $artistsIds);

        $query->orderBy('created_at', 'desc');

        // Paginate
        $query->offset($offset);
        $query->limit($limit);

        return $query->get();
    }
}
