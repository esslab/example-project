<?php
namespace App\Services\Admin;

use App\Http\Requests\Request;
use App\Http\Requests\Admin\CompetitionRequest;
use App\Http\Requests\Admin\CompetitionsRequest;
use App\Competition;
use App\Classes\Database\Eloquent\CompetitionsCollection;
use App\Classes\Database\Eloquent\SongsCollection;
use App\Classes\Traits\Services\CompetitionTrait;
use Illuminate\Database\Eloquent\Model;
use App\Events\ArtistWinCompetition;
use App\Exceptions\CompetitionUpdateException;

class CompetitionService
{
    use CompetitionTrait;
    /**
     * @var Request|null
     */
    private $request;

    /**
     * @return Request|null
     */
    protected function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request|null $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @return array|null
     */
    public function getCompetition()
    {
        /**
         * @var CompetitionRequest $request
         * @var Competition|null $competition
         */
        $request = $this->getRequest();

        $competition = Competition::with(['songs' => function ($query) {
            // Because we have a pivot, there i can order by its values
            $query->orderBy('competitions_list.position', 'asc')
                ->orderBy('competitions_list.voted', 'desc')
                ->orderBy('competitions_list.liked', 'desc')
                ->orderBy('competitions_list.played', 'desc')
                ->orderBy('songs.created_at');
        }, 'songs.genres', 'songs.stat', 'songs.user'])
        ->find($request->competition);

        if (!$competition) {
            return null;
        }

        $competitionResult = $competition->toArray();

        if (!$competition->songs->isEmpty()) {
            /**
             * @var SongsCollection $songs
             */
            $songs = $competition->songs;
            $competitionResult['songs'] = $songs->mapToAdminCompetitionSongs()->toArray();
        }

        // Get competition next and previous
        $competitionResult['previous'] = null;
        $competitionResult['next'] = null;

        $previous = $this->getPreviousCompetition($competition);
        if ($previous) {
            $competitionResult['previous'] = $previous;
        }

        $next = $this->getNextCompetition($competition);
        if ($next) {
            $competitionResult['next'] = $next;
        }

        return $competitionResult;
    }

    /**
     * @param Competition $currentCompetition
     * @return Competition|null
     */
    protected function getNextCompetition($currentCompetition)
    {
        return Competition::where('date_start', '>', $currentCompetition->date_end)
            ->where('type', '=', $currentCompetition->type)
            ->orderBy('date_start', 'asc')
            ->first();
    }

    /**
     * @param Competition $currentCompetition
     * @return Competition|null
     */
    protected function getPreviousCompetition($currentCompetition)
    {
        return Competition::where('date_end', '<', $currentCompetition->date_start)
            ->where('type', '=', $currentCompetition->type)
            ->orderBy('date_start', 'desc')
            ->first();
    }

    /**
     * @return array
     */
    public function getCompetitionsListByType()
    {
        /**
         * @var CompetitionsRequest $request
         * @var CompetitionsCollection $competitions
         */
        $request = $this->getRequest();
        $competitions = Competition::byTypeAndOrderedByDate($request->type, 'desc')->get();

        if (!$competitions->isEmpty()) {
            $competitions = $competitions->mapToSimpleCodeNameList();
        }

        return $competitions->toArray();
    }

    /**
     * @param string $type
     * @return Model|null
     */
    public function getCompetitionByType($type)
    {
        $type = studly_case($type);
        /**
         * @var Competition $competition
         */
        $competition = $this->selectDetailed(Competition::where('type', '=', $type))
            ->where([
                ['status', '<>', Competition::STATUS_PLANNED],
                ['date_end', '<=', $this->endOfWeek()]
            ])
            ->orderBy('date_end', 'desc')
            ->first();

        if (!$competition) {
            return null;
        }

        $competitionResult = $competition->toArray();

        if ($competition->songs->isNotEmpty()) {
            /**
             * @var SongsCollection $songs
             */
            $songs = $competition->songs;
            $competitionResult['songs'] = $songs->mapToAdminCompetitionSongs()->toArray();
        }

        // Get competition next and previous
        $competitionResult['previous'] = null;
        $competitionResult['next'] = null;

        $previous = $this->getPreviousCompetition($competition);
        if ($previous) {
            $competitionResult['previous'] = $previous;
        }

        $next = $this->getNextCompetition($competition);
        if ($next) {
            $competitionResult['next'] = $next;
        }

        return $competitionResult;
    }

    /**
     * @param Competition $competition
     * @param             $status
     * @return bool
     * @throws CompetitionUpdateException
     */
    public function updateStatus(Competition $competition, $status)
    {
        if ($competition->status === $status) {
            return true;
        }
        if ($status == Competition::STATUS_FINISHED) {
            // calculate competition winner (Song, Artist)
            $winner = $this->getCompetitionWinner($competition);
            if (!$winner) {
                $msg = "The Winner of competition is not defined. Can't update status";
                throw new CompetitionUpdateException($msg);
            }
            $competition->winner_song_code = $winner->song_code;
            $competition->winner_artist_code = $winner->user_code;
            event(new ArtistWinCompetition($competition->winner_artist_code));
        }
        $competition->status = $status;
        $competition->save();
        return true;
    }
}
