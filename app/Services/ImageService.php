<?php
namespace App\Services;

use App;

class ImageService
{
    /**
     * Default extension for converted images for Lambda function on AWS
     * @var string
     */
    private static $convertedImageExtension = '.jpg';

    /**
     * @param string $filePath
     * @return array|string
     */
    public static function getAvatarThumbDimensions(string $filePath)
    {
        if (str_contains($filePath, 'avatars/source')) {
            return self::getImagesSizes($filePath, 'avatar');
        }
        return ['original' => $filePath];
    }

    /**
     * @param string $filePath
     * @return array|string
     */
    public static function getCoverThumbDimensions(string $filePath)
    {
        if (str_contains($filePath, 'covers/source')) {
            return self::getImagesSizes($filePath, 'cover');
        }
        return $filePath;
    }

    /**
     * @param string $filePath
     * @return array|string
     */
    public static function getGalleryItemThumbDimensions(string $filePath)
    {
        if (str_contains($filePath, 'images/source')) {
            return self::getImagesSizes($filePath, 'images');
        } elseif (str_contains($filePath, 'video/source')) {
            return self::getVideoSizes($filePath);
        }
        return $filePath;
    }

    /**
     * @param string $filePath
     * @param string $type
     * @return array
     */
    private static function getImagesSizes($filePath, $type)
    {
        $imageSizes = config('image-sizes');
        $result = [];
        $fileExtension = (($pos = strrpos($filePath, '.')) === false) ? null : substr($filePath, $pos);

        $filePath = self::getRelevantFilePath($filePath);

        foreach ($imageSizes[$type] as $k => $imageSize) {
            if ($k === 'original') {
                $result[$k] = $filePath;
                continue;
            }
            if ($fileExtension) {
                $result[$k] = str_replace(
                    ['source', $fileExtension],
                    ['converted', $imageSize . self::$convertedImageExtension],
                    $filePath
                );
            } else {
                $result[$k] = self::getPathForConvertedWithoutExt($filePath, $imageSize);
            }
        }
        return $result;
    }

    /**
     * @param $filePath
     * @param $imageSize
     * @return mixed
     */
    private static function getPathForConvertedWithoutExt($filePath, $imageSize)
    {
        $convertedString = str_replace('source', 'converted', $filePath);
        return $convertedString . $imageSize . self::$convertedImageExtension;
    }

    /**
     * @param string $filePath
     * @return array
     */
    private static function getVideoSizes($filePath)
    {
        $imageSize = config('image-sizes')['video'][0];
        $result = [];
        $fileExtension = (($pos = strrpos($filePath, '.')) === false) ? null : substr($filePath, $pos);

        $filePath = self::getRelevantFilePath($filePath);

        $result['original'] = $filePath;
        $result['large'] = str_replace('source', 'converted', $filePath);
        if ($fileExtension) {
            $result['thumbs'] = str_replace(
                $fileExtension,
                $imageSize . self::$convertedImageExtension,
                $result['large']
            );
        } else {
            $result['thumbs'] = self::getPathForConvertedWithoutExt($filePath, $imageSize);
        }
        return $result;
    }

    /**
     * Return full file path
     * @param string $filePath
     * @return string
     */
    private static function getRelevantFilePath($filePath)
    {
        if (starts_with($filePath, 'http')) {
            $filePath = str_replace_first(config('app.app_files_url'), '', $filePath);
        }

        if (starts_with($filePath, '/')) {
            return $filePath;
        }
        return $filePath;
    }
}
