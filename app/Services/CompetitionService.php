<?php
namespace App\Services;

use App\Competition;
use App\Exceptions\NotFoundException;
use App\Repository\Modules\Competitions\ApiCompetitionsCollection;
use App\Repository\Modules\Competitions\ApiCompetitionSingle;
use App\Song;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use App\Classes\Traits\Services\CompetitionTrait;
use Illuminate\Http\Request as IlluminateRequest;
use App\CompetitionsList;

class CompetitionService
{
    use CompetitionTrait;
    /**
     * @param string $type
     * @return Model
     */
    public function getCurrentOrPreviousCompetitionByType($type = 'weekly')
    {
        $type = Str::studly($type);
        $date = $this->endOfWeek();
        return ApiCompetitionSingle::currentOrPrevious($date)->ofType($type)->first();
    }

    /**
     * @param string $type
     * @return Model
     * @throws ModelNotFoundException
     */
    public function getNextCompetitionByType($type = 'weekly')
    {
        $type = Str::studly($type);
        $date = $this->endOfWeek();
        return ApiCompetitionsCollection::nextCompetition($date)->ofType($type)->firstOrFail();
    }

    /**
     * @param string $type
     * @return Model
     */
    public function getCompetitionByType($type = 'weekly')
    {
        $competition = $this->getCurrentOrPreviousCompetitionByType($type);

        if (is_null($competition)) {
            $competition = $this->getNextCompetitionByType($type);

            $this->throwExceptionCurrentOrPreviousNotFound($competition);
        }

        return $competition;
    }


    /**
     * @param $competition
     * @return NotFoundException
     */
    private function throwExceptionCurrentOrPreviousNotFound($competition)
    {
        $message = sprintf(
            'The %s Top6 is due on %s, stay tuned!',
            $competition->type,
            $competition->date_start->toDateString()
        );
        throw new NotFoundException($message);
    }

    /**
     * Get a collection of archived competitions of specified type
     * @param string $type
     * @param int    $status
     * @param int    $limit
     * @param int    $offset
     * @return mixed
     */
    public function getCompetitions($type = 'weekly', $status = Competition::STATUS_FINISHED, $limit = 10, $offset = 0)
    {
        $competitions = ApiCompetitionSingle::detailed()
            ->ofType($type)
            ->withStatus($status)
            ->orderBy('date_start', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();

        return $competitions;
    }

    /**
     * Get a collection of archived competitions of specified type
     * @param string $type
     * @return Collection
     */
    public function getArchived($type = 'weekly')
    {
        $fields = ['code', 'date_start', 'date_end'];

        $competitions = Competition::ofType($type)
            ->select($fields)
            ->withStatus(Competition::STATUS_FINISHED)
            ->orderBy('date_start', 'desc')
            ->get($fields);

        return $competitions;
    }

    /**
     * Return 6 songs from last Top6 competition. If empty return top 6 songs from last Top50
     * @return array|Collection
     */
    public function lastCompetition6Songs()
    {
        $lastTop6Competition = Competition::with('songs')->recent()->top6()->finished()->first();
        if ($lastTop6Competition) {
            return $lastTop6Competition->songs()->get();
        }

        $lastTop50Competition = Competition::with('songs')->recent()->ofType('top50')->finished()->first();
        if ($lastTop50Competition) {
            return $this->getTop6SongsFromTop50($lastTop50Competition);
        }
        return [];
    }

    /**
     * Return ordered collection of Songs from Top50 competition and take 6
     * @param $lastTop50Competition
     * @return Collection
     */
    private function getTop6SongsFromTop50($lastTop50Competition)
    {
        return $lastTop50Competition->songs()
            ->orderBy('pivot_liked', 'desc')
            ->orderBy('pivot_played', 'desc')
            ->orderBy('pivot_added_to_playlist', 'desc')
            ->limit(6)
            ->get();
    }


    /**
     * Return competitions where the number of participating songs less than 6
     * @return mixed
     */
    public function getNotFullCompetitions()
    {
        $competitions = Competition::has('songs', '<', 6)
            ->has('songs', '>', 0)
            ->top6()
            ->orderBy('date_start', 'desc')
            ->get();

        return $competitions;
    }

    /**
     * Return competitions where specified song can participate
     * @param $song
     * @return \Illuminate\Support\Collection|array
     */
    public function getCompetitionsWhereCanParticipate($song)
    {
        $competitionsNotFull = $this->getNotFullCompetitions();
        if ($competitionsNotFull->isEmpty()) {
            return $competitionsNotFull;
        }

        $filteredCompetitions = $competitionsNotFull->load('songs')->filter(function ($competition) use ($song) {
            return !$this->isSongOrArtistInCompetition($song, $competition);
        });
        if ($filteredCompetitions->isEmpty()) {
            return [];
        }

        return $filteredCompetitions->makeHidden('songs')->values();
    }

    /**
     * Checks whether Song or Artist of the song already participate in the competition
     * @param Song $song
     * @param Competition $competition
     * @return bool
     */
    public function isSongOrArtistInCompetition(Song $song, Competition $competition)
    {
        return (bool)$competition->songs->search(function ($songItem, $k) use ($song) {
            return ($songItem->code === $song->code || $songItem->user_code === $song->user_code);
        });
    }

    public function competitionAddSong(Competition $competition, IlluminateRequest $request)
    {
        if ($competition->type === 'Top50') {
            $songPosition = 0;
        } else {
            /**
             * todo added here because of some broken competitions on Staging DB
             * we don't need this on normal site because we make recalculation of positions after deleting a song
             */
            $this->recalculateSongsPosition($competition);
            $songPosition = $this->competitionLastSongPosition($competition) + 1;
        }
        $request->merge(['added_by_admin' => 1, 'position' => $songPosition]);
        $competition->songs()->attach($request->song_code, $request->except('song_code', 'competition_code'));
        return true;
    }

    public function competitionRemoveSong(Competition $competition, $songCode)
    {
        $competition->songs()->detach($songCode);
        if ($competition->type !== 'Top50') {
            $this->recalculateSongsPosition($competition);
        }
        return true;
    }

    /**
     * Recalculate songs positions in competition to have the sequential order
     * @param $competition
     * @return bool
     */
    private function recalculateSongsPosition($competition)
    {
        $competitionSongs = CompetitionsList::where('competition_code', $competition->code)->orderBy('position')->get();

        $newPosition = 1;
        foreach ($competitionSongs as $competitionSong) {
            if ($competitionSong->position != $newPosition) {
                CompetitionsList::where('competition_code', $competition->code)
                    ->where('song_code', $competitionSong->song_code)
                    ->update(['position' => $newPosition]);
            }
            $newPosition++;
        }

        return true;
    }

    /**
     * Return the last song position in competition
     * @param $competition
     * @return mixed
     */
    private function competitionLastSongPosition($competition)
    {
        return CompetitionsList::where('competition_code', $competition->code)
            ->orderBy('position', 'desc')
            ->first(['position'])->position ?? 0;
    }

    /**
     * Change Song position in specified Competition to specified position
     *
     * @param $competitionCode
     * @param $songCode
     * @param $newPosition
     * @return bool
     */
    public function changeSongPosition($competitionCode, $songCode, $newPosition)
    {
        $competitionList = CompetitionsList::where('competition_code', $competitionCode)->get();

        $oldPosition = $competitionList->first(function ($competition, $index) use ($songCode) {
            return $competition->song_code === $songCode;
        })->position;

        if ($oldPosition === $newPosition) {
            abort(400, 'The song already on that position.');
        }

        // position not empty
        if ($res = $this->isCompetitionSongPositionNotEmpty($competitionList, $newPosition)) {
            $recountUpperPositions = ($newPosition > $oldPosition);
            $this->competitionOffsetPositions($competitionCode, $newPosition, $oldPosition, $recountUpperPositions);
        }
        CompetitionsList::where('competition_code', $competitionCode)
            ->where('song_code', $songCode)
            ->update([
                'position'       => $newPosition,
                'added_by_admin' => 1,
            ]);

        return true;
    }

    /**
     * Return true if song position in competition not empty
     *
     * @param Collection $competitionList
     * @param integer    $position
     * @return bool
     */
    private function isCompetitionSongPositionNotEmpty(Collection $competitionList, $position)
    {
        return $competitionList->contains(function ($item, $key) use ($position) {
            return $item->position === $position;
        });
    }

    /**
     * Offset(increment/decrement) songs positions by one from/to specified position
     *
     * @param string  $competitionCode
     * @param integer $newPosition
     * @param integer $oldPosition
     * @param boolean $recountUpperPositions
     */
    private function competitionOffsetPositions($competitionCode, $newPosition, $oldPosition, $recountUpperPositions)
    {
        $query = CompetitionsList::where('competition_code', $competitionCode);
        if ($recountUpperPositions) {
            $query->whereBetween('position', [($oldPosition + 1), $newPosition])->decrement('position');
        } else {
            $query->whereBetween('position', [$newPosition, ($oldPosition - 1)])->increment('position');
        }
    }

    /**
     * Return next competition of specified type (only date_start field)
     * @param string $type
     * @return Model|static
     */
    public function getNextByType($type = 'weekly')
    {
        $type = Str::studly($type);
        $date = $this->endOfWeek();

        return Competition::select('date_start')
            ->ofType($type)
            ->whereDate('date_start', '>', $date)
            ->orderBy('date_start')
            ->firstOrFail(['date_start']);
    }

    /**
     * @param string $type
     * @param Carbon $dateStart
     * @return string
     */
    public function getNextCompetitionMessage($type, Carbon $dateStart)
    {
        return sprintf(
            'The %s Top6 is due on %s, stay tuned!',
            $type,
            $dateStart->toDateString()
        );
    }
}
