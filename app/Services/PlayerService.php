<?php
namespace App\Services;

use App\User;
use Illuminate\Http\Request;

class PlayerService
{
    public function updatePlayerForUser(User $user, Request $request)
    {
        $playerData = $request->except('songs');
        $songs = json_decode($request['songs']);

        if (!empty($songs)) {
            $playerData['tracksOrder'] = implode(',', array_map(function ($song) {
                return "'" . $song . "'";
            }, $songs));
        }

        $user->player()->update($playerData);

        $user->player()->first()->songs()->sync($songs);

        return true;
    }

    public function getPlayerForUser(User $user)
    {
        $player = $user->player()->first();

        $query = $player->songs()->with('genres', 'stat', 'user', 'languages');

        $player->songs = $query->orderByTracksOrder($player->tracksOrder)->get();

        return $player;
    }
}
