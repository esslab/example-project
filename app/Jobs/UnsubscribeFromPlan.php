<?php

namespace App\Jobs;

use App\Notifications\SubscriptionPriceChanged;
use App\Notifications\SubscriptionPriceChangedToFree;
use App\Repository\SubscriptionsRepository;
use App\Subscription;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\ArtistUnSubscribe;

class UnsubscribeFromPlan implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    private $artist;

    /**
     * UnsubscribeFromPlan constructor.
     * @param $artist
     */
    public function __construct($artist)
    {
        $this->artist = $artist;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $subscriptions = Subscription::allActiveSubscriptionsForPlan($this->artist->code)->get();
        SubscriptionsRepository::unsubscribeUsers($subscriptions, $this->artist);
        $this->sendUnsubscribedNotifications($subscriptions);
    }

    /**
     * @param $subscriptions
     */
    private function sendUnsubscribedNotifications($subscriptions)
    {
        foreach ($subscriptions as $subscription) {
            $user = User::find($subscription->user_id);
            if ($this->artist->plan_cost == 0) {
                $user->notify(new SubscriptionPriceChangedToFree($user, $this->artist));
            } else {
                $user->notify(new SubscriptionPriceChanged($user, $this->artist));
            }
            event(new ArtistUnSubscribe($this->artist));
        }
    }
}
