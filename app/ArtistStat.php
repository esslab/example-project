<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArtistStat extends Model
{
    protected $table = 'artists_stats';

    public $primaryKey = 'user_code';

    protected $fillable = ['user_code'];

    public $incrementing = false;

    protected $hidden = ['pivot', 'created_at', 'updated_at', 'user_code'];

    //@todo With new project structure this should be transfered to different place (maybe Service or Domain model)
    /**
     * Increment added_to_playlist property for specified artists
     * @param array $artistsIds
     * @return $this
     */
    public function incrementAddedToPlaylistPropertyForArtists(array $artistsIds)
    {
        $this->whereIn('user_code', $artistsIds)->each(function ($artistStat) {
            $artistStat->increment('added_to_playlist');
        });
        return $this;
    }
}
