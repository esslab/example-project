<?php
namespace App\Classes\Tools;

use App\Classes\Traits\SentryReport as SentryReportTrait;
use App\Classes\Traits\SlackReport as SlackReportTrait;
use Log;
use Exception;

class Logger
{
    use SlackReportTrait, SentryReportTrait;

    /**
     * Logs particular exception to Sentry and Logs file
     *
     * @param Exception $exception
     */
    public function logException($exception)
    {
        $logLevel = 'error';
        $this->writeToLogFile($this->getExceptionMessage($exception), $logLevel);
    }

    /**
     * Logs particular message to Sentry or to logging file
     *
     * @param string $message
     * @param string $level
     */
    public function logMessage($message, $level = 'info')
    {
        $this->writeToLogFile($message, $level);
    }

    /**
     * @param string $level
     * @return bool
     */
    protected function canWriteToLogFile($level)
    {
        return (config('app.log_level') === 'debug' || $level === 'info' || config('app.log_level') === $level);
    }

    /**
     * @param Exception $exception
     * @return string
     */
    protected function getExceptionMessage($exception)
    {
        $exceptionString = sprintf(
            "Exception \"%s\" at file: %s, line: %d",
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine()
        );

        if (config('app.debug') == 'true' && config('app.log_level') === 'debug') {
            $trace = $exception->getTraceAsString();
            $exceptionString .= "\nStack trace:\n$trace";
        }

        return $exceptionString;
    }

    /**
     * @param string $message
     * @param string $level
     */
    protected function writeToLogFile($message, $level = 'info')
    {
        if ($this->canWriteToLogFile($level)) {
            switch ($level) {
                case 'info':
                    Log::info($message);
                    break;
                case 'error':
                    Log::error($message);
                    break;
                case 'debug':
                    Log::debug($message);
                    break;
            }
        }
    }
}
