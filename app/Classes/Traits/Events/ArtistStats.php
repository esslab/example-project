<?php
namespace App\Classes\Traits\Events;

use App\Artist;

trait ArtistStats
{
    /**
     * @param Artist $artist
     * @return boolean
     */
    protected function checkIfArtistHasRelatedStats($artist)
    {
        return $artist && ($artist instanceof Artist) && $artist->stat;
    }
}
