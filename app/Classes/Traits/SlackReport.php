<?php

namespace App\Classes\Traits;

use App;
use App\Notifications\ReportToSlack;
use Illuminate\Notifications\Notifiable;

trait SlackReport
{
    use Notifiable;

    public $slack_webhook_url;

    public function reportToSlack($message, $title = null, array $fields = [], $channel = null)
    {
        if ($this->cantReport()) {
            return;
        }
        $this->slack_webhook_url = $channel ?? config('notifications.slack_errors');
        $this->notify(new ReportToSlack($message, $title, $fields));
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return $this->slack_webhook_url;
    }

    private function cantReport()
    {
        return App::environment('local');
    }
}
