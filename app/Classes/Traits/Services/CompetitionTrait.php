<?php
namespace App\Classes\Traits\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\CompetitionsList;
use App\Competition;

trait CompetitionTrait
{

    /**
     * @param Builder $query
     * @return Builder
     */
    protected function selectDetailed($query)
    {
        /**
         * @param Builder $q
         */
        $competitionStats = function ($q) {
            $q->select(
                'song_code',
                'competition_code',
                'position',
                'liked',
                'voted',
                'played',
                'added_to_playlist'
            )
                ->orderBy('voted', 'desc')
                ->orderBy('liked', 'desc')
                ->orderBy('played', 'desc')
                ->orderBy('added_to_playlist', 'desc')
                ->orderBy('position');
        };

        $query->with([
            'songs',
            'songs.user',
            'songs.competitionsStats' => $competitionStats,
            'songs.genres',
            'songs.languages'
        ]);
        return $query;
    }

    /**
     * @param string|null $dateEnd
     * @return Carbon $instance
     */
    protected function endOfWeek($dateEnd = null)
    {
        $requestDate = ($dateEnd && !empty($dateEnd)) ? Carbon::parse($dateEnd) : Carbon::now()->endOfWeek();
        return $requestDate->isSaturday() ? $requestDate->endOfDay() : $requestDate->subDay()->endOfDay();
    }

    /**
     * @param Competition $competition
     * @return Model|null
     */
    public function getCompetitionWinner(Competition $competition)
    {
        $query = CompetitionsList::leftJoin('songs', 'songs.code', 'competitions_list.song_code')
            ->where('competitions_list.competition_code', $competition->code);
        if ($competition->type === 'Top50') {
            $query->orderBy('competitions_list.liked', 'desc');
        } else {
            $query->orderBy('competitions_list.voted', 'desc');
        }
        $query->orderBy('competitions_list.played', 'desc');
        $query->orderBy('competitions_list.added_to_playlist', 'desc');
        $query->orderBy('competitions_list.position');

        $song = $query->first();

        return $song;
    }
}
