<?php

namespace App\Classes\Traits;

trait TUuid
{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();
        $faker = \Faker\Factory::create();
        static::creating(function ($model) use ($faker) {
            $model->code = $faker->uuid;
        });
    }
}
