<?php

namespace App\Classes\Traits;

use App;
use Sentry;

trait SentryReport
{
    public function reportExceptionToSentry($exception)
    {
        if (!$this->canReportToSentry()) {
            return false;
        }
        $this->setSentryEnvironment();
        return Sentry::captureException($exception, $this->setSentryOptions());
    }

    public function reportMessageToSentry($message, $level = 'info')
    {
        if (!$this->canReportToSentry()) {
            return false;
        }
        $this->setSentryEnvironment();
        return Sentry::captureMessage($message, [], $this->setSentryOptions($level));
    }

    /**
     * @return bool
     */
    protected function canReportToSentry()
    {
        return App::environment(['development', 'production']);
    }


    /**
     * @param null|string $level
     * @return array
     */
    protected function setSentryOptions($level = null)
    {
        $options = [];

        if ($level && in_array($level, ['debug', 'info', 'warning'])) {
            $options['level'] = $level;
        }

        return $options;
    }

    protected function setSentryEnvironment()
    {
        Sentry::setEnvironment(config('app.env'));
    }
}
