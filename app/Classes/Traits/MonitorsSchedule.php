<?php

namespace App\Classes\Traits;

use Carbon\Carbon;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

trait MonitorsSchedule
{
    /**
     * Monitor each of the scheduled events.
     *
     * @param Schedule $schedule
     */
    public function monitor(Schedule $schedule)
    {
        $maxLogSize = pow(2, 16); //64kb - max size of text field

        $date = Carbon::today()->toDateString();

        $events = new Collection($schedule->events());

        $events->each(function (Event $event) use ($date, $maxLogSize) {

            $command = substr($event->command, strpos($event->command, 'artisan') + strlen('artisan') + 1);
            $filename = str_slug($command) . "-$date.log";
            $path = storage_path("logs/$filename");

            $event->sendOutputTo($path)->after(function () use ($command, $path, $maxLogSize) {
                if (file_exists($path)) {
                    if (filesize($path) < $maxLogSize) {
                        $output = file_get_contents($path);
                    } else {
                        $output = 'Log file is too long';
                    }
                    DB::table('scheduled_events')->insert([
                        'command'   => $command,
                        'output'    => $output,
                        'logged_at' => Carbon::now(),
                    ]);
                    unlink($path);
                }
            });
        });
    }
}
