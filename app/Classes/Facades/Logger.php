<?php
namespace App\Classes\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Logger
 * @package App\Classes\Facades
 * @method static void logException($exception)
 * @method static void logMessage($message, $level = 'info')
 */
class Logger extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Logger';
    }
}
