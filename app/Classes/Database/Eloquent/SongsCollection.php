<?php
namespace App\Classes\Database\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use App\Song;
use Closure;
use Illuminate\Contracts\Support\Arrayable;

class SongsCollection extends Collection
{
    /**
     * @return SongsCollection
     */
    public function mapToAdminCompetitionSongs()
    {
        return $this->map($this->mapToAdminCompetitionSongsClosure());
    }

    /**
     * @return Closure
     */
    protected function mapToAdminCompetitionSongsClosure()
    {
        /**
         * @param Song $song
         */
        return function ($song) {

            return new class ($song) implements Arrayable {
                private $song;

                public function __construct($song)
                {
                    $this->song = $song;
                }
                /**
                 * Get the instance as an array.
                 *
                 * @return array
                 */
                public function toArray()
                {
                    return [
                        'code' => $this->song->code,
                        'title' => $this->song->title,
                        'cover' => $this->song->cover,
                        'competition_code' => $this->song->pivot->competition_code,
                        'liked' => $this->song->pivot->liked,
                        'played' => $this->song->pivot->played,
                        'voted' => $this->song->pivot->voted,
                        'position' => $this->song->pivot->position,
                        'user_code' => $this->song->user->code,
                        'user_name' => $this->song->user->name,
                        'genres' => $this->genresString()
                    ];
                }

                /**
                 * Convert genres collection to genres string
                 * @return string
                 */
                protected function genresString()
                {
                    if (!isset($this->song->genres) || $this->song->genres->isEmpty()) {
                        return '';
                    } else {
                        /**
                         * @var Collection $genres
                         */
                        $genres = $this->song->genres->map(function ($genre) {
                            return $genre->title;
                        });
                        if ($genres->isEmpty()) {
                            return '';
                        }

                        return implode(', ', $genres->toArray());
                    }
                }
            };
        };
    }
}
