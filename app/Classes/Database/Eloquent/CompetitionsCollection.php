<?php
namespace App\Classes\Database\Eloquent;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Competition;
use Closure;
use Illuminate\Contracts\Support\Arrayable;

class CompetitionsCollection extends Collection
{
    /**
     * @return Collection|\Illuminate\Support\Collection
     */
    public function mapToSimpleCodeNameList()
    {
        return $this->map($this->mapToSimpleCodeNameListClosure());
    }

    /**
     * Map closure for mapToSimpleCodeNameList
     * @return Closure
     */
    protected function mapToSimpleCodeNameListClosure()
    {
        /**
         * This function maps Competition and return an anonymous class instance
         * @param Competition $competition
         * @return mixed
         */
        return function ($competition) {
            return new class (
                $competition->code,
                $competition->title,
                $competition->status,
                $competition->date_start,
                $competition->date_end
            ) implements Arrayable
            {
                /**
                 * @var string $code
                 */
                public $code;
                /**
                 * @var string $title
                 */
                public $title;
                /**
                 * @var integer $status
                 */
                public $status;
                /**
                 * @var Carbon $date_start
                 */
                public $date_start;
                /**
                 * @var Carbon $date_end
                 */
                public $date_end;

                public function __construct($code, $title, $status, $date_start, $date_end)
                {
                    $this->code = $code;
                    $this->title = $title;
                    $this->status = $status;
                    $this->date_start = $date_start;
                    $this->date_end = $date_end;
                }
                /**
                 * Get the instance as an array.
                 *
                 * @return array
                 */
                public function toArray()
                {
                    return [
                        'code'       => $this->code,
                        'title'      => $this->title,
                        'status'     => $this->status,
                        'date_start' => $this->date_start->toDateString(),
                        'date_end'   => $this->date_end->toDateString()
                    ];
                }
            };
        };
    }
}
