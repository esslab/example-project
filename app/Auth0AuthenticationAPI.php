<?php
namespace App;

use App\Repository\Contracts\Auth0Repository;
use App\Exceptions\Repository\Auth0BadAPIResponseException;
use App\Exceptions\Repository\Auth0BadAPIRequestException;
use App\Exceptions\Repository\Auth0BadAccessTokenAPIRequestException;
use Auth;
use Auth0\SDK\API\Authentication;
use Auth0\SDK\API\Management;
use Illuminate\Http\Response;

class Auth0AuthenticationAPI extends Auth0Repository
{
    public function getTokenInfo()
    {
        $url = 'https://' . $this->getDomain() . '/tokeninfo';
        $params = [
            'id_token' => $this->getToken()
        ];
        $method = self::METHOD_POST;
        $headers = [];
        $formType = self::FORM_TYPE_APPLICATION;

        $data = $this->getAuthAPI()->fetch($url, $params, $method, $headers, $formType);

        if (!is_array($data) || !isset($data['code']) || !isset($data['result'])) {
            throw new Auth0BadAPIResponseException();
        }

        if ($data['code'] != Response::HTTP_OK) {
            throw new Auth0BadAPIRequestException($url, $data['result'], (int) $data['code']);
        }

        return $data['result'];
    }

    /**
     * Return access token from Auth0 service for Frontend application
     *
     * @return array
     * @throws Auth0BadAPIRequestException
     * @throws Auth0BadAPIResponseException
     * @throws \OAuth2\Exception
     */
    public function getAccessToken($request)
    {
        $providerUserId = $request['provider_user_id'];
        $service = $request['service'];
        $authDomain = $this->getDomain();

        // Getting access token to Management API
        $authentication = new Authentication($authDomain);
        $data = $authentication->oauth_token(
            config("laravel-auth0.clients.non_interactive.client_id"),
            config("laravel-auth0.clients.non_interactive.client_secret"),
            'client_credentials',
            null,
            config("laravel-auth0.clients.non_interactive.audience")
        );

        // Getting user data
        $management = new Management($data["access_token"], $authDomain);
        try {
            $user = $management->users->get($providerUserId);
        } catch (\Exception $e) {
            throw new Auth0BadAccessTokenAPIRequestException($e);
        }

        $identity = $this->getIdentityWithRequiredService($user['identities'], $service);

        return is_null($identity) ? $identity : ['access_token' => $identity['access_token']];
    }

    /**
     * @param $request
     * @throws \OAuth2\Exception
     */
    public function basicAuthViaCredentials($request)
    {
        $params = [
            'connection' => config('auth.pass_connection'),
            'password'   => $request->password,
            'username'   => Auth::user()->email,
            'client_id'  => config('auth.client_id'),
            "grant_type" => "http://auth0.com/oauth/grant-type/password-realm",
            "realm"      => config("laravel-auth0.connection"),
            "scope"      => "openid email",
            "audience"   => config('laravel-auth0.api_identifier')
        ];
        $url = 'https://' . $this->getDomain() . '/oauth/token';
        $method = self::METHOD_POST;
        $formType = self::FORM_TYPE_APPLICATION;

        $data = $this->getAuthAPI()->fetch($url, $params, $method, [], $formType);

        if (starts_with($data['code'], ['4', '5'])) {
            if ($data['code'] === 403 &&
                str_contains($data['result']['error_description'], 'Wrong email or password.')
            ) {
                $errorMessage = 'Wrong password.';
            } else {
                $errorMessage = $data['result']['error_description'] ?? 'Some errors occurs during authentication.';
            }
            abort($data['code'], $errorMessage);
        }
    }

    /**
     * Return user identity from Auth0 for specified service
     * @param array $identities
     * @param       $service
     * @return mixed
     */
    private function getIdentityWithRequiredService(array $identities, $service)
    {
        return collect($identities)->first(function ($value, $key) use ($service) {
            return $value['provider'] === $service;
        });
    }
}
