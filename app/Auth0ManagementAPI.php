<?php
namespace App;

use App\Repository\Contracts\Auth0Repository;
use Auth;
use Auth0\SDK\API\Management;
use Auth0\SDK\Exception\CoreException;

class Auth0ManagementAPI extends Auth0Repository
{
    protected $auth0Api;

    public function __construct()
    {
        parent::__construct();
        $this->domain = config('auth.domain');
        $this->token = $this->getManagementApiAccessToken();
        $this->auth0Api = new Management($this->token, $this->domain);
    }

    public function updateUser($data, $user = null)
    {
        $user = $user ?? Auth::user();
        $this->auth0Api->users->update($user->auth0_id, $data);
        return $this->auth0Api->users->get($user->auth0_id);
    }

    public function getUser($providerUserId)
    {
        return $this->auth0Api->users->get($providerUserId);
    }

    public function getAll($properties = [])
    {
        return $this->auth0Api->users->getAll($properties);
    }

    public function getConnection($user)
    {
        if ($user->provider == 'auth0') {
            return config('laravel-auth0.connection');
        } else {
            return $user->provider;
        }
    }

    public function createUser($userData)
    {
        return $this->auth0Api->users->create($userData);
    }

    public function deleteUser($userId)
    {
        try {
            return $this->auth0Api->users->delete($userId);
        } catch (CoreException $e) {
            throw new CoreException($e);
        }
    }

    public function isUserHasCredentialsAccount(User $user)
    {
        $auth0User = $this->getUser($user->auth0_id);
        return collect($auth0User['identities'])->contains('provider', 'auth0');
    }
}
