<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Classes\Database\Eloquent\CompetitionsCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class Competition
 * @package App
 * @property string code
 * @property string title
 * @property string type
 * @property string date_start
 * @property string date_end
 * @property Collection songs
 * @method static Competition ofType ($type)
 * @see Competition::scopeOfType
 * @method static Competition withStatus ($status)
 * @see Competition::scopeWithStatus
 * @method static Competition exceptStatus ($status)
 * @see Competition::scopeExceptStatus
 * @method static Competition byTypeAndOrderedByDate ($type, $direction)
 * @see Competition::scopeByTypeAndOrderedByDate
 * @method static Competition finished
 * @see Competition::scopeFinished
 * @method static Competition top6
 * @see Competition::scopeTop6
 */
class Competition extends Model
{
    protected $primaryKey = 'code';
    public $incrementing = false;

    protected $table = self::TABLE_NAME;

    protected $dates = ['date_start', 'date_end', 'created_at', 'updated_at'];

    const TYPE_WEEKLY = 'Weekly';
    const TYPE_BIMONTHLY = 'Bimonthly';
    const TYPE_YEARLY = 'Yearly';
    const TYPE_TOP50 = 'Top50';
    const TOP6_MAX_SONGS_COUNT = 6;

    const TYPES = [
        'top50',
        'weekly',
        'bimonthly',
        'yearly'
    ];

    const STATUS_PLANNED = 0;
    const STATUS_STARTED = 1;
    const STATUS_FINISHED = 2;

    const TABLE_NAME = 'competitions';

    const FIELD_DATE_START = 'date_start';
    const FIELD_DATE_END = 'date_end';
    const FIELD_STATUS = 'status';
    const FIELD_TYPE = 'type';
    const FIELD_WINNER_SONG = 'winner_song_code';
    const FIELD_WINNER_ARTIST = 'winner_artist_code';
    const FIELD_CODE = 'code';

    const TOTAL_PLANNING_WEEKS_NUMBER = 51;

    protected $hidden = ['created_at', 'updated_at', 'pivot', 'winner_song_code', 'winner_artist_code'];

    public function votes()
    {
        return $this->hasMany(Vote::class, 'competition_code', 'code');
    }

    public function songs()
    {
        return $this->belongsToMany(Song::class, 'competitions_list', 'competition_code', 'song_code')
            ->withPivot(['position', 'liked', 'voted', 'played', 'added_to_playlist'])
            ->withTrashed()
            ->withTimestamps();
    }

    public function competitionSongs()
    {
        return $this->hasMany(CompetitionsList::class, 'competition_code', 'code');
    }

    /**
     * Extending a standard collection with new functionality
     * @param array $models
     * @return CompetitionsCollection
     */
    public function newCollection(array $models = [])
    {
        return new CompetitionsCollection($models);
    }


    public function isStarted()
    {
        return $this->status == 1;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1)
            ->whereDate('date_start', '<=', Carbon::today()->toDateString())
            ->whereDate('date_end', '>=', Carbon::today()->toDateString());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActiveSick6($query)
    {
        return $query->where('status', 1)
            ->where('type', '!=', 'Top50')
            ->whereDate('date_start', '<=', Carbon::today()->toDateString())
            ->whereDate('date_end', '>=', Carbon::today()->toDateString());
    }

    /**
     * @param Builder $query
     * @param string  $type
     * @return Builder
     */
    public function scopeOfType($query, $type)
    {
        return $query->where('type', studly_case($type));
    }

    /**
     * @param Builder $query
     * @param string $status
     * @return Builder
     */
    public function scopeWithStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    /**
     * @param Builder $query
     * @param string $status
     * @return Builder
     */
    public function scopeExceptStatus($query, $status)
    {
        return $query->where('status', '<>', $status);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeRecent($query)
    {
        return $query->where('date_end', '<', Carbon::now())->orderBy('date_start', 'desc');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeFinished($query)
    {
        return $query->where(Competition::FIELD_STATUS, Competition::STATUS_FINISHED);
    }

    /**
     * @param Builder $query
     * @param string $type
     * @param string $direction
     * @return Builder
     */
    public function scopeByTypeAndOrderedByDate($query, $type = 'Top50', $direction = 'desc')
    {
        return $query->where(Competition::FIELD_TYPE, '=', $type)
            ->orderBy(Competition::FIELD_DATE_START, $direction)
            ->orderBy(Competition::FIELD_DATE_END, $direction);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeTop6($query)
    {
        return $query->where(Competition::FIELD_TYPE, '<>', 'Top50');
    }
}
