<?php

namespace App;

use Auth;
use Carbon\Carbon;

class Subscription extends \Laravel\Cashier\Subscription
{
    protected $casts = [
        'name' => 'integer',
    ];

    public function artist()
    {
        return $this->hasOne('App\User', 'code', 'stripe_plan');
    }

    public function scopeActiveSubscriptions($query, $plan)
    {
        return $query->where([
            'user_id' => Auth::id(),
            'stripe_plan' => $plan,
        ])->where(function ($query) {
            $query->where('ends_at', '>', Carbon::now())
                ->orWhere('ends_at', null);
        });
    }

    public function scopeAllActiveSubscriptions($query)
    {
        return $query->where('user_id', Auth::id())->where(function ($query) {
            $query->where('ends_at', '>', Carbon::now())
                ->orWhere('ends_at', null);
        });
    }

    public function scopeAllActiveSubscriptionsForPlan($query, $plan)
    {
        return $query->where([
            'stripe_plan' => $plan,
            'ends_at'   => null
        ]);
    }
}
