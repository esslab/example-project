<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ModuleCompetition
 * @method static Builder findAvailableCompetitionByCode ($competition)
 * @see ModuleCompetition::scopeFindAvailableCompetitionByCode
 * @method static Builder findAvailableCompetitionByCodeWithSong ($competition, $song)
 * @see ModuleCompetition::scopeFindAvailableCompetitionByCodeWithSong
 * @package App
 * @todo this class can be divided into several classes in different modules,
 * @todo because this class adds some extra functionality to the base model
 */
class ModuleCompetition extends Competition
{
    /**
     * Returns available competition in current period of time by competition code
     * @param $query
     * @param $competition
     * @return mixed
     */
    public function scopeFindAvailableCompetitionByCode($query, $competition)
    {
        return $query->where('code', '=', $competition)
            ->whereDate('date_start', '<=', Carbon::today()->toDateString())
            ->whereDate('date_end', '>=', Carbon::today()->toDateString());
    }

    /**
     * Returns available competition with a concrete song in current period of time by competition code and song code
     * @param $query
     * @param $competition
     * @param $song
     * @return mixed
     */
    public function scopeFindAvailableCompetitionByCodeWithSong($query, $competition, $song)
    {
        return $this->scopeFindAvailableCompetitionByCode($query, $competition)
            ->whereHas('songs', function ($query) use ($song) {
                $query->where('song_code', '=', $song);
            });
    }
}
