<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $primaryKey = 'code';

    protected $fillable = ["code", "status", "privacy", "title"];

    public $incrementing = false;

    public $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_code', 'code');
    }

    public function songs()
    {
        return $this->belongsToMany('App\Song', 'playlists_songs', 'playlist_code', 'song_code')
            ->withTrashed()
            ->withTimestamps()
            ->withPivot('privacy');
    }
}
