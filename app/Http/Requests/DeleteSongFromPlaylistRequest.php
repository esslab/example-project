<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteSongFromPlaylistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->playlist);
    }

    /**
     * @return array
     */
    public function validationData()
    {
        return [
            'song' => $this->song->code,
            'playlist' => $this->playlist->code,
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'song' => [
                Rule::exists('playlists_songs', 'song_code')->where(function ($query) {
                    $query->where('playlist_code', $this->playlist->code);
                })
            ],
        ];
    }
}
