<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSongRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $song = $this->song;
        return $song && $this->user()->can('update', $song);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"        => "sometimes|string|max:255",
            "cover"        => "sometimes|string|max:255",
            "producer"     => "sometimes|string|max:255",
            'genres'       => 'sometimes|array',
            'langs'        => 'sometimes|array',
            'can_download' => 'sometimes|string',
            'can_listen'   => 'sometimes|string',
            'genres.*'     => 'required_with:genres|uuid|exists:genres,code',
            'langs.*'      => 'required_with:langs|uuid|exists:languages,code',
        ];
    }
}
