<?php

namespace App\Http\Requests;

/**
 * Class SaveMediaRequest
 * @package App\Http\Requests
 * @property string type
 * @property string source_file
 * @property integer privacy
 * @property integer status
 * @property string thumbnail
 */
class UpdateMediaItemRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $item = $this->item;
        return $item && $this->user()->can('update', $item);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:255',
            'privacy' => 'sometimes|integer',
            'access' => 'sometimes|string',
            'status' => 'sometimes|integer',
        ];
    }
}
