<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddSongToCompetitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * @return array
     */
    public function validationData()
    {
        $this->merge(['competition_code' => $this->competition->code]);
        return $this->toArray();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'competition_code' => [
                "not_of_type_closed_top50:{$this->competition->status},{$this->competition->type}",
                "not_completely_filled_with_songs:{$this->competition->code},{$this->competition->type}"
            ],
            'song_code' => [
                'required',
                'uuid',
                'exists:songs,code',
                "unique_song_or_artist_in_competition:{$this->competition->code},{$this->competition->type}"
            ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'song_code.exists' => 'The :attribute field is invalid. Such Song does not exists.',
        ];
    }
}
