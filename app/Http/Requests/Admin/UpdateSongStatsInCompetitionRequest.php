<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSongStatsInCompetitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $this->merge([
            'competition_code' => $this->route('competition'),
            'song_code'        => $this->route('song')
        ]);

        return $this->all();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'competition_code' => [
                'bail',
                'required',
                'uuid',
                'exists:competitions,code',
                'planned_weekly_competition'
            ],
            'song_code' => [
                'required',
                'uuid',
                Rule::exists('competitions_list')->where(function ($query) {
                    $query->where('competition_code', $this->competition_code);
                })
            ],
            'position' => [
                'required',
                'numeric',
                'in:1,2,3,4,5,6',
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'song_code.exists' => 'Song should participate in Competition.',
        ];
    }
}
