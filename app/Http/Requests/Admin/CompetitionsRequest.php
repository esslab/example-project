<?php
namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;
use Auth;

/**
 * Class CompetitionsRequest
 * @package App\Http\Requests\Admin
 * @property string $type
 */
class CompetitionsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    protected function prepareForValidation()
    {
        $type = $this->route('type');
        $this->merge(['type' => $type]);
    }

    public function rules()
    {
        return [];
    }
}
