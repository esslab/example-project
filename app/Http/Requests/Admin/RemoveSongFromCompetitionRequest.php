<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RemoveSongFromCompetitionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $this->merge(['song_code' => $this->song]);
        return $this->all();
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $competitionCode = $this->competition->code;

        return [
            'song_code' => [
                Rule::exists('competitions_list', 'song_code')->where(function ($query) use ($competitionCode) {
                    $query->where('competition_code', $competitionCode);
                })
            ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'song_code.exists' => 'The :attribute field is invalid. Competition has no such song.',
        ];
    }
}
