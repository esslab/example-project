<?php
namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;
use Auth;

/**
 * Class CompetitionRequest
 * @package App\Http\Requests\Admin
 * @property string $competition
 */
class CompetitionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    protected function prepareForValidation()
    {
        $competition = $this->route('competition');
        $this->merge(['competition' => $competition]);
    }

    public function rules()
    {
        return [
            'competition' => [
                'required',
                'string',
                'exists:competitions,code'
            ]
        ];
    }
}
