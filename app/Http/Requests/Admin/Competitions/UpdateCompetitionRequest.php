<?php
namespace App\Http\Requests\Admin\Competitions;

use App\Http\Requests\Request;
use Auth;

/**
 * Class UpdateCompetitionRequest
 * @package App\Http\Requests\Admin\Competitions
 * @property string $competition
 * @property integer $status
 */
class UpdateCompetitionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'status' => [
                'required',
                'integer',
                'min:1',
                'max:2',
                'not_update_status_closed:' . $this->competition->status
            ]
        ];
    }
}
