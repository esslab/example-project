<?php

namespace App\Http\Requests\Admin\Competitions;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

/**
 * Class GetCompetitionByTypeRequest
 * @package App\Http\Requests\Admin\Competitions
 * @property string $type
 */
class GetCompetitionByTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string|in:top50,weekly,bimonthly,yearly,Top50,Weekly,Bimonthly,Yearly'
        ];
    }
}
