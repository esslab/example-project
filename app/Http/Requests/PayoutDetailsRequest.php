<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class PayoutDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user == Auth::id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string',
            'last_name' => 'string',
            'birthday' => 'date',
            'country' => 'string',
            'city' => 'string',
            'address' => 'string',
            'postcode' => 'string|max:12',
            'id_number' => 'string',
            'file' => 'string',
            'account_number' => 'string',
            'swift' => 'string',
            'bank_name' => 'string',
            'personal_id' => 'string'
        ];
    }
}
