<?php

namespace App\Http\Requests;

use Auth;

class GetPayoutsRequest extends FilterRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'month' => 'sometimes|integer|between:1,12',
            'year' => 'sometimes|date_format:Y|required_with:month',
            'page' => 'sometimes|integer|min:0',
        ]);
    }
}
