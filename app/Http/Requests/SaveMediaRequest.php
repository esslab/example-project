<?php

namespace App\Http\Requests;

/**
 * Class SaveMediaRequest
 * @package App\Http\Requests
 * @property string type
 * @property string source_file
 * @property integer privacy
 * @property integer status
 * @property string thumbnail
 */
class SaveMediaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string',
            'title' => 'required|string|max:255',
            'source_file' => 'required|string|url',
            'access' => 'required|string',
            'privacy' => 'sometimes|integer',
            'status' => 'sometimes|integer',
            'thumbnail' => 'sometimes|required|string',
            'duration' => 'sometimes|required|string|max:20'
        ];
    }

    public function messages()
    {
        return [
            'source_file.url' => ':attribute should be a valid url.'
        ];
    }
}
