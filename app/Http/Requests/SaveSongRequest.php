<?php

namespace App\Http\Requests;

use App\Services\SongService;

class SaveSongRequest extends Request
{
    /**
     * @throws \App\Exceptions\ExtractSongUuidException
     */
    protected function prepareForValidation()
    {
        if (!empty($this->file)) {
            $this->merge(['code' => SongService::getUuidFromFileName($this->file)]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'         => 'required_with:file|uuid|unique:songs,code',
            'cover'        => 'required|string|url',
            'file'         => 'required|string|url',
            'title'        => 'required|string|max:255',
            'producer'     => 'required|min:3|string|max:255',
            'genres'       => 'required|array',
            'langs'        => 'required|array',
            'genres.*'     => 'required_with:genres|uuid|exists:genres,code',
            'langs.*'      => 'required_with:langs|uuid|exists:languages,code',
            'can_download' => 'sometimes|string',
            'can_listen'   => 'required|string',
            'duration'     => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'code.unique' => 'Song with such file attribute value already present.',
            'code.uuid' => 'Song with such file attribute value has incorrect UUID in file name.',
            'cover.url' => ':attribute should be a valid url.',
            'file.url' => ':attribute should be a valid url.',
        ];
    }
}
