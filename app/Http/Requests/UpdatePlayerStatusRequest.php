<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePlayerStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $data = $this->except('songs');
        $data['songs'] = json_decode($this->songs);

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "songs" => "array",
            "songs.*" => [
                'sometimes',
                'uuid',
                'exists:songs,code'
            ],
            "isShuffle" => [
                "required",
                "in:0,1"
            ],
            "repeatState" => [
                "required",
                "in:0,1,2"
            ],
            "timePaused" => [
                "required",
                "numeric"
            ],
            "trackPaused" => "required|integer"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'songs.*.exists' => 'The :attribute field is invalid. Such Song does not exists.'
        ];
    }
}
