<?php

namespace App\Http\Requests;

class SongLineRequest extends Request
{
    public function rules()
    {
        return [
            'offset' => 'sometimes|integer',
            'limit' => 'sometimes|integer'
        ];
    }
}
