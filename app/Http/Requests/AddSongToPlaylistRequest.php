<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AddSongToPlaylistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->playlist);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $playlistCode = $this->playlist->code;
        return [
            'song_code' => [
                'required',
                'uuid',
                'exists:songs,code',
                Rule::unique('playlists_songs')->where(function ($query) use ($playlistCode) {
                    $query->where('playlist_code', $playlistCode);
                })
            ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return \Illuminate\Support\MessageBag|array
     */
    public function messages()
    {
        return [
            'song_code.exists' => 'The :attribute field is invalid. Such Song does not exists.',
            'song_code.unique' => 'The Song is already in the Playlist.',
        ];
    }
}
