<?php

namespace App\Http\Requests;

class UpdateMediaLambdaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'duration'      => 'required|string|max:20',
            'transcodingCompleted'      => 'required|boolean',
            'lambda_token'  => 'required|string|max:255|token'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'lambda_token.token' => 'Lambda token is invalid!'
        ];
    }
}
