<?php

namespace App\Http\Requests;

class UpdateSongLambdaRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transcodingCompleted'      => 'required|boolean',
            'lambda_token'  => 'required|string|max:255|token'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'lambda_token.token' => 'Lambda token is invalid!'
        ];
    }
}
