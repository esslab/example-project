<?php
namespace App\Http\Requests;

use Illuminate\Http\Response;

class DiscoverFilterRequest extends Request
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        if ($this->has('ids')) {
            $this->merge(['ids' => explode(',', $this->input('ids'))]);
        }

        if ($this->has('genres')) {
            $this->merge(['genres' => explode(',', $this->input('genres'))]);
        }

        if ($this->has('languages')) {
            $this->merge(['languages' => explode(',', $this->input('languages'))]);
        }

        if ($this->has('sort')) {
            $sort = collect(explode(',', $this->input('sort')))
                ->map(function ($item) {
                    return str_replace(' ', '+', $item);
                });

            if ($sort->contains('random')) {
                $sort = collect(['random']);
            }

            $this->merge(['sort' => $sort->all()]);
        }

        return $this->all();
    }
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'offset' => 'sometimes|integer',
            'limit' => 'sometimes|integer',
            'sort' => 'sometimes|required|array',
            'sort.*' => [
                'required_with:sort',
                'string',
                'regex:/^((random|(\+|\-){1}updated_at|(\+|\-){1}title|(\+|\-){1}rating|(\+|\-){1}producer){1})+$/'
            ],
            'genres' => 'sometimes|required|array',
            'genres.*' => 'required_with:genres|uuid|exists:genres,code',
            'languages' => 'sometimes|required|array',
            'languages.*' => 'required_with:languages|uuid|exists:languages,code',
            'from' => 'sometimes|date_format:Y-m-d',
            'to' => 'sometimes|date_format:Y-m-d',
            'artist' => 'sometimes|user_code|exists:users,code',
            'ids' => 'sometimes|array',
            'ids.*' => 'required_with:ids|uuid'
        ];
    }

    /**
     * @param array $errors
     * @return mixed
     */
    public function response(array $errors)
    {
        return response()->json(['errors' => $errors], Response::HTTP_BAD_REQUEST);
    }

    public function messages()
    {
        return [
            'offset.integer' => 'Offset must be a number.',
            'limit.integer' => 'Limit must be a number.',
            'sort.*.regex' => 'Sort value contains unavailable characters.',
            'genres.array' => 'Genres value is not an array.',
            'genres.*.uuid' => 'Genre code must be in UUID format.',
            'genres.*.exists' => 'Genre with such code does not exist.',
            'languages.array' => 'Languages value is not an array.',
            'languages.*.uuid' => 'Language code must be in UUID format.',
            'languages.*.exists' => 'Language with such code does not exist.',
            'from.date_format' => 'From has incorrect format.',
            'to.date_format' => 'From has incorrect format.',
            'artist.user_code' => 'You try to look for incorrect artist.',
            'artist.exists' => 'Can not find such artist, looks like it does not exists.'
        ];
    }
}
