<?php

namespace App\Http\Requests;

use App\Subscription;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class PlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user->can('update', Auth::user());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => 'required|integer|subscription_price_values'
        ];
    }
}
