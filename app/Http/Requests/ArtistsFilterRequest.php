<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArtistsFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offset' => 'sometimes|integer',
            'limit' => 'sometimes|integer',
            'sort.*' => ['sometimes', 'string', 'regex:/^((\+|\-){1}(created_at|name|rating){1})+$/'],
            'genres.*' => ['sometimes', 'string', 'uuid'],
            'languages.*' => ['sometimes', 'string', 'uuid'],
            'top6' => 'sometimes|boolean'
        ];
    }

    protected function prepareForValidation()
    {
        $preparedAttributes = [];

        if ($this->has('genres')) {
            $genres = $this->input('genres');

            $genres = explode(',', $genres);

            $preparedAttributes['genres'] = $genres;
        }

        if ($this->has('languages')) {
            $languages = $this->input('languages');

            $languages = explode(',', $languages);

            $preparedAttributes['languages'] = $languages;
        }

        if ($this->has('top6')) {
            $top6 = $this->input('top6');

            $top6 = $top6 == 'true';

            $preparedAttributes['top6'] = $top6;
        }

        if ($this->has('sort')) {
            $sort = $this->input('sort');

            $sort = explode(',', $sort);

            $preparedAttributes['sort'] = $sort;
        }

        $this->merge($preparedAttributes);
    }

    public function messages()
    {
        return [
            'sort.*.regex' => 'You provided wrong sort field.'
        ];
    }
}
