<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetMediaItemsRequest extends FormRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        if ($this->has('ids')) {
            $this->merge(['ids' => explode(',', $this->input('ids'))]);
        }
        return $this->all();
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'offset' => 'sometimes|integer',
            'limit' => 'sometimes|integer',
            'ids' => 'sometimes|required|array',
            'ids.*' => 'required_with:ids|uuid'
        ];
    }
}
