<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPlaylistRequest extends FormRequest
{
    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $data = [];
        $data['title'] = $this->title;
        $data['songs'] = json_decode($this->songs);

        return $data;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'songs' => [
                'sometimes',
                'array'
            ],
            'songs.*' => [
                'required_with:songs',
                'uuid',
                'exists:songs,code'
            ],
            'title' => [
                'required',
                'string',
                'min:3',
                'max:255'
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'songs.*.uuid' => 'The :attribute field should be in UUID format',
            'songs.*.exists' => 'The :attribute field is invalid. Such Song does not exists.'
        ];
    }
}
