<?php

namespace App\Http\Requests;

use Auth;

class UserDataRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && Auth::id() === (int)$this->route('user');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                 => 'required|string|min:3',
            'password'             => 'required_with:new_password,new_password_confirm|min:4',
            'new_password'         => 'required_with:password,new_password_confirm|min:4',
            'new_password_confirm' => 'required_with:password,new_password|same:new_password|min:4',
            'location'             => 'sometimes|string',
            'genre'                => 'bail|required_if:is_artist,true|array',
            'genre.*'              => 'required_with:genre|uuid',
            'languages'            => 'bail|required_if:is_artist,true|array',
            'languages.*'          => 'required_with:languages|uuid',
            'avatar'               => 'required|string',
            'artist_bio'           => 'sometimes|string',
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'is_artist' => Auth::user()->is_artist
        ]);
    }

    public function messages()
    {
        return [
            'genre.required_if' => "The field :attribute required for the Artist.",
            'languages.required_if' => "The field :attribute required for the Artist.",
            'avatar.url' => ':attribute should be a valid url.'
        ];
    }
}
