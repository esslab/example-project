<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode;

class CheckApiForMaintenanceMode extends CheckForMaintenanceMode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (App::isDownForMaintenance()) {
            if ($request->getRequestUri() === "/api/healthcheck") {
                return $next($request);
            }
        }
        return parent::handle($request, $next);
    }
}
