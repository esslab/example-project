<?php
namespace App\Http\Middleware;

use App;
use App\Exceptions\InvalidTokenException;
use Auth0\Login\Middleware\Auth0JWTMiddleware;
use Auth0\SDK\Exception\CoreException;
use Closure;
use App\JWTUser;

class StrictAuth0Middleware extends Auth0JWTMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws InvalidTokenException
     */
    public function handle($request, Closure $next)
    {
        $token = $this->getToken($request);
        if (!$this->validateToken($token)) {
            throw new InvalidTokenException($token, null, 'Empty token.');
        }
        if ($token) {
            try {
                $jwtUser = new JWTUser($token);
            } catch (CoreException $e) {
                throw new InvalidTokenException($token, $e);
            }
            // if it does not represent a valid user, return a HTTP 401
            $user = $this->userRepository->getUserByDecodedJWT($jwtUser);
            if (!$user) {
                throw new InvalidTokenException($token, null, 'Unauthorized user');
            }
            // lets log the user in so it is accessible
            \Auth::login($user);
        }

        // continue the execution
        return $next($request);
    }
}
