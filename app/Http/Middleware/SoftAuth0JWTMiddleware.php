<?php
namespace App\Http\Middleware;

use Auth0\Login\Middleware\Auth0JWTMiddleware;
use Illuminate\Http\Request;

class SoftAuth0JWTMiddleware extends StrictAuth0Middleware
{
    /**
     * @param Request $request
     * @return string
     */
    protected function getToken($request)
    {
        if ($request->hasHeader('Authorization')) {
            return parent::getToken($request);
        } else {
            return false;
        }
    }

    public function handle($request, \Closure $next)
    {
        if ($this->getToken($request)) {
            return parent::handle($request, $next);
        } else {
            return $next($request);
        }
    }
}
