<?php

namespace App\Http\Controllers;

use App;
use App\GalleryItem;
use App\Song;
use App\User;
use Illuminate\Http\Request;

class StaticPagesController extends Controller
{
    private $defaultSongCover = 'https://songline.fm/assets/default_cover.png';

    private $coverDimension = '228';

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSong(User $user, Request $request)
    {
        $songCode = $request->song;
        $song = $user->songs()->find($songCode);

        if ($song) {
            $cover = $this->getSongCover($song);
            $coverDimension = $this->coverDimension;
            return view('crawlers.song', compact('song', 'user', 'cover', 'coverDimension'));
        }
        return view('crawlers.default', ['user' => $user]);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPlaylist(User $user, Request $request)
    {
        $playlistCode = $request->playlist;
        $playlist = $user->playlists()->find($playlistCode);
        if ($playlist) {
            $song = $playlist->songs()->first();
            $cover = $this->getSongCover($song);
            $coverDimension = $this->coverDimension;
            return view('crawlers.playlist', compact('playlist', 'user', 'cover', 'coverDimension'));
        }
        return view('crawlers.default', ['user' => $user]);
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getGalleryItem(User $user, Request $request)
    {
        $itemCode = $request->gallery;
        $item = GalleryItem::where('code', $itemCode)->first();
        if ($item) {
            $image = $this->defaultSongCover;
            if ($item->type == 'video') {
                $image = config('app.app_files_url') . 'video/converted/' . $item->code . '_thumb_00001.jpg';
            } elseif ($item->type == 'picture') {
                $image = config('app.app_files_url') . 'images/converted/' . $item->code . '.jpg';
            }
            return view('crawlers.gallery', ['item' => $item, 'user' => $user, 'image' => $image]);
        } else {
            return view('crawlers.default', ['user' => $user]);
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getArtist(User $user)
    {
        return view('crawlers.artist', ['user' => $user]);
    }

    /**
     * Extract dimension from string like '...asd_70x70.jpg'
     * @param string $cover
     * @return bool|string
     */
    private function getDimensionFromString($cover)
    {
        $coverDimension = substr(
            $cover,
            strrpos($cover, '_') + 1,
            strrpos($cover, 'x') - strrpos($cover, '_') - 1
        );

        return $coverDimension;
    }

    /**
     * @param Song $song
     * @return mixed|string
     */
    private function getSongCover(Song $song)
    {
        if (empty($song->cover)) {
            $cover = $this->defaultSongCover;
        } elseif (is_array($song->cover)) {
            $coverDimensions = "{$this->coverDimension}x{$this->coverDimension}";
            $cover = $song->cover[$coverDimensions];
        } else {
            $cover = $song->cover;
            $this->coverDimension = $this->getDimensionFromString($cover);
        }

        return $cover;
    }
}
