<?php

namespace App\Http\Controllers;

use App;
use App\Charge;
use App\Exceptions\StripeWebHookException;
use App\Mail\SubscriptionPaymentFails;
use App\Mail\SubscriptionWasCanceled;
use App\User;
use Carbon\Carbon;
use Config;
use DB;
use Exception;
use Mail;
use Stripe\Event as StripeEvent;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class WebhookController extends CashierController
{
    /**
     * Handle a Stripe webhook.
     *
     * if you wish to handle the invoice.payment_succeeded webhook,
     * you should add a handleInvoicePaymentSucceeded method to the controller
     */
    public function handleInvoicePaymentSucceeded($payload)
    {
        DB::beginTransaction();
        try {
            $user = User::where('stripe_id', $payload['data']['object']['customer'])->firstOrFail();
            $charge = new Charge();
            $charge->user_code = $user->code;
            $charge->plan = intval($payload['data']['object']['lines']['data'][0]['plan']['id']);
            $charge->amount = $payload['data']['object']['amount_due'];
            $charge->currency = $payload['data']['object']['currency'];
            $charge->created = Carbon::createFromTimestamp($payload['data']['object']['date'])->toDateTimeString();
            $charge->save();
            $this->checkSubscriptionInconsistency($user, $charge->plan);
            $user->subscription($charge->plan)->payment_failures = 0;
            $user->subscription($charge->plan)->save();
            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param $payload
     */
    public function handleInvoicePaymentFailed($payload)
    {
        $user = User::where('stripe_id', $payload['data']['object']['customer'])->first();

        if ($user) {
            $plan = intval($payload['data']['object']['lines']['data'][0]['plan']['id']);
            $artist = User::where('code', $plan)->first();
            $this->checkSubscriptionInconsistency($user, $plan);
            $failures = $user->subscription($plan)->payment_failures;
            if ($failures > 2) {
                $user->subscription($plan)->cancelNow();
                Mail::to($user->email)->send(new SubscriptionWasCanceled($artist->name));
            } else {
                $user->subscription($plan)->increment('payment_failures');
                Mail::to($user->email)->send(new SubscriptionPaymentFails($artist->name));
            }
        }
    }

    /**
     * @param string $id
     * @return bool
     */
    protected function eventExistsOnStripe($id)
    {
        if (App::environment() == 'local') {
            return true;
        }
        try {
            return ! is_null(StripeEvent::retrieve($id, Config::get('services.stripe.secret')));
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Testing webhooks - for local usage only
     * @throws \TeamTNT\Stripe\InvalidEventException
     */
    public function testWebhook()
    {
        $tester = new \TeamTNT\Stripe\WebhookTester();
        $tester->setVersion('2015-03-24');
        $tester->setEndpoint(config('api_url') . 'stripe/webhook');
        $response = $tester->triggerEvent('invoice.payment_failed');
        return $response;
    }

    /**
     * @param User $user
     * @param int $plan
     * @throws StripeWebHookException
     */
    private function checkSubscriptionInconsistency(User $user, int $plan)
    {
        if ($user->subscribed($plan) == false) {
            throw new StripeWebHookException('There is no subscription in database, but charge occurred');
        }
    }
}
