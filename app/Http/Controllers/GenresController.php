<?php
namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Response;

class GenresController extends Controller
{
    /**
     * Returns all available genres
     * @return mixed
     */
    public function all()
    {
        $genres = Genre::orderBy('title', 'asc')->get(['code', 'title']);

        return response()
            ->json($genres, Response::HTTP_OK)
            ->header('Cache-Control', 'max-age=86400');
    }
}
