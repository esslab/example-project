<?php
namespace App\Http\Controllers;

use App\Http\Requests\UpdatePlayerStatusRequest;
use App\Services\PlayerService;
use Auth;
use Response;
use Illuminate\Http\Response as HttpResponse;

class PlayerController extends Controller
{
    /**
     * Return Player for current User
     * (repeat on/off, shuffle on/off, current track played, current playlist played.)
     *
     * @param PlayerService $playerService
     * @return string Illuminate\Http\JsonResponse
     */
    public function getStatus(PlayerService $playerService)
    {
        $user = Auth::user();
        $player = $playerService->getPlayerForUser($user);

        return Response::json($player, HttpResponse::HTTP_OK);
    }

    /**
     * Update or create Player for current User
     *
     * @param UpdatePlayerStatusRequest $request
     * @param PlayerService             $playerService
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(UpdatePlayerStatusRequest $request, PlayerService $playerService)
    {
        $user = Auth::user();

        $playerService->updatePlayerForUser($user, $request);

        return Response::json(['statusCode' => 200, 'message' => 'Updated'], HttpResponse::HTTP_OK);
    }
}
