<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArtistFollowRequest;
use App\Http\Requests\ArtistUnfollowRequest;
use App\Http\Requests\PayoutDetailsRequest;
use App\Notifications\SubscriptionPriceChanged;
use App\PayoutDetails;
use App\Repository\SubscriptionsRepository;
use App\Repository\UserRepository;
use App\User;
use App\Auth0AuthenticationAPI;
use App\Auth0ManagementAPI;
use App\Http\Requests\ArtistsFilterRequest;
use App\Http\Requests\UserDataRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Repository\Modules\Profiles\ApiArtists;

class UserController extends Controller
{
    /**
     * @return mixed
     */
    public function getCode()
    {
        $user = collect(Auth::user())->only(
            'code',
            'auth0_id',
            'name',
            'avatar',
            'email',
            'multichannel'
        );
        $user['role'] = Auth::user()->isAdmin() ? 'admin' : 'user';
        return response()->json($user, HttpResponse::HTTP_OK);
    }

    /**
     * @param ArtistsFilterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getArtists(ArtistsFilterRequest $request)
    {
        $artists = ApiArtists::profiles($request)
            ->get();

        return response()
            ->json($artists, HttpResponse::HTTP_OK)
            ->header('Cache-Control', 'max-age=120');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProfileSettings()
    {
        $user = User::with('genres', 'languages')->find(Auth::id());
        $user = $this->appendHasDatabaseAccount($user);
        return response()->json($user, HttpResponse::HTTP_OK);
    }

    /**
     * @param UserDataRequest        $request
     * @param Auth0AuthenticationAPI $authUser
     * @param Auth0ManagementAPI     $manageUser
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     * @throws \OAuth2\Exception
     */
    public function setProfileSettings(
        UserDataRequest $request,
        Auth0AuthenticationAPI $authUser,
        Auth0ManagementAPI $manageUser
    ) {
        if ($request->has('password')) {
            $authUser->basicAuthViaCredentials($request);
        }

        $user = Auth::user();
        $user->name = $request->name;
        $user->location = $request->input('location', '');
        $user->avatar = $request->avatar;
        $user->bio = $request->artist_bio;

        $connection = $manageUser->getConnection($user);

        $data = [
            'connection' => $connection,
            "user_metadata" => [
                "nickname" => $request->name,
                "location" => $request->input('location', ''),
                'picture' => $request->avatar,
            ]
        ];

        if ($request->has('new_password') && $manageUser->isUserHasCredentialsAccount($user)) {
            $data['password'] = $request->new_password;
            // update data[connection] if connection was not an Auth0
            $data['connection'] = config('laravel-auth0.connection');
        }

        $manageUser->updateUser($data);

        $genresIds = $user->is_artist ? $request->input('genre') : [];
        $langsIds = $user->is_artist ? $request->input('languages') : [];

        $user->save();
        $user->genres()->sync($genresIds);
        $user->languages()->sync($langsIds);

        return response()->json([]);
    }

    /**
     * @param User $user
     * @param ArtistFollowRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function followArtist(User $user, ArtistFollowRequest $request)
    {
        DB::transaction(function () use ($user) {
            SubscriptionsRepository::addFreeSubscription($user);
            UserRepository::follow($user);
        });

        return response()->json([], HttpResponse::HTTP_CREATED);
    }

    /**
     * @param User $user
     * @param ArtistUnfollowRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function unfollowArtist(User $user, ArtistUnfollowRequest $request)
    {
        DB::transaction(function () use ($user) {
            if ($user->plan_cost == 0 && Auth::user()->subscribed($user->code)) {
                Auth::user()->subscription($user->code)->delete();
            }
            $user->followedByUsers()->detach(Auth::id());
            $user->stat()->decrement('followed');
        });

        return response()->json([], HttpResponse::HTTP_NO_CONTENT);
    }

    /**
     * Return access token from Auth0 service for Frontend application
     * @param Auth0AuthenticationAPI $auth0AuthApi
     * @return mixed
     * @throws \App\Exceptions\Repository\Auth0BadAPIRequestException
     * @throws \App\Exceptions\Repository\Auth0BadAPIResponseException
     */
    public function getAccessToken(Auth0AuthenticationAPI $auth0AuthApi, Request $request)
    {
        $this->validate($request, [
            'provider_user_id' => 'required|string',
            'service' => 'required|string|in:dropbox,google-oauth2,facebook'
        ]);
        $response = $auth0AuthApi->getAccessToken($request->all());
        return response()->json($response, HttpResponse::HTTP_OK);
    }

    /**
     * @param PayoutDetails $user
     * @param PayoutDetailsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function savePayoutDetails(PayoutDetailsRequest $request, $user)
    {
        $payoutDetails = PayoutDetails::findOrNew($user);
        $payoutDetails->user_code = Auth::id();
        $payoutDetails->fill($request->all());
        $payoutDetails->save();
        return response()->json([], HttpResponse::HTTP_OK);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPayoutDetails(User $user)
    {
        $payoutDetails = PayoutDetails::find($user->code);
        return response()->json($payoutDetails, HttpResponse::HTTP_OK);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotifications()
    {
        $notifications = collect($this->getPriceChangedNotification());
        $notifications = $notifications->merge(Auth::user()->getUnreadNotifications());
        return response()->json($notifications, HttpResponse::HTTP_OK);
    }

    /**
     * @return array
     */
    private function getPriceChangedNotification()
    {
        $notificationsCount = Auth::user()->unreadNotifications()
            ->where('type', SubscriptionPriceChanged::class)
            ->get()
            ->count();

        return [
            'SubscriptionPriceChanged' => [
                'data' => [
                    'artist_count' => $notificationsCount
                ]
            ]
        ];
    }

    /**
     * @param $notification
     * @return \Illuminate\Http\JsonResponse
     */
    public function markNotificationAsRead($notification)
    {
        $notification = Auth::user()->unreadNotifications()->findOrFail($notification);
        $notification->read_at = Carbon::now();
        $notification->save();
        return response()->json([], HttpResponse::HTTP_NO_CONTENT);
    }

    /**
     * Mark notifications of type SubscriptionPriceChanged as read
     * @return \Illuminate\Http\JsonResponse
     */
    public function markNotificationsSubscriptionPriceChangedAsRead()
    {
        Auth::user()->unreadNotifications()
            ->where('type', SubscriptionPriceChanged::class)
            ->update(['read_at' => Carbon::now()]);

        return response()->json([], HttpResponse::HTTP_OK);
    }

    /**
     * @param $user
     * @return mixed
     */
    private function appendHasDatabaseAccount($user)
    {
        $auth0 = new Auth0ManagementAPI();
        $auth0User = $auth0->getUser(Auth::user()->auth0_id);
        foreach ($auth0User['identities'] as $identity) {
            if ($identity['provider'] == 'auth0') {
                $user['has_database_account'] = true;
            }
        }
        return $user;
    }
}
