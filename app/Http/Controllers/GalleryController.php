<?php

namespace App\Http\Controllers;

use App\Exceptions\InternalErrorException;
use App\GalleryItem;
use App\Gallery;
use App\Http\Requests\GetMediaItemsRequest;
use App\Http\Requests\SaveMediaRequest;
use App\Http\Requests\UpdateMediaItemRequest;
use App\Http\Requests\UpdateMediaLambdaRequest;
use App\MediaLike;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Faker\Generator as Faker;
use Illuminate\Http\Response;
use Validator;

class GalleryController extends Controller
{
    /**
     * Create new media item in User gallery.
     * Also Gallery will be created if not exists
     *
     * @param SaveMediaRequest $request
     * @param Faker            $faker
     * @return mixed
     * @throws InternalErrorException
     * @throws \Exception
     * @throws \Throwable
     */
    public function saveMediaItem(SaveMediaRequest $request, Faker $faker)
    {
        $gallery = Gallery::forUser(Auth::id())->first();
        $newGalleryItemCode = filename_from_path($request->source_file);

        if (is_null($newGalleryItemCode)) {
            throw new InternalErrorException("Can't extract filename from source_file({$request->source_file})");
        }
        // @todo better to do this action when the user first time will be created
        if (is_null($gallery)) {
            $gallery = new Gallery();
            $gallery->title = Auth::user()->name . ' gallery';
            $gallery->user_code = Auth::id();
        }

        $item = GalleryItem::findOrNew($newGalleryItemCode);
        $item->code = $newGalleryItemCode;
        $item->user_code = Auth::id();
        $item->type = $request->type;
        $item->source_file = $request->source_file;
        $item->thumbnail = $request->thumbnail;
        $item->access = $request->access;
        $item->title = $request->title;
        $item->duration = $request->input('duration', null);

        DB::transaction(function () use ($gallery, $item) {
            $gallery->save();
            $item->gallery_code = $gallery->code;
            $item->save();
        });
        return response()->json($item, HttpResponse::HTTP_CREATED);
    }

    /**
     * Update media item in User gallery.
     *
     * @param $userCode
     * @param GalleryItem $item
     * @param UpdateMediaItemRequest $request
     * @return mixed
     */
    public function updateMediaItem($userCode, GalleryItem $item, UpdateMediaItemRequest $request)
    {
        $item->update($request->input());
        return response()->json([], HttpResponse::HTTP_NO_CONTENT);
    }

    /**
     * Returns collections of items for default user gallery
     * @param $userCode
     * @param GetMediaItemsRequest $request
     * @return mixed
     */
    public function getMediaItems($userCode, GetMediaItemsRequest $request)
    {
        $rules = ['user' => 'exists:users,code'];
        $validator = Validator::make(['user' => $userCode], $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), HttpResponse::HTTP_BAD_REQUEST);
        }
        $offset = $request->input('offset', 0);
        $limit = $request->input('limit', 10);
        $query = GalleryItem::forUser($userCode)
            ->offset($offset)
            ->limit($limit)
            ->latest();
        if ($request->has('ids')) {
            $query->whereIn('code', $request->input('ids'));
        }
        $media = $query->get();

        return response()->json($media, HttpResponse::HTTP_OK);
    }

    /**
     * Returns specified Item from default user gallery
     * @param $userCode
     * @param $itemCode
     * @return mixed
     */
    public function getMediaItemById($userCode, $itemCode)
    {
        $rules = ['user' => 'exists:users,code'];
        $validator = Validator::make(['user' => $userCode], $rules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), HttpResponse::HTTP_BAD_REQUEST);
        }
        $mediaItem = GalleryItem::forUser($userCode)->findOrFail($itemCode);

        return response()->json($mediaItem, HttpResponse::HTTP_OK);
    }

    /**
     * Delete one item from authenticated User gallery
     *
     * @param string $userCode
     * @param string $galleryItemCode
     * @return mixed
     */
    public function deleteMediaItem($userCode, $galleryItemCode)
    {
        $galleryItem = GalleryItem::findOrFail($galleryItemCode);
        $this->authorize('delete', $galleryItem);
        $galleryItem->delete();

        return response()->json([], HttpResponse::HTTP_NO_CONTENT);
    }

    /**
     * Update media item duration
     *
     * @param string                   $galleryItemId
     * @param UpdateMediaLambdaRequest $request
     * @return mixed
     */
    public function lambdaUpdateMediaItem($galleryItemId, UpdateMediaLambdaRequest $request)
    {
        $galleryItem = GalleryItem::findOrFail($galleryItemId);
        $newData = $request->except('lambda_token');
        $galleryItem->update($newData);
        return response()->json('Updated', HttpResponse::HTTP_OK);
    }

    /**
     * @param GalleryItem $galleryItem
     * @return mixed
     */
    public function likeAdd($galleryItem)
    {
        $galleryItem = GalleryItem::findOrFail($galleryItem);

        if (!Auth::user()->can('like', $galleryItem)) {
            abort(409);
        }

        MediaLike::create([
            'item_code' => $galleryItem->code,
            'user_code' => Auth::id()
        ]);

        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Like was added.'
        ], Response::HTTP_OK);
    }

    /**
     * @param GalleryItem $galleryItem
     * @return \Illuminate\Http\JsonResponse
     */
    public function likeRemove($galleryItem)
    {
        $galleryItem = GalleryItem::findOrFail($galleryItem);

        if (!Auth::user()->can('dislike', $galleryItem)) {
            abort(409);
        }

        MediaLike::where('item_code', $galleryItem->code)
            ->where('user_code', Auth::id())
            ->delete();

        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Like was removed.'
        ], Response::HTTP_OK);
    }
}
