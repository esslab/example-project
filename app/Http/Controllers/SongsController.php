<?php
namespace App\Http\Controllers;

use App;
use App\ArtistStat;
use App\Events\SongDeleted;
use App\GalleryItem;
use App\Http\Requests\DiscoverFilterRequest;
use App\Http\Requests\SaveSongRequest;
use App\Http\Requests\SongLineRequest;
use App\Http\Requests\UpdateSongLambdaRequest;
use App\Http\Requests\UpdateSongRequest;
use App\SongStat;
use App\Services\SongService;
use App\Song;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SongsController extends Controller
{
    /**
     * @var SongService
     */
    private $songService;

    /**
     * SongsController constructor.
     *
     * @param SongService $songService
     */
    public function __construct(SongService $songService)
    {
        $this->songService = $songService;
    }

    /**
     * @param Song $song
     * @return mixed
     * @throws Exception
     * @throws \Throwable
     */
    public function playAdd(Song $song)
    {
        //TODO: we need to limit number of requests from one user to prevent cheating
        DB::transaction(function () use ($song) {
            $song->plays()->create([]);
            $song->stat()->increment('played');
            $this->songService->incrementSongStatInCompetition($song, 'played');
        });
        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param Song $song
     * @return mixed
     * @throws Exception
     */
    public function likeAdd(Song $song)
    {
        if (!Auth::user()->can('like', $song)) {
            abort(409);
        }
        $this->songService->likeSong($song);

        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Like was added.'
        ], Response::HTTP_OK);
    }

    public function likeRemove(Song $song)
    {
        if (!Auth::user()->can('dislike', $song)) {
            abort(409);
        }
        $this->songService->unlikeSong($song);

        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Like was removed.'
        ], Response::HTTP_OK);
    }

    /**
     * @param Song $song
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function vote(Song $song)
    {
        if (!Auth::user()->can('vote', $song)) {
            abort(409);
        }
        $user = Auth::user();

        $this->songService->voteSong($song, $user);

        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Vote was added.'
        ], Response::HTTP_OK);
    }

    public function unvote(Song $song)
    {
        if (!Auth::user()->can('unvote', $song)) {
            abort(409);
        }
        $user = Auth::user();

        $this->songService->unvoteSong($song, $user);

        return response()->json([
            'statusCode' => Response::HTTP_OK,
            'message' => 'Vote was removed.'
        ], Response::HTTP_OK);
    }

    /**
     * @param DiscoverFilterRequest $request
     * @param SongService           $service
     * @return JsonResponse
     */
    public function getSongs(DiscoverFilterRequest $request, SongService $service)
    {
        $songs = $service->getSongs($request);

        return \response()->json($songs, 200, ['Cache-Control' => 'max-age=120']);
    }

    /**
     * @param SaveSongRequest $request
     * @return JsonResponse
     * @throws Exception
     * @throws \Throwable
     */
    public function saveSong(SaveSongRequest $request)
    {
        $user = Auth::user();
        $song = new Song($request->input());

        DB::transaction(function () use ($song, $request, $user) {
            $this->markUserAsArtist($user, $request);
            $song = $user->songs()->save($song);
            $song->stat()->create([]);
            $song->genres()->attach($request->genres);
            $song->languages()->attach($request->langs);
        });

        $song->load('user', 'stat', 'genres', 'languages');
        return response()->json($song, Response::HTTP_CREATED);
    }

    /**
     * @param Song              $song
     * @param UpdateSongRequest $request
     * @param SongService       $songService
     * @return JsonResponse
     */
    public function updateSong(Song $song, UpdateSongRequest $request, SongService $songService)
    {
        $songService->updateSong($song, $request);
        return response()->json(['statusCode' => Response::HTTP_OK, 'message' => 'Updated'], Response::HTTP_OK);
    }

    /**
     * This method updates song transcodingCompleted parameter
     *
     * @param Song $song
     * @param UpdateSongLambdaRequest $request
     * @return JsonResponse
     */
    public function updateSongStatus(Song $song, UpdateSongLambdaRequest $request)
    {
        $song->transcodingCompleted = $request->transcodingCompleted;
        $song->update();
        return response()->json('Updated', Response::HTTP_OK);
    }

    /**
     * Returns favorite users content for SongLine section.
     *
     * @param SongLineRequest $request
     * @param SongService     $service
     * @return mixed|JsonResponse
     */
    public function getFavoriteArtistsSongs(SongLineRequest $request, SongService $service)
    {
        $user = $request->user();
        $favoriteArtistsIds = $user->followedArtists()->get()->pluck('code')->toArray();

        //todo: Find out if there is a proper handling of the 404 response on Front end side
        // abort_if(empty($favoriteArtistsIds), 404, 'User has no favorite artists.');

        if (empty($favoriteArtistsIds)) {
            return \response()->json([])->header('Cache-Control', 'max-age=120');
        }

        $songs = $service->userFavoriteArtistsSongs(
            $favoriteArtistsIds,
            $request->input('offset', 0),
            $request->input('limit', 5)
        );

        $items = GalleryItem::byUsers($favoriteArtistsIds)
            ->recent()
            ->offset($request->input('offset', 0))
            ->limit($request->input('limit', 5))
            ->get();

        $songsAndItems = $songs->merge($items)->sortByDesc('created_at')->values()->all();

        return \response()->json($songsAndItems)->header('Cache-Control', 'max-age=120');
    }

    /**
     * @param Song $song
     * @return mixed
     * @throws \Throwable
     */
    public function deleteSong(Song $song)
    {
        $this->authorize('update', $song);

        DB::transaction(function () use ($song) {
            $song->title = '';
            $song->cover = '';
            $song->save();
            $song->delete();
            event(new SongDeleted($song));
        });

        return response()->json(['Song was deleted.'], Response::HTTP_OK);
    }

    /**
     * @param Song $song
     * @return mixed
     */
    public function shareAdd(Song $song)
    {
        $song->stat()->increment('shared');

        $stat = ArtistStat::find($song->user_code);
        if (null !== $stat) {
            $stat->increment('shared');
        }
        return response()->json([], Response::HTTP_CREATED);
    }

    /**
     * @param $user
     * @param $request
     */
    private function markUserAsArtist($user, $request)
    {
        if ($user->is_artist == false) {
            $user->genres()->sync($request->input('genres'));
            $user->languages()->sync($request->input('langs'));
            $user->is_artist = true;
            $user->save();
        }
    }
}
