<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Charge;
use App\Http\Requests\PlanRequest;
use App\Http\Requests\SubscriptionRequest;
use App\Http\Requests\TokenRequest;
use App\Http\Requests\WalletRequest;
use App\Jobs\UnsubscribeFromPlan;
use App\Notifications\SubscriptionPriceChanged;
use App\Repository\StripeRepository;
use App\Repository\SubscriptionsRepository;
use App\User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Response;
use App\Subscription;
use App\Events\ArtistSubscribe;
use App\Events\ArtistUnSubscribe;

class SubscriptionsController extends Controller
{

    private $stripeRepository;

    public function __construct()
    {
         $this->stripeRepository = new StripeRepository();
    }

    /**
     * @param SubscriptionRequest $request
     * @return mixed
     */
    public function subscribe(
        SubscriptionRequest $request
    ) {
        $plan = (int)$request->plan;
        try {
            $subscription = Auth::user()->subscription($plan);
            if (empty($subscription->quantity)) {
                return response()->json($this->createNewSubscription($plan), Response::HTTP_CREATED);
            } else {
                if ($subscription->onGracePeriod()) {
                    DB::transaction(function () use ($subscription, $plan) {
                        $subscription->resume();
                        $artist = Artist::findOrFail($plan);
                        event(new ArtistSubscribe($artist));
                        Auth::user()->subscription($plan)->price = $artist->plan_cost;
                        Auth::user()->subscription($plan)->resubscribe = false;
                        Auth::user()->subscription($plan)->save();
                    });
                    return response()->json([], Response::HTTP_OK);
                } elseif (!$subscription->price || $subscription->cancelled()) {
                    $subscription->delete();
                    return response()->json($this->createNewSubscription($plan), Response::HTTP_CREATED);
                }
                return response()->json([], Response::HTTP_CONFLICT);
            }
        } catch (\Stripe\Error\Base $e) {
            abort($e->getHttpStatus(), $e->getMessage());
        }
    }

    /**
     * @param $plan
     * @return mixed
     */
    public function unsubscribe($plan)
    {
        $plan = (int)$plan;
        DB::beginTransaction();
        try {
            $user = User::where(['code' => $plan])->firstOrFail();
            $subscription = Subscription::activeSubscriptions($plan)->firstOrFail();
            event(new ArtistUnSubscribe($user));
            if ($user->plan_cost == 0) {
                $subscription->delete();
            } else {
                Auth::user()->subscription($subscription->stripe_plan)->cancel();
            }
            DB::commit();
            return response()->json([], Response::HTTP_NO_CONTENT);
        } catch (\Stripe\Error\Base $e) {
            DB::rollBack();
            abort($e->getHttpStatus(), $e->getMessage());
        }
    }

    /**
     * @param TokenRequest $request
     * @return mixed
     */
    public function updateCardDetails(TokenRequest $request)
    {
        try {
            $customer = Auth::user()->asStripeCustomer();
            if ($customer->id) {
                Auth::user()->updateCard($request->token);
            } else {
                Auth::user()->createAsStripeCustomer($request->token);
            }
            return response()->json([], Response::HTTP_NO_CONTENT);
        } catch (\Stripe\Error\Base $e) {
            abort($e->getHttpStatus(), $e->getMessage());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCardCredentials(StripeRepository $stripe)
    {
        $card = $stripe->retrieveCard(Auth::user());
        $result['last4'] = $card['last4'];
        $result['exp_month'] = $card['exp_month'];
        $result['exp_year'] = $card['exp_year'];
        $result['card_brand'] = $card['brand'];
        return response()->json($result, Response::HTTP_OK);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getAllSubscriptions(User $user)
    {
        $artists = User::subscribedByUser($user->code)->get();
        return response()->json($artists, Response::HTTP_OK);
    }

    /**
     * @return \App\User
     */
    public function getSubscribers(User $user)
    {
        $users = User::getSubscribers($user->code)->get();
        return response()->json($users, Response::HTTP_OK);
    }

    /**
     * @param PlanRequest $request
     * @param StripeRepository $stripeClient
     * @return mixed
     */
    public function newPlan(PlanRequest $request, StripeRepository $stripeClient)
    {
        $response = $stripeClient->createPlan(Auth::user(), $request->price);
        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * @param PlanRequest $request
     * @param StripeRepository $stripeClient
     * @return mixed
     */
    public function editPlan(User $user, PlanRequest $request, StripeRepository $stripeClient)
    {
        DB::transaction(function () use ($request, $stripeClient) {
            DB::table('users')->where('code', Auth::id())->update(['plan_cost' => $request->price]);
            $stripeClient->updatePlan(Auth::user(), $request);
            $unsubscribeJob = (new UnsubscribeFromPlan(Auth::user()));
            dispatch($unsubscribeJob);
        });
        return response()->json([], Response::HTTP_NO_CONTENT);
    }

    /**
     * @param $plan_id
     * @param StripeRepository $stripeClient
     * @return mixed
     */
    public function getPlan(StripeRepository $stripeClient, $plan_id)
    {
        $plan = $stripeClient->getPlan($plan_id);
        return response()->json($plan, Response::HTTP_OK);
    }

    /**
     * @param WalletRequest $request
     * @return mixed
     */
    public function getWallet(WalletRequest $request)
    {
        if ($request->has('year') and $request->has('month')) {
            $dateFrom = Carbon::create($request->year, $request->month, 1, 0, 0, 0);
            $dateTo = Carbon::create($request->year, $request->month, 1, 0, 0, 0)
                ->addMonth();
        } else {
            $dateFrom = null;
            $dateTo = null;
        }

        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);

        $charges = $this->getCharges($dateFrom, $dateTo, $offset, $limit);
        $payout = $this->getPayouts($dateFrom, $dateTo);

        $transfers = $payout ? $charges->prepend($payout) : $charges;

        return response()->json($transfers, Response::HTTP_OK);
    }

    /**
     * @param      $plan
     * @param null $token
     * @return Subscription
     * @throws \Throwable
     */
    private function createNewSubscription($plan, $token = null)
    {
        $artist = Artist::findOrFail($plan);
        $plan = $this->stripeRepository->getPlan($plan);
        if ($plan->amount == 0) {
            DB::beginTransaction();
            try {
                event(new ArtistSubscribe($artist));
                $subscription = SubscriptionsRepository::subscribeWithoutCard($plan->id);
                DB::commit();
                return $subscription;
            } catch (\Exception $e) {
                DB::rollBack();
                throw $e;
            }
        }
        $user = Auth::user();
        if (empty($user->card_brand) || empty($user->card_last_four)) {
            abort(402, "Add payment details before you can subscribe.");
        }
        DB::transaction(function () use ($user, $plan, $token, $artist) {
            $sub = $user->newSubscription($plan->id, $plan->id)
                ->create(
                    $token,
                    ['email' => Auth::user()->email]
                );
            $sub->price = $plan->amount;
            $sub->save();
            event(new ArtistSubscribe($artist));
        });
        return $user;
    }

    private function getCharges($dateFrom, $dateTo, $offset, $limit)
    {
        $charges = Charge::where('user_code', Auth::id())
            ->whereBetween('created', [
                $dateFrom->toDateTimeString(),
                $dateTo->toDateTimeString()
            ])->get();
        $charges = $charges->splice($offset, $limit)
            ->map(function ($charge) {
                // TODO: Attention! DB request inside the cycle
                $user = User::find($charge['plan']);
                return [
                    'type' => 'charge',
                    'amount' => $charge['amount'],
                    'subscription' => 'Subscription at ' . $user->name,
                    'currency' => $charge['currency'],
                    'date' => date('d-m-Y', strtotime($charge['created'])),
                ];
            })
            ->reject(function ($value) {
                return $value == null;
            });
        return $charges;
    }

    /**
     * @param $dateTo
     * @return array
     */
    private function getPayouts($dateFrom, $dateTo)
    {
        //get payouts for previous month
        $charges = Charge::where('plan', Auth::id())
            ->where('created', '>=', $dateFrom->copy()->subMonth()->toDateTimeString())
            ->where('created', '<', $dateTo->copy()->subMonth()->toDateTimeString())
            ->get()
            ->sum('amount');
        //TODO: filter and count by currency
        if ($charges) {
            $payout = [
                'type' => 'payout',
                'amount' => $charges * config('services.stripe.payout_percent'),
                'subscription' => 'Monthly payout',
                'currency' => 'usd',
                'date' => $dateFrom->format('d-m-Y'),
            ];
            return $payout;
        }
        return null;
    }

    /**
     * @param $subscription
     * @return mixed
     * @throws \Throwable
     */
    public function markNotificationAsRead($subscription)
    {
        DB::beginTransaction();
        try {
            $notification = Auth::user()->unreadNotifications()
                ->where('type', SubscriptionPriceChanged::class)
                ->get();
            $notification->map(function ($value) use ($subscription) {
                if ($value->data['artist_code'] == $subscription) {
                    $value->markAsRead();
                }
            });
            $subscription = Subscription::where('stripe_plan', $subscription)
                ->where('user_id', Auth::id())
                ->first();
            if ($subscription) {
                $subscription->resubscribe = false;
                $subscription->save();
            }
            DB::commit();
        } catch (\Throwable $exception) {
            DB::rollBack();
            throw $exception;
        }
        return response()->json([], Response::HTTP_OK);
    }

    /**
     * @param User             $user
     * @param StripeRepository $stripe
     * @return mixed
     */
    public function deleteCard(User $user, StripeRepository $stripe)
    {
        $this->authorize('deleteCard', $user);

        $response = $stripe->removeCard();
        if ($response) {
            $user->card_last_four = null;
            $user->card_brand = null;
            $user->save();
            return response()->json([], Response::HTTP_NO_CONTENT);
        } else {
            return response()->json('No attached card', Response::HTTP_NOT_FOUND);
        }
    }
}
