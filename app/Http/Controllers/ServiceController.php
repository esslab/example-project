<?php

namespace App\Http\Controllers;

use App;

class ServiceController extends Controller
{
    public function healthcheck()
    {
        return response('', 200);
    }
}
