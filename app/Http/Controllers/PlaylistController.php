<?php

namespace App\Http\Controllers;

use App\ArtistStat;
use App\Events\PlaylistWasAddedToPlaylist;
use App\Events\SongAddedToPlaylist;
use App\Http\Requests\AddPlaylistRequest;
use App\Http\Requests\AddSongToPlaylistRequest;
use App\Http\Requests\DeleteSongFromPlaylistRequest;
use App\Http\Requests\UpdatePlaylistRequest;
use App\Playlist;
use App\Song;
use App\SongStat;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Response;
use Illuminate\Http\Response as HttpResponse;
use Faker\Generator as Faker;

class PlaylistController extends Controller
{
    /**
     * Return a listing of the Playlists for current user.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $user = Auth::user();

        $userPlaylists = $user->playlists()
            ->with('user', 'songs.genres', 'songs.stat', 'songs.user', 'songs.languages')
            ->orderBy('created_at', 'desc')
            ->get();

        return Response::json($userPlaylists->makeHidden(['user_code', 'status', 'privacy']));
    }

    /**
     * Store a newly created Playlist in storage.
     *
     * @param AddPlaylistRequest|Request $request
     * @param Faker                      $faker
     * @return HttpResponse
     */
    public function store(AddPlaylistRequest $request, Faker $faker)
    {
        $user = Auth::user();

        $newPlaylist = new Playlist;
        $newPlaylist->code = $faker->uuid;
        $newPlaylist->user_code = $user->code;
        $newPlaylist->title = $request->title;
        $newPlaylist->privacy = 1;
        $newPlaylist->status = 1;
        $newPlaylist->save();

        if ($request->has('songs')) {
            $songsIds = json_decode($request->songs);

            DB::transaction(function () use ($newPlaylist, $songsIds) {
                $newPlaylist->songs()->attach($songsIds);
                // fire an Event
                event(new PlaylistWasAddedToPlaylist($songsIds));
            });
        }

        return Response::json(
            $newPlaylist,
            HttpResponse::HTTP_CREATED,
            ["Location" => url("api/playlists", [$newPlaylist->code])]
        );
    }

    /**
     * Update the specified Playlist in storage.
     *
     * @param UpdatePlaylistRequest $request
     * @param Playlist              $playlist
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePlaylistRequest $request, Playlist $playlist)
    {
        $playlist->title = $request->title;
        $playlist->save();

        return Response::json([], HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified Playlist from storage.
     *
     * @param String Uuid   $playlistCode
     * @return HttpResponse
     */
    public function destroy(Playlist $playlist)
    {
        $this->authorize('delete', $playlist);

        $songs = $playlist->songs()->get();

        //TODO: DB requests in cycle
        $songs->map(function ($song) {
            SongStat::findOrFail($song->code)->decrement('added_to_playlist');
            ArtistStat::findOrFail($song->user_code)->decrement('added_to_playlist');
        });


        $playlist->delete();

        return Response::json([], HttpResponse::HTTP_NO_CONTENT);
    }

    /**
     * Add song to playlist of the current user
     *
     * @param AddSongToPlaylistRequest $request
     * @param Playlist $playlist
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception.
     */
    public function addSong(AddSongToPlaylistRequest $request, Playlist $playlist)
    {
        DB::beginTransaction();
        try {
            $playlist->songs()->attach($request->song_code, ['privacy' => 0]);
            $song = Song::withTrashed()->findOrFail($request->song_code);

            event(new SongAddedToPlaylist($song));

            DB::commit();
            return Response::json([], HttpResponse::HTTP_CREATED);
        } catch (Exception $error) {
            DB::rollBack();
            throw $error;
        }
    }

    /**
     * Delete song from specified playlist
     *
     * @param Playlist $playlist
     * @param Song $song
     * @param DeleteSongFromPlaylistRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function deleteSong(Playlist $playlist, Song $song, DeleteSongFromPlaylistRequest $request)
    {
        DB::beginTransaction();
        try {
            $playlist->songs()->detach($song->code);

            $song = Song::withTrashed()->findOrFail($song->code);
            SongStat::findOrFail($song->code)->decrement('added_to_playlist');
            ArtistStat::findOrFail($song->user_code)->decrement('added_to_playlist');
            DB::commit();
            return Response::json([], HttpResponse::HTTP_NO_CONTENT);
        } catch (Exception $error) {
            DB::rollBack();
            throw $error;
        }
    }
}
