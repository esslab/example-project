<?php
namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Response;

class LanguagesController extends Controller
{
    /**
     * Return collection of all languages
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function all()
    {
        $languages = Language::orderBy('full_name')->get(['code', 'short_name', 'full_name']);

        return response()
            ->json($languages, Response::HTTP_OK)
            ->header('Cache-Control', 'max-age=86400');
    }
}
