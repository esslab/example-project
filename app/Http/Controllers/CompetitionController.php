<?php

namespace App\Http\Controllers;

use App\Competition;
use App\Exceptions\NotFoundException;
use App\Http\Requests\GetCompetitionsRequest;
use App\Services\CompetitionService;
use App\Repository\Modules\Competitions\ApiCompetitionSingle;
use Response;

class CompetitionController extends Controller
{
    /**
     * @param GetCompetitionsRequest $request
     * @param CompetitionService     $competitionService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCollection(GetCompetitionsRequest $request, CompetitionService $competitionService)
    {
        $type = $request->input('type');
        $competitions = [];

        if (strtolower($type) === 'archive') {
            $competitions['weekly'] = $competitionService->getArchived('weekly');
            $competitions['bimonthly'] = $competitionService->getArchived('bimonthly');
            $competitions['yearly'] = $competitionService->getArchived('yearly');
        } else {
            $competitionWeekly = $competitionService->getCurrentOrPreviousCompetitionByType('weekly');
            if (null === $competitionWeekly) {
                $nextWeeklyCompetition = $competitionService->getNextByType('weekly');
                $message = $competitionService->getNextCompetitionMessage(
                    'weekly',
                    $nextWeeklyCompetition->date_start
                );

                $competitionWeekly = ['message' => $message];
            }
            $competitionBimonthly = $competitionService->getCurrentOrPreviousCompetitionByType('bimonthly');
            if (null === $competitionBimonthly) {
                $nextBimonthlyCompetition = $competitionService->getNextByType('bimonthly');
                $message = $competitionService->getNextCompetitionMessage(
                    'bimonthly',
                    $nextBimonthlyCompetition->date_start
                );

                $competitionBimonthly = ['message' => $message];
            }
            $competitionYearly = $competitionService->getCurrentOrPreviousCompetitionByType('yearly');
            if (null === $competitionYearly) {
                $nextYearlyCompetition = $competitionService->getNextByType('yearly');
                $message = $competitionService->getNextCompetitionMessage(
                    'yearly',
                    $nextYearlyCompetition->date_start
                );

                $competitionYearly = ['message' => $message];
            }

            $competitions['weekly'] = $competitionWeekly;
            $competitions['bimonthly'] = $competitionBimonthly;
            $competitions['yearly'] = $competitionYearly;
        }

        return Response::json($competitions);
    }

    public function getSingle($competitionCode)
    {
        $competition = ApiCompetitionSingle::detailed()->findOrFail($competitionCode);

        return response()->json($competition);
    }

    /**
     * @param CompetitionService $competitionService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentWeekly(CompetitionService $competitionService)
    {
        $response = $competitionService->getCompetitionByType('Weekly');

        if ($response instanceof NotFoundException) {
            return Response::json($response->getMessage(), $response->getStatusCode());
        }
        return Response::json($response);
    }

    /**
     * @param CompetitionService $competitionService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentBimonthly(CompetitionService $competitionService)
    {
        $response = $competitionService->getCompetitionByType('bimonthly');

        if ($response instanceof NotFoundException) {
            return Response::json($response->getMessage(), $response->getStatusCode());
        }

        return Response::json($response);
    }

    /**
     * @param CompetitionService $competitionService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCurrentYearly(CompetitionService $competitionService)
    {
        $response = $competitionService->getCompetitionByType('Yearly');

        if ($response instanceof NotFoundException) {
            return Response::json($response->getMessage(), $response->getStatusCode());
        }

        return Response::json($response);
    }
}
