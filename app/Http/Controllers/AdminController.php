<?php

namespace App\Http\Controllers;

use App\Charge;
use App\Competition;
use App\Http\Requests\Admin\AddSongToCompetitionRequest;
use App\Http\Requests\Admin\Competitions\UpdateCompetitionRequest;
use App\Http\Requests\Admin\CompetitionsRequest;
use App\Http\Requests\Admin\RemoveSongFromCompetitionRequest;
use App\Http\Requests\Admin\UpdateSongStatsInCompetitionRequest;
use App\Http\Requests\Admin\Competitions\GetCompetitionByTypeRequest;
use App\Http\Requests\GetPayoutsRequest;
use App\Services\CompetitionService;
use App\Song;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Services\Admin\CompetitionService as AdminService;
use App\Http\Requests\Admin\CompetitionRequest;

class AdminController extends Controller
{
    /**
     * @param GetPayoutsRequest $request
     * @return mixed
     */
    public function payouts(GetPayoutsRequest $request)
    {
        $limit = $request->input('limit', 10);
        $offset = $request->input('offset', 0);
        $page = $request->input('page', 0);
        if ($page) {
            $dateFrom = Carbon::now()->startOfMonth()->subMonth($page);
        } else {
            $year = $request->input('year', Carbon::now()->format('Y'));
            $month = $request->input('month', Carbon::now()->format('m'));
            $dateFrom = Carbon::create($year, $month, 1, 0, 0, 0);
        }

        $payout = [];
        $users = User::all();
        foreach ($users as $user) {
            $charges = Charge::where('plan', $user->code)
                ->where('created', '>=', $dateFrom)
                ->where('created', '<=', $dateFrom->copy()->addMonth())
                ->get()
                ->splice($offset, $limit)
                ->sum('amount');
            if ($charges) {
                $payoutDetails = $user->payoutDetails()->first();
                $name = $payoutDetails->first_name ?? $user->name;
                $lastname = $payoutDetails->last_name ?? '';
                $swift = $payoutDetails->swift ?? '';
                $bank = $payoutDetails->bank_name ?? '';
                $account = $payoutDetails->account_number ?? '';
                $payout[] = [
                    'user' => $user->code,
                    'name' => $name . ' ' . $lastname,
                    'email' => $user->email,
                    'bank' => $bank,
                    'swift' => $swift,
                    'account' => $account,
                    'total' => $charges . ' USD',
                    'fee' => 100 - config('services.stripe.payout_percent') * 100 . '%',
                    'payout' => $charges * config('services.stripe.payout_percent') . ' USD',
                    'date' => $dateFrom->format('d-m-Y'),
                ];
            }
        }
        return response()->json($payout, Response::HTTP_OK);
    }

    /**
     * @param GetCompetitionByTypeRequest $request
     * @param AdminService $adminService
     * @return mixed
     */
    public function getCompetitions(GetCompetitionByTypeRequest $request, AdminService $adminService)
    {
        $competition = $adminService->getCompetitionByType($request->type);

        if ($competition) {
            return response()->json($competition, Response::HTTP_OK);
        } else {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Add song to competition (only for admin)
     *
     * @param Competition                 $competition
     * @param AddSongToCompetitionRequest $request
     * @param CompetitionService          $competitionService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addSongToCompetition(
        Competition $competition,
        AddSongToCompetitionRequest $request,
        CompetitionService $competitionService
    ) {
        $competitionService->competitionAddSong($competition, $request);
        return response()->json(['message' => 'Song was added to competition.']);
    }

    /**
     * Remove song from competition and recalculate other songs position
     *
     * @param Competition                      $competition
     * @param string                           $songCode
     * @param RemoveSongFromCompetitionRequest $request
     * @param CompetitionService               $competitionService
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param string $song
     */
    public function removeSongFromCompetition(
        Competition $competition,
        $songCode,
        RemoveSongFromCompetitionRequest $request,
        CompetitionService $competitionService
    ) {
        $competitionService->competitionRemoveSong($competition, $songCode);
        return response()->json(['message' => 'Song was removed from competition.']);
    }

    /**
     * @param UpdateCompetitionRequest $request
     * @param AdminService $service
     * @param Competition $competition
     * @return mixed
     */
    public function updateCompetition(
        UpdateCompetitionRequest $request,
        AdminService $service,
        Competition $competition
    ) {
        $service->updateStatus($competition, $request->status);

        return response()->json(['Competition status was updated.'], Response::HTTP_OK);
    }

    /**
     * Return a competition by code with songs
     * @param AdminService $service
     * @param CompetitionRequest $request
     * @return mixed
     */
    public function getCompetition(AdminService $service, CompetitionRequest $request)
    {
        $service->setRequest($request);
        $competition = $service->getCompetition();

        if ($competition) {
            return response()->json($competition, Response::HTTP_OK);
        } else {
            return response()->json([], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @param string                              $competitionCode
     * @param string                              $songCode
     * @param UpdateSongStatsInCompetitionRequest $request
     * @param CompetitionService                  $competitionService
     * @return JsonResponse
     * @internal param Competition $competition
     */
    public function updateSongStatsInCompetition(
        $competitionCode,
        $songCode,
        UpdateSongStatsInCompetitionRequest $request,
        CompetitionService $competitionService
    ) {
        $competitionService->changeSongPosition($competitionCode, $songCode, (int)$request->position);
        return response()->json(['message' => 'Position was updated.']);
    }

    /**
     * Return a list of competitions by type for admin panel
     * @param CompetitionsRequest $request
     * @param AdminService $service
     * @return mixed
     */
    public function getCompetitionsByType(CompetitionsRequest $request, AdminService $service)
    {
        $service->setRequest($request);
        $competitions = $service->getCompetitionsListByType();

        return response()->json($competitions, Response::HTTP_OK);
    }

    /**
     * @param Song               $song
     * @param CompetitionService $competitionService
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSongCompetitions(Song $song, CompetitionService $competitionService)
    {
        $competitions = $competitionService->getCompetitionsWhereCanParticipate($song);
        return response()->json($competitions);
    }
}
