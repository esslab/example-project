<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfigRequest;
use App\Repository\AwsRepository;
use Faker\Generator as Faker;
use Illuminate\Http\Response;

class ConfigController extends Controller
{
    /**
     * @return Response
     */
    public function getS3Config(ConfigRequest $request, Faker $faker)
    {
        $directory = config('aws.s3.directories.' . $request->entity);
        $config['file_name'] = $faker->uuid;
        $config['path'] = $directory . '/' . $config['file_name'];
        $config['bucket'] = config('aws.s3.bucket');
        $config = array_merge($config, AwsRepository::getAmazonPresignedPOST($config['path']));
        return response()->json($config, Response::HTTP_OK);
    }
}
