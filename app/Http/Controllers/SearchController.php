<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Song;
use App\User;
use Illuminate\Http\Request;
use Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class SearchController extends Controller
{
    public function searchSongsOrArtists(Request $request)
    {
        $rules = [
            'q' => 'required|string|min:3',
            'limit' => 'sometimes|integer|max:50'
        ];
        $this->validate($request, $rules);
        $limit = $request->has('limit') ? $request->input('limit')/2 : 5;

        $search = $this->getPreparedSearchString($request->input('q'));

        // get Songs by title
        $songs = Song::searchByTitle($search)
            ->with('user', 'stat', 'genres', 'languages', 'competitions')
            ->take($limit)
            ->get();

        // get users by Name
        $artists = User::searchByName($search)
            ->artists()
            ->with('genres')
            ->take($limit)
            ->get();

        $response = collect([$songs, $artists])->collapse()->sortByDesc('relev');

        return Response::json($response->values()->all(), HttpResponse::HTTP_OK);
    }

    /**
     * Get string for FullText search in Boolean mode
     * Example for 2 words: '>"ona swift" <( +ona +swift*)'
     *
     * @param $search
     * @return String
     */
    private function getPreparedSearchString($search)
    {
        // apply bigger relevance to whole phrase (> "whole phrase")
        $fullPhrase = '>"' . $search . '"';
        $separateWords = array_reduce(explode(' ', $search), function ($prevResult, $word, $init = '') {
            return $prevResult .= ' +' . $word;
        });
        $preparedSearchString = $fullPhrase;
        // each words in Phrase can be (+)
        // Last word can be with 'wildcard' (*)
        // Separate words have smaller relevance (<) than a whole phrase
        $preparedSearchString .= ! empty($separateWords) ? ' <(' . $separateWords . '*)' : '';

        return $preparedSearchString;
    }
}
