<?php

namespace App\Http\Controllers;

use App\Events\UserWasDeleted;
use App\Repository\UserRepository;
use App\User;
use App\ArtistsFollows;
use App\Playlist;
use App\Song;
use Auth;
use Illuminate\Http\Response as HttpResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ProfileController extends Controller
{
    public $user;
    public $artistFollows;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProfile($user)
    {
        $user = $this->user->with('stat', 'genres', 'languages')->findOrFail($user);
        $user->followers_count = $user->followedByUsers()->count();
        $user->followed = $user->followedArtists()->count();
        return response()->json($user, HttpResponse::HTTP_OK);
    }

    /**
     * @param $user
     * @param Playlist $playlist
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPlaylists(Playlist $playlist, $user)
    {
        $userPlaylists = $playlist->where(['user_code' => $user])->with(
            'user',
            'songs.genres',
            'songs.stat',
            'songs.user',
            'songs.likes',
            'songs.languages'
        )->get();
        return response()->json($userPlaylists->makeHidden(['user_code', 'status', 'privacy']), HttpResponse::HTTP_OK);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFollowers($user)
    {
        //TODO: we should get rid of find($user) and use Service Container
        //TODO: but it's not going well with unit tests
        $followers = $this->user->find($user)->followedByUsers()->get();
        return response()->json($followers, HttpResponse::HTTP_OK);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFavoriteArtists($user)
    {
        $followed = $this->user->find($user)->followedArtists()->get();
        return response()->json($followed, HttpResponse::HTTP_OK);
    }

    /**
     * Delete authorized User profile (only for development)
     *
     * @param User $user
     * @param UserRepository $repository
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteProfile(User $user, UserRepository $repository)
    {
        if (\App::environment('production')) {
            $message = "Can't delete profile on the Production environment.";
            throw new HttpException(HttpResponse::HTTP_BAD_REQUEST, $message);
        }

        $this->authorize('delete', $user);

        $repository->deleteUser($user->code, $user->auth0_id);
        $user->delete();

        event(new UserWasDeleted($user->code, $user->auth0_id, $repository));

        return response()->json([], HttpResponse::HTTP_NO_CONTENT);
    }
}
