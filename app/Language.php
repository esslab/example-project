<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $primaryKey = 'code';

    public $incrementing = false;

    protected $hidden = ['created_at', 'updated_at', 'pivot'];

    public function songs()
    {
        return $this->belongsToMany('App\Song', 'languages_songs', 'lang_code', 'song_code')
            ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'languages_users', 'lang_code', 'user_code')
            ->withTimestamps();
    }
}
