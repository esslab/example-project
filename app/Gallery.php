<?php

namespace App;

use App\Classes\Traits\TUuid;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use TUuid;

    protected $primaryKey = 'code';

    protected $fillable = ["status", "privacy", "title"];

    protected $hidden = ['privacy', 'created_at', 'updated_at', 'user_code', 'status'];

    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_code', 'code');
    }

    public function items()
    {
        return $this->hasMany(GalleryItem::class, 'gallery_code', 'code');
    }

    public function scopeForUser($query, $user)
    {
        return $query->where(['user_code' => $user]);
    }
}
