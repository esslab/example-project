<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class Like
 * @property string song_code
 * @property string user_code
 * @method static Builder byUserToSong (User $user, Song $song)
 * @see Like::scopeByUserToSong
 * @package App
 */
class Like extends Model
{
    protected $fillable = ["song_code", "user_code"];

    public $incrementing = false;

    protected $table = self::TABLE_NAME;

    const TABLE_NAME = 'likes';

    const FIELD_SONG_CODE = 'song_code';
    const FIELD_USER_CODE = 'user_code';
    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_code', 'code');
    }

    public function song()
    {
        return $this->belongsTo(Song::class, 'song_code', 'code');
    }

    /**
     * @param Builder $query
     * @param User $user
     * @param Song $song
     * @return Builder
     */
    public function scopeByUserToSong($query, User $user, Song $song)
    {
        return $query->where('user_code', $user->code)->where('song_code', $song->code);
    }
}
