<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubscriptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the subscription.
     *
     * @param  \App\User $user
     * @param  \App\Subscription $subscription
     * @return mixed
     */
    public function update(User $user, User $artist)
    {
        return $user->code === $artist->code;
    }
}
