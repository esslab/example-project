<?php

namespace App\Policies;

use App\User;
use App\Playlist;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlaylistPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the playlist.
     *
     * @param  \App\User  $user
     * @param  \App\Playlist  $playlist
     * @return mixed
     */
    public function update(User $user, Playlist $playlist)
    {
        return $user->code === $playlist->user_code;
    }

    /**
     * Determine whether the user can delete the playlist.
     *
     * @param  \App\User  $user
     * @param  \App\Playlist  $playlist
     * @return mixed
     */
    public function delete(User $user, Playlist $playlist)
    {
        return $user->code === $playlist->user_code;
    }
}
