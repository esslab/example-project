<?php

namespace App\Policies;

use App\MediaLike;
use App\User;
use App\GalleryItem;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class GalleryItemPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the gallery item.
     *
     * @param  \App\User  $user
     * @param  \App\GalleryItem  $item
     * @return mixed
     */
    public function update(User $user, GalleryItem $item)
    {
        return $user->code === $item->user_code || $user->isAdmin();
    }

    /**
     * Determine whether the user can delete the gallery item.
     *
     * @param  User  $user
     * @param  GalleryItem  $item
     * @return mixed
     */
    public function delete(User $user, GalleryItem $item)
    {
        return $user->code === $item->user_code || $user->isAdmin();
    }

    /**
     * @param User $user
     * @param GalleryItem $item
     * @return bool
     */
    public function like(User $user, GalleryItem $item)
    {
        return Auth::check()
            && MediaLike::where('user_code', $user->code)
                ->where('item_code', $item->code)
                ->get()
                ->isEmpty();
    }

    /**
     * @param User $user
     * @param GalleryItem $item
     * @return bool
     */
    public function dislike(User $user, GalleryItem $item)
    {
        return Auth::check()
            && !MediaLike::where('user_code', $user->code)
                    ->where('item_code', $item->code)
                    ->get()
                    ->isEmpty();
    }
}
