<?php

namespace App\Policies;

use App\ModuleLike as Like;
use App\Song;
use App\User;
use App\Vote;
use Illuminate\Auth\Access\HandlesAuthorization;

class SongPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the song.
     *
     * @param  \App\User  $user
     * @param  \App\Song  $song
     * @return mixed
     */
    public function update(User $user, Song $song) : bool
    {
        return $user->code === $song->user_code || $user->isAdmin();
    }

    /**
     * Return true if User can listen the Song
     * @param User $user
     * @param Song $song
     * @return bool
     */
    public function listen(User $user, Song $song) : bool
    {
        if ($song->deleted) {
            return false;
        }
        if ($song->can_listen === 'all') {
            return true;
        }
        if ($user && $user->code === $song->user_code) {
            return true;
        }
        if ($song->can_listen === 'subscribers' && $user && $user->subscribed($song->user_code)) {
            return true;
        }
        return false;
    }

    /**
     * Return true if User can download the Song
     * @param User $user
     * @param Song $song
     * @return bool
     */
    public function download(User $user, Song $song) : bool
    {
        if ($song->deleted) {
            return false;
        }
        if ($song->can_download === 'all') {
            return true;
        }
        if ($user && $user->code === $song->user_code) {
            return true;
        }
        if ($song->can_download === 'subscribers' && $user && $user->subscribed($song->user_code)) {
            return true;
        }
        return false;
    }

    /**
     * Return true if User didn't like the song earlier
     * @param User $user
     * @param Song $song
     * @return bool
     */
    public function like(User $user, Song $song) : bool
    {
        return !Like::findByUserAndSong($user->code, $song->code)->first();
    }

    /**
     * Return true if User liked the song earlier
     * @param User $user
     * @param Song $song
     * @return bool
     */
    public function dislike(User $user, Song $song) : bool
    {
        return (bool)Like::findByUserAndSong($user->code, $song->code)->first();
    }

    /**
     * Return true if Song in Active Competition and User did not previously voted for a song in active Competition
     * @param User $user
     * @param Song $song
     * @return bool
     */
    public function vote(User $user, Song $song) : bool
    {
        return (bool)$song->competitions()->activeSick6()->first()
        && !Vote::inActiveCompetition()->byUserToSong($user, $song)->first();
    }

    /**
     * Return true if Song in Active Competition and User voted for a song previously in active Competition
     * @param User $user
     * @param Song $song
     * @return bool
     */
    public function unvote(User $user, Song $song) : bool
    {
        return (bool)$song->competitions()->activeSick6()->first()
        && (bool)Vote::inActiveCompetition()->byUserToSong($user, $song)->first();
    }
}
