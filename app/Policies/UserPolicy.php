<?php

namespace App\Policies;

use App\User;
use Auth;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function unfollow(User $artist, User $follower)
    {
        return Auth::check() &&
        $follower->code !== $artist->code &&
        $follower->followedArtists()->where('artist_code', $artist->code)->first();
    }

    public function follow(User $artist, User $follower)
    {
        return Auth::check() &&
        $follower->code !== $artist->code &&
        is_null($follower->followedArtists()->where('artist_code', $artist->code)->first());
    }

    public function subscribe(User $artist, User $follower)
    {
        return Auth::check() &&
        $follower->code !== $artist->code &&
        $artist->plan_cost > 0 &&
        //there is no subscription
        (!$follower->subscribed($artist->code) ||
            ($follower->subscription($artist->code) &&
            $follower->subscription($artist->code)->onGracePeriod() &&
            $artist->plan_cost > 0)
        );
    }

    public function unsubscribe(User $artist, User $follower)
    {
        return Auth::check() &&
        $follower->code !== $artist->code &&
        $artist->plan_cost > 0 &&
        $follower->subscribed($artist->code) &&
        ($follower->subscription($artist->code) &&
        !$follower->subscription($artist->code)->onGracePeriod());
    }

    public function update(User $user, User $artist)
    {
        return $user->code === $artist->code;
    }

    public function delete(User $authUser, User $user)
    {
        return $authUser->code === $user->code;
    }

    public function deleteCard(User $authUser, User $user)
    {
        return $authUser->code === $user->code;
    }
}
