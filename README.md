# The SongLine API
The SongLine API
# Installation
* After cloning from this repository, run `php composer.phar install --no-scripts` in your command line prompt to install this app.
* When installation will be finished, run `php composer.phar run-script post-root-package-install` command to create a new `.env` file on base `.env.deploy`. 
* Next step, run `php composer.phar run-script post-create-project-cmd`, to generate unique app-key.

Now you need to configure your database connection. You may use local or remote database, of your choice.
For localhost database you need to specify following options in `.env` file:
```
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=your_database_name
DB_USERNAME=your_user_name
DB_PASSWORD=your_user_password
```
For remote database you need set up `DB_HOST=52.50.146.153`. This is staging database.
# Testing
Each class should have a corresponding unit test, which must be stored under `./test` directory. 
To create a new Unit Test execute following command `php artisan make:test <your test name>`. 
To run phpunits tests execute command `".\vendor\bin\phpunit"` for Windows or `./vendor/bin/phpunit` for Linux.
For more information see [Laravel 5 Testing documentation](https://laravel.com/docs/5.2/testing).