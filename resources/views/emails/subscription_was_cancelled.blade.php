<!DOCTYPE html>
<html>
    <body>
        <div class="container">
            <div class="content">
                <div>Subscription for artist {{ $artist }} was cancelled. Please update your payment details and subscribe again.</div>
            </div>
            <br/>
            <div>Best regards, <a href="https://songline.fm">songline.fm</a> team.</div>
        </div>
    </body>
</html>
