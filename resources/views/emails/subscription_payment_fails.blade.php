<!DOCTYPE html>
<html>
    <body>
        <div class="container">
            <div class="content">
                <div>We trying to extend subscription for artist {{ $artist }}, but payment was failed. Please check and update your payment details, otherwise we have to unsubscribe you.</div>
            </div>
            <br/>
            <div>Best regards, <a href="https://songline.fm">songline.fm</a> team.</div>
        </div>
    </body>
</html>
