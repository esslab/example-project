<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <title>SongLine Artist's Gallery</title>

    <meta property="og:title" content="SongLine Artist’s Gallery" />
    <meta property="og:description"
          content="Check out '{{ $item->title }}' {{ $item->type }} by {{ $user->name }}" />
    <meta property="og:image" content="{{ $image }}" />
    <meta property="og:image:width" content="230" />
    <meta property="og:image:height" content="230" />
    <meta property="og:url" content="{{ config('app.url') }}/profile/{{ $user->code }}/gallery/{{$item->code}}" />
    <meta property="fb:app_id" content="{{ config('app.app_facebook_id') }}" />
    <meta property="og:type" content="website" />
    <meta name="twitter:card" content="summary">
</head>
<body>
    <p>Metadata for social crawlers</p>
</body>
</html>
