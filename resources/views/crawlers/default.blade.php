<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SongLine Artist's Profile</title>

    <meta property="og:title" content="SongLine Artist's Profile" />
    <meta property="og:description"
          content="Songline was created by independent musicians in search of new ways to grow a fan base and monetize their craft. Tune in!" />
    <meta property="og:image" content="{{ config('app.sharing_default_cover') }}" />
    <meta property="og:url" content="{{ config('app.url') }}/profile/{{ $user->code }}" />
    <meta property="fb:app_id" content="{{ config('app.app_facebook_id') }}" />
    <meta property="og:type" content="website" />
    <meta name="twitter:card" content="summary_large_image">

    <link rel="icon" type="image/x-icon" href="assets/icon/favicon.png">
</head>
<body>
    <p>Metadata for social crawlers</p>
</body>
</html>
