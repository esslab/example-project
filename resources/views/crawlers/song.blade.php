<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <title>SongLine Artist's Song</title>

    <meta property="og:title" content="SongLine Artist's Song" />
    <meta property="og:description"
          content="Check out '{{ $song->title }}' song by {{ $user->name }}" />
    <meta property="og:image" content="{{ $cover }}" />
    <meta property="og:image:width" content="{{ $coverDimension }}" />
    <meta property="og:image:height" content="{{ $coverDimension }}" />
    <meta property="og:url" content="{{ config('app.url') }}/profile/{{ $user->code }}/{{$song->code}}" />
    <meta property="fb:app_id" content="{{ config('app.app_facebook_id') }}" />
    <meta property="og:type" content="website" />
    <meta name="twitter:card" content="summary">
</head>
<body>
    <p>Metadata for social crawlers</p>
</body>
</html>
