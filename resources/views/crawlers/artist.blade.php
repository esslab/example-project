<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <title>SongLine Artist's Profile</title>

    <meta property="og:title" content="SongLine Artist's Profile" />
    <meta property="og:description"
          content="Artist profile on Songline" />
    <meta property="og:image" content="{{ $user->avatar['120x120'] ?? $user->avatar['original'] }}" />
    <meta property="og:image:width" content="120" />
    <meta property="og:image:height" content="120" />
    <meta property="og:url" content="{{ config('app.url') }}/profile/{{ $user->code }}" />
    <meta property="fb:app_id" content="{{ config('app.app_facebook_id') }}" />
    <meta property="og:type" content="website" />
    <meta name="twitter:card" content="summary">
</head>
<body>
    <p>Metadata for social crawlers</p>
</body>
</html>
