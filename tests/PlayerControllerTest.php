<?php
use App\Http\Controllers\PlayerController;
use App\Http\Requests\UpdatePlayerStatusRequest;
use App\Player;
use App\Services\PlayerService;
use App\User;

class PlayerControllerTest extends TestCase
{
    private $updatePlayerRequestMock;
    private $playerServiceMock;
    private $user;
    private $controller;

    /**
     *
     */
    public function setUp()
    {
        parent::setUp();

        $this->controller = new PlayerController();
        $this->playerServiceMock = Mockery::mock(PlayerService::class);
        $this->updatePlayerRequestMock = Mockery::mock(UpdatePlayerStatusRequest::class);
        $this->user = Mockery::mock(User::class);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * @test
     */
    public function getStatus_should_return_player_object_with_ok_status()
    {
        // $playerInstance = new Player();
        $playerInstance = Mockery::mock(Player::class);

        $this->actingAs($this->user);

        $playerInstance->shouldReceive('toArray')->once();

        $this->playerServiceMock->shouldReceive('getPlayerForUser')
            ->once()
            ->with($this->user)
            ->andReturn($playerInstance);

        $response = $this->controller->getStatus($this->playerServiceMock);
        $this->assertEquals(200, $response->status());
    }

    /**
     * @test
     */
    public function updateStatus_should_return_ok_status_and_message()
    {
        $this->playerServiceMock->shouldReceive('updatePlayerForUser')
            ->once()
            ->withArgs([Mockery::any(), Mockery::any()])
            ->andReturn(true);

        $this->actingAs($this->user);

        $response = $this->controller->updateStatus($this->updatePlayerRequestMock, $this->playerServiceMock);

        $this->assertEquals(200, $response->status());
        $this->assertContains('{"statusCode":200,"message":"Updated"}', $response->getContent());
    }
}
