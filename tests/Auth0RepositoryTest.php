<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repository\Contracts\Auth0Repository;
use OAuth2\Client;

class Auth0RepositoryTest extends TestCase
{
    private static $clientId;
    private static $clientSecret;
    private static $domain;
    private static $tokenRaw;
    private static $tokenFiltered;

    public static function setUpBeforeClass()
    {
        self::$clientId = 'abc';
        self::$clientSecret = '123';
        self::$domain = 'dummy.example.com';
        self::$tokenFiltered = 'qwerty';
        self::$tokenRaw = 'Bearer qwerty';
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIfAllSetFunctionsAreCalledInConstructor()
    {
        $mock = $this->getMockBuilder(Auth0Repository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->once())
            ->method('setAuthAPI')
            ->with($this->equalTo(self::$clientId), $this->equalTo(self::$clientSecret));

        $mock->expects($this->once())
            ->method('setDomain')
            ->with($this->equalTo(self::$domain));

        $mock->expects($this->once())
            ->method('setToken')
            ->with($this->equalTo(self::$tokenRaw));

        $reflectedClass = new ReflectionClass(Auth0Repository::class);
        $constructor = $reflectedClass->getConstructor();
        $constructor->invoke($mock, self::$clientId, self::$clientSecret, self::$domain, self::$tokenRaw);
    }

    public function testEmptyConstructor()
    {
        $testingClass = new Auth0Repository();

        $this->assertTrue($testingClass instanceof Auth0Repository);
    }

    public function testFilledConstructor()
    {
        $testingClass = new Auth0Repository(self::$clientId, self::$clientSecret, self::$domain, self::$tokenRaw);

        $this->assertTrue($testingClass instanceof Auth0Repository);

        return $testingClass;
    }

    /**
     * @param Auth0Repository $testingClass
     * @depends testFilledConstructor
     */
    public function testGetAuthAPI(Auth0Repository $testingClass)
    {
        $result = $testingClass->getAuthAPI();

        $this->assertTrue($result instanceof Client);
    }

    /**
     * @param Auth0Repository $testingClass
     * @depends testFilledConstructor
     */
    public function testGetDomain(Auth0Repository $testingClass)
    {
        $result = $testingClass->getDomain();

        $this->assertEquals(self::$domain, $result);
    }

    /**
     * @param Auth0Repository $testingClass
     * @depends testFilledConstructor
     */
    public function testGetToken(Auth0Repository $testingClass)
    {
        $result = $testingClass->getToken();

        $this->assertEquals(self::$tokenFiltered, $result);
    }

}
