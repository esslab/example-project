<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repository\UserRepository;
use App\User;

class UserRepositoryTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpsertUser()
    {
        $this->markTestIncomplete('This test will be finished when stuff from modular design will be implemented');
    }

    public function testGetUserByDecodedJWT ()
    {
        $this->markTestIncomplete('This test will be finished when stuff from modular design will be implemented');
        $jwt = new stdClass();
        $jwt->sub = 'auth0|qwerty';

        $mock = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $reflectedClass = new ReflectionClass(UserRepository::class);
        $result = $reflectedClass->getMethod('getUserByDecodedJWT')->invoke($mock, $jwt);

        $this->assertTrue($result instanceof User);
    }
}
