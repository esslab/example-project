<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use OAuth2\Client;
use Illuminate\Http\Response;
use App\Auth0AuthenticationAPI;

class Auth0AuthenticationAPITest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $mockClient = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockClient->method('fetch')
            ->willReturn(['result' => 'result', 'code' => Response::HTTP_OK]);

        $testingClassMock = $this->getMockBuilder(Auth0AuthenticationAPI::class)
            ->disableOriginalConstructor()
            ->getMock();

        $testingClassMock->expects($this->once())
            ->method('getAuthAPI')
            ->willReturn($mockClient);

        $reflectedClass = new ReflectionClass(Auth0AuthenticationAPI::class);
        $reflectedClass->getMethod('getTokenInfo')->invoke($testingClassMock);
    }

    /**
     * @expectedException App\Exceptions\Repository\Auth0BadAPIRequestException
     */
    public function testRequestExceptionThrow()
    {
        $mockClient = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockClient->method('fetch')
            ->willReturn(['result' => 'result', 'code' => 888]);

        $testingClassMock = $this->getMockBuilder(Auth0AuthenticationAPI::class)
            ->disableOriginalConstructor()
            ->getMock();

        $testingClassMock->expects($this->once())
            ->method('getAuthAPI')
            ->willReturn($mockClient);

        $reflectedClass = new ReflectionClass(Auth0AuthenticationAPI::class);
        $reflectedClass->getMethod('getTokenInfo')->invoke($testingClassMock);
    }

    /**
     * @expectedException App\Exceptions\Repository\Auth0BadAPIResponseException
     */
    public function testResponseExceptionThrow()
    {
        $mockClient = $this->getMockBuilder(Client::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockClient->method('fetch')
            ->willReturn([]);

        $testingClassMock = $this->getMockBuilder(Auth0AuthenticationAPI::class)
            ->disableOriginalConstructor()
            ->getMock();

        $testingClassMock->expects($this->once())
            ->method('getAuthAPI')
            ->willReturn($mockClient);

        $reflectedClass = new ReflectionClass(Auth0AuthenticationAPI::class);
        $reflectedClass->getMethod('getTokenInfo')->invoke($testingClassMock);
    }
}
