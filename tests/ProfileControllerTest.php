<?php

use App\ArtistsFollows;
use App\Playlist;
use App\Song;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class ProfileControllerTest extends TestCase
{

    public $userMock;
    public $songMock;
    public $playlistMock;
    private $userId = 10000000;

    /**
     *
     */
    public function testGetProfileResponse()
    {
        $this->userMock = Mockery::mock(User::class);
        $mockQuery = Mockery::mock();

        //get User with relationship
        $this->userMock->shouldReceive('with')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('findOrFail')->once()->andReturn($mockQuery);

        //count following users
        $mockQuery->shouldReceive('followedByUsers')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('count')->once()->andReturn(1);

        //count followed users
        $mockQuery->shouldReceive('followedArtists')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('count')->once()->andReturn(1);
        
        $this->app->instance(User::class, $this->userMock);
        $this->call('GET', '/api/profile/' . $this->userId);
        $this->seeJsonStructure([
            'followers_count',
            'followed',
        ]);
        $this->seeStatusCode(200);
    }
    
    /**
     *
     */
    public function testGetPlaylistsResponse()
    {
        $this->playlistMock = Mockery::mock(Playlist::class);
        $mockQuery = Mockery::mock();

        $this->playlistMock->shouldReceive('where')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('with')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('get')->once()->andReturn(new Collection([]));

        $this->app->instance(Playlist::class, $this->playlistMock);
        $this->call('GET', '/api/profile/' . $this->userId . '/playlists');
        $this->seeStatusCode(200);
    }
    
    /**
     *
     */
    public function testGetFollowersResponse()
    {
        $this->userMock = Mockery::mock(User::class);
        $mockQuery = Mockery::mock();

        $this->userMock->shouldReceive('find')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('followedByUsers')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('get')->once()->andReturn($mockQuery);

        $this->app->instance(User::class, $this->userMock);
        $this->call('GET', '/api/profile/' . $this->userId . '/followers');
        $this->seeStatusCode(200);
    }

    public function testGetFavoriteArtistsResponse()
    {
        $this->userMock = Mockery::mock(User::class);
        $mockQuery = Mockery::mock();

        $this->userMock->shouldReceive('find')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('followedArtists')->once()->andReturn($mockQuery);
        $mockQuery->shouldReceive('get')->once()->andReturn($mockQuery);

        $this->app->instance(User::class, $this->userMock);
        $this->call('GET', '/api/profile/' . $this->userId . '/favorites');
        $this->seeStatusCode(200);
    }
}
